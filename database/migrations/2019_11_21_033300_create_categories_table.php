<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('category_name');
            $table->string('category_slug');
            $table->longText('category_description');
            $table->string('category_seo_title')->nullable();
            $table->string('category_seo_keyword')->nullable();
            $table->text('category_seo_description')->nullable();
            $table->integer('category_parent')->default(0);
            $table->text('category_thumbnail')->nullable();
            $table->string('category_type');
            $table->string('language', 10)->default('vi');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
