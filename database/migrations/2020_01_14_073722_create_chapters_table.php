<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChaptersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chapters', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('story_id')->unsigned();
            $table->string('chapter_title');
            $table->string('chapter_slug');
            $table->longText('chapter_content');
            $table->string('chapter_seo_title')->nullable();
            $table->text('chapter_seo_keyword')->nullable();
            $table->text('chapter_seo_description')->nullable();
            $table->enum('chapter_status', ['trash', 'pending', 'draft', 'publish']);
            $table->bigInteger('chapter_order')->default(0);
            $table->string('language', 10)->default('vi');
            $table->bigInteger('created_by_user')->unsigned();
            $table->bigInteger('updated_by_user')->unsigned();
            $table->timestamps();
            $table->foreign('created_by_user')->references('id')->on('users');
            $table->foreign('updated_by_user')->references('id')->on('users');
            $table->foreign('story_id')->references('id')->on('stories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chapters');
    }
}
