<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('story_title');
            $table->string('story_slug');
            $table->text('story_description');
            
            $table->string('story_seo_title')->nullable();
            $table->text('story_seo_keyword')->nullable();
            $table->text('story_seo_description')->nullable();
            $table->string('story_thumbnail')->nullable();
            $table->string('story_author', 100);
            $table->string('story_source', 100);
            $table->enum('story_status', ['pending', 'coming', 'full']);
            $table->string('story_type', 40)->default('light-novel');
            $table->tinyInteger('story_feature')->default(0);
            $table->string('language', 10)->default('vi');
            $table->bigInteger('created_by_user')->default(0)->unsigned();
            $table->bigInteger('updated_by_user')->default(0)->unsigned();
            $table->timestamps();
            $table->foreign('created_by_user')->references('id')->on('users');
            $table->foreign('updated_by_user')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stories');
    }
}
