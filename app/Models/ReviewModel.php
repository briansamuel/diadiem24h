<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

class ReviewModel
{
    //
    protected static $table = 'places_comments';

    public static function search($search, $filter)
    {
        return DB::table(self::$table)->where($filter)->where('author', 'LIKE', $search.'%')->get();
    }

    
    public static function getMany($columns = ['*'],$pagination, $sort, $filter)
    {
        $pagination['perpage']  = isset($pagination['perpage']) ? $pagination['perpage'] : 20;
        $offset = ($pagination['page'] - 1) * $pagination['perpage'];

        $query = DB::table(self::$table)->select($columns)->skip($offset)->take($pagination['perpage']);

        if(isset($filter['place']) && $filter['place'] != ""){
            $query->where('place', '=', $filter['place']);
        }

        if(isset($filter['author']) && $filter['author'] != ""){
            $query->where('author', 'like', "%".$filter['author']."%");
        }

        if(isset($filter['device']) && $filter['device'] != ""){
            $query->where('device', 'like', "%".$filter['device']."%");
        }

        if (isset($filter['created_at']) && $filter['created_at'] != "") {
            $time_filter = explode(" - ", $filter['created_at']);
            $start_time = date("Y-m-d 00:00:00", strtotime($time_filter[0]));
            $end_time = date("Y-m-d 23:59:59", strtotime($time_filter[1]));

            $query->where('created_at', '>=', $start_time);
            $query->where('created_at', '<', $end_time);
        }

        if(isset($sort['field']) && $sort['field'] != ""){
            $query->orderBy($sort['field'], $sort['sort']);
        } else {
            $query->orderBy('created_at', 'DESC');
        }

        return $query->get();
    }

    public static function getManyWithPaginate($columns = ['*'],$pagination, $sort, $filter)
    {
        $pagination['perpage']  = isset($pagination['perpage']) ? $pagination['perpage'] : 20;
        $offset = ($pagination['page'] - 1) * $pagination['perpage'];

        $query = DB::table(self::$table);
                // ->leftJoin('provinces','provinces.id','=','places.province')
                // ->leftJoin('districts','districts.id','=','places.district')
                // ->leftJoin('areas','areas.id','=','places.area')
                // ->leftJoin('careers','careers.id','=','places.career');

        $query->select($columns)->skip($offset)->take($pagination['perpage']);

        if(isset($filter['places.status']) && $filter['status'] != ""){
            $query->where('status', '=', $filter['status']);
        }

        if(isset($filter['places.title']) && $filter['title'] != ""){
            $query->where('title', 'like', "%".$filter['title']."%");
        }


        if (isset($filter['places.created_at']) && $filter['created_at'] != "") {
            $time_filter = explode(" - ", $filter['created_at']);
            $start_time = date("Y-m-d 00:00:00", strtotime($time_filter[0]));
            $end_time = date("Y-m-d 23:59:59", strtotime($time_filter[1]));

            $query->where('created_at', '>=', $start_time);
            $query->where('created_at', '<', $end_time);
        }

        if(isset($sort['field']) && $sort['field'] != ""){
            $query->orderBy($sort['field'], $sort['sort']);
        } else {
            $query->orderBy('places.created_at', 'DESC');
        }

        return $query->get();
    }

    public static function totalRows($filter) {

        $query = DB::table(self::$table)->select('id');
        
        if(isset($filter['place']) && $filter['place'] != ""){
            $query->where('place', '=', $filter['place']);
        }
        if(isset($filter['author']) && $filter['author'] != ""){
            $query->where('author', 'like', "%".$filter['author']."%");
        }

        
        
        if (isset($filter['created_at']) && $filter['created_at'] != "") {
            $time_filter = explode(" - ", $filter['created_at']);
            $start_time = date("Y-m-d 00:00:00", strtotime($time_filter[0]));
            $end_time = date("Y-m-d 23:59:59", strtotime($time_filter[1]));

            $query->where('created_at', '>=', $start_time);
            $query->where('created_at', '<', $end_time);
        }
        return $query->count();
        // $count = count($query->get());
        // return $count;

    }

    public static function countReviewsByPlace($place_ids) {
        
        $query = DB::table(self::$table)->select(array('place', DB::raw('COUNT(*) as count')));
        if(isset($place_ids) && $place_ids != ""){
            $query->whereIn('place', $place_ids);
        }
        $query->groupBy('place');
        return $query->get();
    }

    public static function findByKey($key, $value, $columns = ['*'])
    {
        $data = DB::table(self::$table)->select($columns)->where($key, $value)->first();
        return $data ? $data : [];
    }

    public static function findByCondition($condition, $columns = ['*'])
    {
        $data = DB::table(self::$table)->select($columns)->where($condition)->first();
        return $data ? $data : [];
    }

    public static function insert($params)
    {
        return DB::table(self::$table)->insertGetId($params);

    }

    public static function update($id, $params)
    {
        return DB::table(self::$table)->where('id', $id)->update($params);

    }

   

    public static function updateManyPost($ids, $data)
    {
        return DB::table(self::$table)->whereIn('id', $ids)->update($data);
    }

    public static function delete($id)
    {
        return DB::table(self::$table)->where('id', $id)->delete();

    }

    public static function deleteManyPost($ids)
    {
        return DB::table(self::$table)->whereIn('id', $ids)->delete();
    }

    public static function takeNew($quantity, $filter)
    {
        $query = DB::table(self::$table);

        if(isset($filter['status']) && $filter['status'] != ""){
            $query->where('status', '=', $filter['status']);
        }
        if(isset($filter['title']) && $filter['title'] != ""){
            $query->where('title', 'like', $filter['title']."%");
        }

        if(isset($filter['ids']) && $filter['ids'] != ""){
            $query->whereIn('id', $filter['ids']);
        }

        if(isset($filter['exclude']) && $filter['exclude'] != ""){
            $query->where('id', '!=', $filter['exclude']);
        }

        if(isset($filter['language']) && $filter['language'] != ""){
            $query->where('language', $filter['language']);
        }

        $query->where('status', 'publish');
        $query->orderBy('updated_at', 'DESC');
        return $query->paginate($quantity);
    }

    public static function takeHotNew($quantity, $filter)
    {
        $query = DB::table(self::$table);

        if(isset($filter['status']) && $filter['status'] != ""){
            $query->where('status', '=', $filter['status']);
        }
        if(isset($filter['title']) && $filter['title'] != ""){
            $query->where('title', 'like', "%".$filter['title']."%");
        }

        if(isset($filter['ids']) && $filter['ids'] != ""){
            $query->whereIn('id', $filter['ids']);
        }

        if(isset($filter['exclude']) && $filter['exclude'] != ""){
            $query->where('id', '!=', $filter['exclude']);
        }

        $query->where('status', 'publish');

        return $query->paginate($quantity);
    }
}
