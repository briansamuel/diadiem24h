<?php

namespace App\Models;
use Session;
use Illuminate\Support\Facades\DB;

class PlaceModel
{
    //
    protected static $table = 'places';

    public static function search($search, $filter)
    {
        return DB::table(self::$table)->where($filter)->where('title', 'LIKE', $search.'%')->get();
    }

    
    public static function getMany($columns = ['*'],$pagination, $sort, $filter)
    {
        $pagination['perpage']  = isset($pagination['perpage']) ? $pagination['perpage'] : 20;
        $offset = ($pagination['page'] - 1) * $pagination['perpage'];

        $query = DB::table(self::$table)->select($columns)->skip($offset)->take($pagination['perpage']);

        if(isset($filter['status']) && $filter['status'] != ""){
            $query->where('status', '=', $filter['status']);
        }

        if(isset($filter['title']) && $filter['title'] != ""){
            $query->where('title', 'like', "%".$filter['title']."%");
        }

        if(isset($filter['language']) && $filter['language'] != ""){
            $query->where('language', $filter['language']);
        }

        if (isset($filter['created_at']) && $filter['created_at'] != "") {
            $time_filter = explode(" - ", $filter['created_at']);
            $start_time = date("Y-m-d 00:00:00", strtotime($time_filter[0]));
            $end_time = date("Y-m-d 23:59:59", strtotime($time_filter[1]));

            $query->where('created_at', '>=', $start_time);
            $query->where('created_at', '<', $end_time);
        }

        if(isset($sort['field']) && $sort['field'] != ""){
            $query->orderBy($sort['field'], $sort['sort']);
        } else {
            $query->orderBy('created_at', 'DESC');
        }

        return $query->get();
    }

    public static function getManyWithPaginate($columns = ['*'],$pagination, $sort, $filter)
    {
        // Time Start
        $time_start = microtime(true); 

        $pagination['perpage']  = isset($pagination['perpage']) ? $pagination['perpage'] : 20;
        $offset = ($pagination['page'] - 1) * $pagination['perpage'];
        DB::flushQueryLog();
        $query = DB::table(self::$table);
                // ->leftJoin('provinces','provinces.id','=','places.province')
                // ->leftJoin('districts','districts.id','=','places.district')
                // ->leftJoin('areas','areas.id','=','places.area')
                // ->leftJoin('careers','careers.id','=','places.career');

        $query->select($columns)->skip($offset)->take($pagination['perpage']);

        if(isset($filter['status']) && $filter['status'] != ""){
            $query->where('status', '=', $filter['status']);
        }

        if(isset($filter['title']) && $filter['title'] != ""){
            $query->where('title', 'like', "%".$filter['title']."%");
        }

        if(isset($filter['province']) && $filter['province'] != ""){
            $query->where('province', '=', $filter['province']);
        }
        if(isset($filter['district']) && $filter['district'] != ""){
            $query->where('district', '=', $filter['district']);
        }
        if(isset($filter['area']) && $filter['area'] != ""){
            $query->where('area', '=', $filter['area']);
        }

        if(isset($filter['career']) && $filter['career'] != ""){
            $query->where('career', '=', $filter['career']);
        }
        
        if(isset($filter['career_ids']) && $filter['career_ids'] != ""){
            $query->whereIn('career', $filter['career_ids']);
        }

        if(isset($filter['rating']) && $filter['rating'] != ""){

            if($filter['rating'] == 1) {
                $query->whereBetween('rating_value', [1, 5]);
            } else if($filter['rating'] == 2) {
                $query->whereBetween('rating_value', [5, 7]);
            } else if($filter['rating'] == 3) {
                $query->whereBetween('rating_value', [7, 9]);
            } else if($filter['rating'] == 4) {
                $query->whereBetween('rating_value', [9, 10]);
            }
            
        }

        if (isset($filter['places.created_at']) && $filter['created_at'] != "") {
            $time_filter = explode(" - ", $filter['created_at']);
            $start_time = date("Y-m-d 00:00:00", strtotime($time_filter[0]));
            $end_time = date("Y-m-d 23:59:59", strtotime($time_filter[1]));

            $query->where('created_at', '>=', $start_time);
            $query->where('created_at', '<', $end_time);
        }

        if(isset($sort['field']) && $sort['field'] != ""){
            $query->orderBy($sort['field'], $sort['sort']);
        } else {
            $query->orderBy('places.created_at', 'DESC');
        }
        $result =  $query->get();
        $time_end = microtime(true);
        $execution_time = ($time_end - $time_start);
        Session::flash('query_excution_time', round($execution_time, 4));
        return $result;
    }

    public static function totalRows($filter) {

        $query = DB::table(self::$table);
        
        if(isset($filter['status']) && $filter['status'] != ""){
            $query->where('status', '=', $filter['status']);
        }
        if(isset($filter['title']) && $filter['title'] != ""){
            $query->where('title', 'like', "%".$filter['title']."%");
        }

        if(isset($filter['province']) && $filter['province'] != ""){
            $query->where('province', '=', $filter['province']);
        }

        if(isset($filter['district']) && $filter['district'] != ""){
            $query->where('district', '=', $filter['district']);
        }

        if(isset($filter['area']) && $filter['area'] != ""){
            $query->where('area', '=', $filter['area']);
        }

        if(isset($filter['career']) && $filter['career'] != ""){
            $query->where('career', '=', $filter['career']);
        }

        if(isset($filter['rating']) && $filter['rating'] != ""){

            if($filter['rating'] == 1) {
                $query->whereBetween('rating_value', [1, 5]);
            } else if($filter['rating'] == 2) {
                $query->whereBetween('rating_value', [5, 7]);
            } else if($filter['rating'] == 3) {
                $query->whereBetween('rating_value', [7, 9]);
            } else if($filter['rating'] == 4) {
                $query->whereBetween('rating_value', [9, 10]);
            }
            
        }

        if(isset($filter['career_ids']) && $filter['career_ids'] != ""){
            $query->whereIn('career', $filter['career_ids']);
        }

        if(isset($filter['ids']) && $filter['ids'] != ""){
            $query->whereIn('id', $filter['ids']);
        }
        
        if (isset($filter['created_at']) && $filter['created_at'] != "") {
            $time_filter = explode(" - ", $filter['created_at']);
            $start_time = date("Y-m-d 00:00:00", strtotime($time_filter[0]));
            $end_time = date("Y-m-d 23:59:59", strtotime($time_filter[1]));

            $query->where('created_at', '>=', $start_time);
            $query->where('created_at', '<', $end_time);
        }
        if (isset($filter['groupby']) && $filter['groupby'] != "") { 
            $query->groupBy($filter['groupby']);
        }

        $total_place = $query->count();
        
        return $total_place;

    }

    public static function countPlaceByProvince($filter) {
        
        $query = DB::table(self::$table)->select(array('province', DB::raw('COUNT(*) as count')));
        if (isset($filter['groupby']) && $filter['groupby'] != "") { 
            $query->groupBy($filter['groupby']);
        }
        return $query->get();
    }



    public static function findByKey($key, $value, $columns = ['*'])
    {
        $data = DB::table(self::$table)->select($columns)->where($key, $value)->first();
        return $data ? $data : [];
    }

    public static function findByCondition($condition, $columns = ['*'])
    {
        $data = DB::table(self::$table)->select($columns)->where($condition)->first();
        return $data ? $data : [];
    }

    public static function insert($params)
    {
        return DB::table(self::$table)->insertGetId($params);

    }

    public static function update($id, $params)
    {
        return DB::table(self::$table)->where('id', $id)->update($params);

    }

   

    public static function updateManyPost($ids, $data)
    {
        return DB::table(self::$table)->whereIn('id', $ids)->update($data);
    }

    public static function delete($id)
    {
        return DB::table(self::$table)->where('id', $id)->delete();

    }

    public static function deleteManyPost($ids)
    {
        return DB::table(self::$table)->whereIn('id', $ids)->delete();
    }

    public static function takeNew($quantity, $filter)
    {
        $query = DB::table(self::$table);

        if(isset($filter['status']) && $filter['status'] != ""){
            $query->where('status', '=', $filter['status']);
        }
        if(isset($filter['title']) && $filter['title'] != ""){
            $query->where('title', 'like', $filter['title']."%");
        }

        if(isset($filter['ids']) && $filter['ids'] != ""){
            $query->whereIn('id', $filter['ids']);
        }

        if(isset($filter['exclude']) && $filter['exclude'] != ""){
            $query->where('id', '!=', $filter['exclude']);
        }

        if(isset($filter['language']) && $filter['language'] != ""){
            $query->where('language', $filter['language']);
        }

        $query->where('status', 'publish');
        $query->orderBy('updated_at', 'DESC');
        return $query->paginate($quantity);
    }

    public static function takeHotNew($quantity, $filter)
    {
        $query = DB::table(self::$table);

        if(isset($filter['status']) && $filter['status'] != ""){
            $query->where('status', '=', $filter['status']);
        }
        if(isset($filter['title']) && $filter['title'] != ""){
            $query->where('title', 'like', "%".$filter['title']."%");
        }

        if(isset($filter['ids']) && $filter['ids'] != ""){
            $query->whereIn('id', $filter['ids']);
        }

        if(isset($filter['exclude']) && $filter['exclude'] != ""){
            $query->where('id', '!=', $filter['exclude']);
        }

        $query->where('status', 'publish');

        return $query->paginate($quantity);
    }
}
