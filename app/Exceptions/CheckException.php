<?php
namespace App\Throwable;

use Log;

class CheckException extends \Throwable
{
	protected $code;
	protected $content;

	public function __construct($message, $code, $content = [])
	{
		$this->code = $code;
		$this->content = $content;

		// log exception
		Log::warning($message, $content);

		parent::__construct($message);
	}

	public function getErrorCode()
	{
		return $this->code;
	}

	public function getContent($content)
	{
		return $this->content;
	}
}
