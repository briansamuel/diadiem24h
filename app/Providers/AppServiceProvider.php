<?php

namespace App\Providers;


use App\Helpers\ArrayHelper;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use App\Services\ProvinceService;
use App\Services\CareerService;
use App\Services\PlaceService;
use App\Services\SettingService;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {   
        $provinces = ProvinceService::getAll();
        $careers = CareerService::getMany(['*'], null, null, array('parent' => 0));
        $countPlaceProvinces = PlaceService::countPlaceByProvince(array('groupby' => 'province'))->toArray();
        $provinceArgs = [];
        foreach($countPlaceProvinces as $item) {
            $provinceArgs[$item->province] = $item->count;
        }
        foreach($provinces as $key => $item_province) {
            
            if(isset($provinceArgs[$item_province->id])) {
                $item_province->count = $provinceArgs[$item_province->id];
            } else {
                $item_province->count = 0;
            }
            
        }

        View::share('arrayLang', ArrayHelper::arrayLang());
        View::share('setting', new SettingService());
        View::share('provinces', $provinces);
        View::share('careers', $careers);
        Schema::defaultStringLength(191);
    }
}
