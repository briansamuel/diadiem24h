<?php
namespace App\Services;


use Validator;

class ValidationService
{

    /*
    * function make validation
    */
    public function make($params, $type)
    {
        $validator = Validator::make(
            $params,
            $this->getRules($type),
            $this->getCustomMessages(),
            $this->attributes()
        );
        return $validator;
    }

    /*
    * function get rule config
    */
    public function getRules($type)
    {
        $rules = [
            'update_my_profile_fields' => [
                'full_name' => self::getRule('require_field'),
            ],
            'change_password_fields' => [
                'password' => self::getRule('password'),
                'new_password' => self::getRule('password'),
                'confirm_new_password' => self::getRule('password'),
            ],
            'add_user_fields' => [
                'email' => self::getRule('email'),
                'username' => self::getRule('username'),
                'full_name' => self::getRule('full_name'),
                'account_type' => self::getRule('require_field'),
                'password' => self::getRule('password')
            ],
            'edit_user_fields' => [
                'full_name' => self::getRule('full_name'),
                'account_type' => self::getRule('require_field'),
                'status' => self::getRule('require_field')
            ],

            'add_page_fields' => [
                'page_title' => self::getRule('page_unique'),
                'page_description' => self::getRule('require_field'),
                'page_content' => self::getRule('require_field'),
                
            ],
            'edit_page_fields' => [
                'page_title' => self::getRule('require_field'),
                'page_description' => self::getRule('require_field'),
                'page_content' => self::getRule('require_field'),
                
            ],
            
            'add_news_fields' => [
                'post_title' => self::getRule('require_field'),
                'post_description' => self::getRule('require_field'),
                'post_content' => self::getRule('require_field'),
                
            ],
            'edit_news_fields' => [
                'post_title' => self::getRule('require_field'),
                'post_description' => self::getRule('require_field'),
                'post_content' => self::getRule('require_field'),
                
            ],
            'add_user_group_fields' => [
                'group_name' => self::getRule('require_field'),
                'enabled' => self::getRule('require_field'),
                'short_description' => self::getRule('require_field')
            ],

            'edit_contact_fields' => [
                'status' => self::getRule('require_field')
            ],
            'add_contact_reply_fields' => [
                'contact_id' => self::getRule('require_field'),
                'message' => self::getRule('require_field')
            ],
        ];

        return isset($rules[$type]) ? $rules[$type] : array();
    }

    /*
    * function get Attrbutes
    */
    public function attributes()
    {
        return [
            
            'post_title' => 'Tiêu đề',
            'post_description' => 'Mô tả',
            'post_content' => 'Nội dung',
            'host_name' => 'Tên khách sạn, resort',
            'host_descrition' => 'Mô tả khách sạn, resort',
            

        ];
       
    }

    /*
    * function get rule
    */
    public function getRule($rule)
    {
        $rules = [
            'limit' => 'numeric',
            'offset' => 'numeric',
            'user_id' => 'numeric|required',
            'username' => 'required|min:3',
            'full_name' => 'required',
            'phone_number' => 'digits_between:10,15|required',
            'password' => 'nullable|min:6|required',
            'require_field' => 'required',
            'email' => 'email',
            'payment_method' => 'required',
            'ip' => 'ip',
            'amount' => 'numeric|required',
            'number' => 'numeric',
            'post_unique' => 'required|unique:posts',
            'page_unique' => 'required|unique:pages',
            'host_name' => 'required|unique:hosts'
        ];

        return $rules[$rule];
    }

    /**
     * function get custom messages
     */
    public function getCustomMessages()
    {
        $messages = [
            'required' => 'Không bỏ trống trường :attribute',
            'unique' => ':attribute đã tồn tại, vui lòng thay đổi nội dung',
        ];

        return $messages;
    }
}
