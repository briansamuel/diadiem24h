<?php
namespace App\Services;

use App\Models\ReviewModel;
use App\Transformers\AgencyTransformer;
use Illuminate\Support\Str;

class ReviewService
{
	public static function totalRows($params) {
        $result = ReviewModel::totalRows($params);
        return $result;
	}
	
	public static function getMany($columns,$pagination, $sort, $filter)
	{
		$result = ReviewModel::getMany($columns,$pagination, $sort, $filter);
        return $result ? $result : [];
	}

	public static function countReviewsByPlace($place_ids) {
		$result = ReviewModel::countReviewsByPlace($place_ids);
        return $result ? $result : [];
	}
	public static function searchNews($search)
	{
	    $filter = array('post_status' => 'publish');
		$result = ReviewModel::search($search, $filter);
        return $result ? $result : [];
	}

	public static function findByKey($key, $value)
	{
        $result = ReviewModel::findByKey($key, $value);
        return $result ? $result : [];
	}

	public static function findBySlug($slug)
	{
	    $condition['post_slug'] = $slug;
	    $condition['post_status'] = 'publish';
        $result = ReviewModel::findByCondition($condition);
        return $result ? $result : [];
	}

	public function insert($params)
	{
		// $insert['post_title'] = $params['post_title']; 
		// $insert['post_slug'] = isset($params['post_slug']) ? Str::slug($params['post_slug']) : Str::slug($params['post_title']);
		// $insert['post_description'] = $params['post_description']; 
		// $insert['post_content'] = $params['post_content']; 
		// $insert['post_seo_title'] = isset($params['post_seo_title']) ? $params['post_seo_title'] : '';
		// $insert['post_seo_description'] = isset($params['post_seo_description']) ? $params['post_seo_description'] : ''; 
		// $insert['post_seo_keyword'] = isset($params['post_seo_keyword']) ? $params['post_seo_keyword'] : ''; 
		// $insert['post_thumbnail'] = isset($params['post_thumbnail']) ? $params['post_thumbnail'] : ''; 
		// $insert['post_author'] = isset($params['post_author']) ? $params['post_author'] : 0; 
		// $insert['post_status'] = $params['post_status']; 
		// $insert['language'] = isset($params['language']) ? $params['language'] : 'vi';
		// $insert['created_by_user'] = isset($params['created_by_user']) ? $params['created_by_user'] : 0; 
		// $insert['updated_by_user'] = isset($params['updated_by_user']) ? $params['updated_by_user'] : 0; 
		$params['created_at'] = isset($params['created_at']) ? $params['created_at'] : date("Y-m-d H:i:s"); 
		$params['updated_at'] = date("Y-m-d H:i:s"); 
		return ReviewModel::insert($params);		
	}
	public function update($id, $params)
	{
		// $update['post_title'] = $params['post_title']; 
		// $update['post_slug'] = isset($params['post_slug']) ? $params['post_slug'] : Str::slug($params['post_title']);
		// $update['post_description'] = $params['post_description']; 
		// $update['post_content'] = $params['post_content']; 
		// $update['post_seo_title'] = isset($params['post_seo_title']) ? $params['post_seo_title'] : '';
		// $update['post_seo_description'] = isset($params['post_seo_description']) ? $params['post_seo_description'] : ''; 
		// $update['post_seo_keyword'] = isset($params['post_seo_keyword']) ? $params['post_seo_keyword'] : ''; 
		// $update['post_thumbnail'] = isset($params['post_thumbnail']) ? $params['post_thumbnail'] : ''; 
		// $update['post_status'] = $params['post_status']; 
		// $update['language'] = isset($params['language']) ? $params['language'] : 'vi';
		// $update['updated_by_user'] = isset($params['updated_by_user']) ? $params['updated_by_user'] : 0; 
		// $update['created_at'] = isset($params['created_at']) ? $params['created_at'] : date("Y-m-d H:i:s"); 
		$params['updated_at'] = date("Y-m-d H:i:s"); 
		return ReviewModel::update($id, $params);		
	}

	public function updateView($id, $view = 1)
    {
        return ReviewModel::updateView($id, $view);
    }

	public function updateMany($ids, $data)
    {
        return ReviewModel::updateManyPost($ids, $data);
	}
	
	public function deleteMany($ids)
    {
        return ReviewModel::deleteManyPost($ids);
	}
	
	public function delete($id)
	{
		return ReviewModel::delete($id);		
	}

	public function getList(array $params)
    {
		$pagination = $params['pagination'];
        $sort = isset($params['sort']) ? $params['sort'] : [];
        $query = isset($params['query']) ? $params['query'] : [];
		$total = self::totalRows($query);
		
		$column = ['id', 'post_title', 'post_status', 'language', 'post_thumbnail', 'post_description', 'post_slug', 'created_at', 'post_author'];
        $result = ReviewModel::getMany($column, $pagination, $sort, $query);

        $data['data'] = $result;
        $data['meta']['page'] = isset($pagination['page']) ? $pagination['page'] : 1;
        $data['meta']['perpage'] = isset($pagination['perpage']) ? $pagination['perpage'] : 20;
        $data['meta']['total'] = $total;
        $data['meta']['pages'] = ceil($total / $data['meta']['perpage']);
		$data['meta']['rowIds'] = self::getListIDs($result);
        return $data;
	}
	
	public function getListIDs($data) {

		$ids = array();

		foreach($data as $row) {
			array_push($ids, $row->id);
		}

		return $ids;
	}

	public static function takeNew($quantity, $filter)
    {
        return ReviewModel::takeNew($quantity, $filter);
    }

    public static function takeHotNew($quantity, $filter)
    {
        return ReviewModel::takeHotNew($quantity, $filter);
    }
}