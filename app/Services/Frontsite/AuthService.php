<?php

namespace App\Services\Frontsite;

use App\Models\GuestModel;
use App\Services\GuestService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthService
{
    /*
     * function check login
     */

    public static function checkLogin()
    {
        if (!Auth::guard('user')->check()) {
            return false;
        }

        return true;
    }

    public static function login($email, $password, $remember = true){
        $result['status'] = false;
        $result['msg'] = "Dữ liệu đầu vào không hợp lệ";
        $result['data'] = array();
        if(!isset($email) || !$email || !isset($password) || !$password){
            return $result;
        }

        $userInfo =  GuestService::getByEmail($email);

        if(!$userInfo){
            $result['msg'] = "Email không tồn tại !";
            return $result;
        }

        if (!Auth::guard('user')->attempt(['email' => $email, 'password' => $password], $remember)) {
            $result['msg'] = "Mật khẩu không chính xác !";
            return $result;
        }
        $result['data'] = $userInfo;
        $result['status'] = true;
        $result['msg'] = "";
        return $result;
    }

    /*
     * function get user page info
     */

    public static function getAuthorize()
    {
        // check user logined
        if (!self::checkLogin()) {
            return false;
        }

        $userInfo = self::getUserInfo();

        return array(
            'user_id' => $userInfo->id,
            'email' => $userInfo->email,
            'status' => $userInfo->status,
            'is_root' => $userInfo->is_root === 1 ? 'yes' : 'no'
        );
    }



    /*
     * function login user
     */

    public static function loginGuest($request)
    {
        $result['status'] = false;
        $result['msg'] = "";
        $result['url'] = "";

        $email = $request->input('email', '');
        $password = $request->input('password', '');
        $loginInfo = self::login($email, $password);
        if(!$loginInfo || !isset($loginInfo['status']) || !$loginInfo['status']){
            $result['msg'] = $loginInfo['msg'];
            return $result;
        }
        //xử lý status
        $userInfo = $loginInfo['data'];
        if($userInfo->status =="active"){
            GuestModel::updateGuest($userInfo->id, ['last_visit' => date("Y-m-d H:i:s")]);

            $result['status'] = true;
            $result['msg'] = "Đăng nhập thành công !!!";
            $result['url'] = "/";

            return $result;
        }elseif($userInfo->status == "blocked"){
            Auth::guard('user')->logout();
            $result['status'] = false;
            $result['msg'] = "Tài khoản của bạn đã bị khóa !";
            $result['url'] = "";

            return $result;
        }elseif($userInfo->status == "deactive"){
            Auth::guard('user')->logout();
            $result['status'] = false;
            $result['msg'] = "Tài khoản của bạn đã bị tạm khóa !";
            $result['url'] = "";

            return $result;
        }else{
            Auth::guard('user')->logout();
            $result['status'] = false;
            $result['msg'] = "Tài khoản của bạn chưa được kích hoạt !";
            $result['url'] = "";

            return $result;
        }
    }

    public static function getUserInfo()
    {
        return Auth::guard('user')->user();
    }

    public static function logout()
    {
        Auth::guard('user')->logout();
        return true;
    }

    public static function checkCurrentPassword($password)
    {
        if (Hash::check($password, Auth::guard('user')->user()->password)){
            return true;
        }

        return false;
    }

}
