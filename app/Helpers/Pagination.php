<?php

namespace App\Helpers;

use GuzzleHttp\Psr7\Request;
use PhpParser\Node\Expr\Cast\Object_;

class Pagination {
    protected $request;
    public $_page = 1;
    public $_limit = 20;
    public $_total = 0;

    function __construct()
    {   

    }

    public function paginate($page = 1, $perpage, $total) {

        $result         = new \stdClass();
        $this->_page   = $page;
        $this->_limit  = $perpage;
        $this->_total  = $total;

        return $result;
    }

    public function createLinks( $links, $list_class ) {
        if ( $this->_limit == 'all' ) {
            return '';
        }
     
        $last       = ceil( $this->_total / $this->_limit );
     
        $start      = ( ( $this->_page - $links ) > 0 ) ? $this->_page - $links : 1;
        $end        = ( ( $this->_page + $links ) < $last ) ? $this->_page + $links : $last;
     
        $html       = '<p class="' . $list_class . '">';
     
        $class      = ( $this->_page == 1 ) ? "disabled" : "";
        $html       .= '<li class="' . $class . '"><a href="page-' . ( $this->_page - 1 ) . '.html">&laquo;</a></li>';
     
        if ( $start > 1 ) {
            $html   .= '<li><a href="page-1.html">1</a></li>';
            $html   .= '<li class="disabled"><span>...</span></li>';
        }
     
        for ( $i = $start ; $i <= $end; $i++ ) {
            $class  = ( $this->_page == $i ) ? "active" : "";
            $html   .= '<li class="' . $class . '"><a href="page-' . $i . '.html">' . $i . '</a></li>';
        }
     
        if ( $end < $last ) {
            $html   .= '<li class="disabled"><span>...</span></li>';
            $html   .= '<li><a href="page-' . $last . '.html">' . $last . '</a></li>';
        }
     
        $class      = ( $this->_page == $last ) ? "disabled" : "";
        $html       .= '<li class="' . $class . '"><a href="page-' . ( $this->_page + 1 ) . '.html">&raquo;</a></li>';
     
        $html       .= '</ul>';
     
        return $html;
    }

    public function createLinksCustom( $slug, $links, $list_class, $query = '' ) {
        if ( $this->_limit == 'all' ) {
            return '';
        }
     
        $last       = ceil( $this->_total / $this->_limit );
     
        $start      = ( ( $this->_page - $links ) > 0 ) ? $this->_page - $links : 1;
        $end        = ( ( $this->_page + $links ) < $last ) ? $this->_page + $links : $last;
     
        $html       = '<p class="' . $list_class . '">';
     
        $class      = ( $this->_page == 1 ) ? "disabled" : "";
        if($start != 1) {
            $html       .= '<li class="' . $class . '"><a href="page-' . ( $this->_page - 1 ) . $slug .'.html'.$query.'">&laquo;</a></li>';
        }
        if ( $start > 1 ) {
            $html   .= '<li><a href="page-1' .$slug . '.html'.$query.'">1</a></li>';
            $html   .= '<li class="disabled"><span>...</span></li>';
        }
     
        for ( $i = $start ; $i <= $end; $i++ ) {
            $class  = ( $this->_page == $i ) ? "active" : "";
            $html   .= '<li class="' . $class . '"><a href="page-' . $i . $slug .'.html'.$query.'">' . $i . '</a></li>';
        }
     
        if ( $end < $last ) {
            $html   .= '<li class="disabled"><span>...</span></li>';
            $html   .= '<li><a href="page-' . $last . $slug .'.html'.$query.'">' . $last . '</a></li>';
        }
     
        $class      = ( $this->_page == $last ) ? "disabled" : "";
        if($end != $last) {
            $html       .= '<li class="' . $class . '"><a href="page-' . ( $this->_page + 1 ) . $slug .'.html'.$query.'">&raquo;</a></li>';
        }
        
     
        $html       .= '</ul>';
     
        return $html;
    }

    public function createLinksFilter( $slug, $links, $list_class ) {
        if ( $this->_limit == 'all' ) {
            return '';
        }
     
        $last       = ceil( $this->_total / $this->_limit );
     
        $start      = ( ( $this->_page - $links ) > 0 ) ? $this->_page - $links : 1;
        $end        = ( ( $this->_page + $links ) < $last ) ? $this->_page + $links : $last;
     
        $html       = '<p class="' . $list_class . '">';
     
        $class      = ( $this->_page == 1 ) ? "disabled" : "";
        if($start != 1) {
            $html       .= '<li class="' . $class . '"><a href="' .  $slug .( $this->_page - 1 ) .'">&laquo;</a></li>';
        }
        if ( $start > 1 ) {
            $html   .= '<li><a href="' .$slug . '1">1</a></li>';
            $html   .= '<li class="disabled"><span>...</span></li>';
        }
     
        for ( $i = $start ; $i <= $end; $i++ ) {
            $class  = ( $this->_page == $i ) ? "active" : "";
            $html   .= '<li class="' . $class . '"><a href="' . $slug . $i .'">' . $i . '</a></li>';
        }
     
        if ( $end < $last ) {
            $html   .= '<li class="disabled"><span>...</span></li>';
            $html   .= '<li><a href="' . $slug . $last .'">' . $last . '</a></li>';
        }
     
        $class      = ( $this->_page == $last ) ? "disabled" : "";
        if($end != $last) {
            $html       .= '<li class="' . $class . '"><a href="' . $slug . ( $this->_page + 1 ) .'">&raquo;</a></li>';
        }
        
     
        $html       .= '</ul>';
     
        return $html;
    }
}