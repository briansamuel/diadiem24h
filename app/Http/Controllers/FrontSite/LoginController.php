<?php

namespace App\Http\Controllers\FrontSite;

use App\Helpers\Message;
use App\Models\GuestModel;
use App\Models\GuestPasswordReset;
use App\Notifications\ResetPasswordRequest;
use App\Services\BannerService;
use App\Services\Frontsite\AuthService;
use App\Services\GuestService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Str;

class LoginController extends Controller
{

    protected $request;
    protected $loginService;
    protected $guestService;

    function __construct(Request $request, GuestService $guestService)
    {
        $this->request = $request;
        $this->guestService = $guestService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function login()
    {
        if(Auth::guard('user')->check()) {
            return redirect('/');
        }
        $banners = BannerService::getForFrontEnd('login');

        return view('frontsite.users.login', compact('banners'));
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function loginAction(Request $request)
    {
        $loginAdmin = AuthService::loginGuest($request);

        return $loginAdmin;
    }

    public function registerAction(Request $request)
    {
        $params = $request->only('gender', 'full_name', 'email', 'password', 'guest_phone');
        if(GuestService::getByEmail($params['email'])) {
            return response()->json(['status' => false, 'msg' => 'Email này đã tồn tại trong hệ thống!']);
        }
        $params['username'] = explode('@', $params['email'])[0];

        $add = GuestService::register($params);
        if ($add) {
            $this->guestService->sendMailActive($params['email']);
            return response()->json(['status' => true, 'msg' => 'Chúng tôi gửi cho bạn một mã kích hoạt. Vui lòng kiểm tra email của bạn và nhấp vào liên kết để kích hoạt!']);
        }

        return redirect()->to('/');
    }

    public function activeGuest(Request $request)
    {
        $active_code = $request->input('active_code');
        if ($active_code == '') {
            return view('errors.404');
        }
        $guestInfo = GuestModel::where('active_code', $active_code)->first();
        if (!$guestInfo || $guestInfo->status !== 'inactive') {
            return view('errors.404');
        }

        $active = $this->guestService->active($guestInfo->id);
        if ($active) {
            Auth::guard('user')->loginUsingId($guestInfo->id);
        } else {
            Message::alertFlash('Bạn đã kích hoạt tài khoản không thành công!', 'danger');
        }


        return redirect()->to('//'.Config::get('domain.web.domain').'/dang-nhap');
    }

    public function sendMailResetPassword(Request $request)
    {
        $user = GuestService::getUserInfoByEmail($request->forgot_email);
        if (!$user) {
            return response()->json([
                'success' => false,
                'message' => 'Email này không tồn tại trong hệ thống!'
            ]);
        }
        $passwordReset = GuestPasswordReset::updateOrCreate([
            'email' => $user->email,
            'token' => Str::random(60)
        ]);
        if ($passwordReset) {
            $user->notify(new ResetPasswordRequest($passwordReset->token));
        }

        return response()->json([
            'success' => true,
            'message' => 'Chúng tôi đã gửi email liên kết đặt lại mật khẩu cho bạn!'
        ]);
    }

    public function resetPassword(Request $request)
    {
        $token = $request->input('token');
        $passwordReset = GuestPasswordReset::where('token', $token)->first();
        if (!$passwordReset) {
            return view('errors.404');
        }
        return view('frontsite.users.reset_password', ['token' => $token]);
    }

    public function reset(Request $request)
    {
        $token = $request->input('token');
        $passwordReset = GuestPasswordReset::where('token', $token)->first();
        // 12 minutes

        if ($passwordReset) {
            if(Carbon::parse($passwordReset->updated_at)->addMinutes(720)->isPast()) {
                $passwordReset->delete();

                return response()->json([
                    'success' => false,
                    'message' => 'Mã token đặt lại mật khẩu đã hết hạn.',
                ]);
            }

            GuestService::updatePasswordByEmail($passwordReset->email, $request->input('password'));
            $passwordReset->delete();
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Mã token đặt lại mật khẩu không hợp lệ.',
            ]);
        }


        return response()->json([
            'success' => true,
            'message' => 'Mật khẩu đã được đặt lại thành công'
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function logout()
    {
        AuthService::logout();

        return redirect('/');
    }

}
