<?php

namespace App\Http\Controllers\FrontSite;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\PlaceService;
use App\Services\ProvinceService;
use App\Services\DistrictService;
use App\Services\AreaService;
use App\Services\CareerService;
use App\Services\ReviewService;

use App\Helpers\Pagination;

use View;
use App;
use Auth;
use Session;

class PlaceController extends Controller
{
    //
    protected $request;
    protected $placeService;
    protected $provinceService;

    function __construct(Request $request, PlaceService $placeService)
    {
        $this->request = $request;
        $this->placeService = $placeService;
    }


    /**
     * ======================
     * Method:: View Home
     * ======================
     */

    public function index($page = 1)
    {
        //check permission
        $pagination = array(
            'perpage' => 20,
            'page'    => $page,
        );
        $provinces = ProvinceService::getAll();
        $careers = CareerService::getMany(['*'], null, null, array('parent' => 0));

        

        $places = $this->placeService->getMany(['id', 'thumbnail', 'title', 'slug', 'address', 'price', 'phone', 'province', 'district', 'area', 'career', 'rating_value'], $pagination, null, null);
        foreach ($places as $key => $place) {
            $province = ProvinceService::findByKey('id', $place->province);
            $district = DistrictService::findByKey('id', $place->district);
            $area = AreaService::findByKey('id', $place->area);
            $career = CareerService::findByKey('id', $place->career);
            $place->province_name = $province->name;
            $place->province_code = $province->code;
            $place->district_name = $district->name;
            $place->district_code = $district->code;
            $place->area_name = $area->name;
            $place->area_code = $area->code;

            if ($place->career != 0) {
                $place->career_name = $career->name;
                $place->career_slug = $career->slug;
            } else {
                $place->career_name = '';
                $place->career_slug = '';
            }
        }
        $totalPlaces = $this->placeService->totalRows([]);
        Session::flash('total_place', $totalPlaces);
        $pagination = new Pagination();
        $pagination->paginate($page, 20, $totalPlaces);
        $renderPagination = $pagination->createLinks(3, 'page-nav');

        foreach ($provinces as $key => $item_province) {
            $countPlace = $this->placeService->totalRows(array('province' => $item_province->id));
            $item_province->count = $countPlace;
        }
        // dd($provinces);
        return view('frontsite.home.index', compact('places', 'provinces', 'careers', 'renderPagination'));
    }




    /**
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function detail($slug, $id)
    {

        $place = $this->placeService->findByKey('id', $id);
        if ($place->slug != $slug) {
            return abort(404);
        }
        $province = ProvinceService::findByKey('id', $place->province);
        $district = DistrictService::findByKey('id', $place->district);
        $area = AreaService::findByKey('id', $place->area);
        $career = CareerService::findByKey('id', $place->career);
        $place->province_name = isset($province->name) ? $province->name : '';
        $place->province_code = isset($province->code) ? $province->code : '';
        $place->district_name = isset($district->name) ? $district->name : '';
        $place->district_code = isset($district->code) ? $district->code : '';
        $place->area_name = $area->name;
        $place->area_code = $area->code;
        $reviews = ReviewService::getMany(['*'], array('perpage' => 50, 'page' => 1), null, array('place' => $place->id));

        if ($place->career != 0) {
            $place->career_name = $career->name;
            $place->career_slug = $career->slug;
        } else {
            $place->career_name = '';
            $place->career_slug = '';
        }


        if ($career->parent == 0) {
            $load_more = "tim-ve-{$career->slug}-tai-{$province->code}.html";
            $career_args = CareerService::getMany(['*'], null, null, array('parent' => $career->id));
        } else {
            $load_more = "tim-cho-{$career->slug}-tai-{$province->code}.html";
            $career_args = CareerService::getMany(['*'], null, null, array('parent' => $career->parent));
        }
        $career_ids =  $career_args->map(function ($c) {
            return $c->id;
        })->toArray();

        $pagination = array(
            'perpage' => 20,
            'page'    => 1,
        );

        $places = $this->placeService->getMany(['id', 'thumbnail', 'title', 'slug', 'address', 'price', 'phone', 'province', 'district', 'area', 'career', 'rating_value', 'created_at'], $pagination, null, ['province' => $province->id, 'career_ids' => $career_ids]);
        $place_ids =  $places->map(function ($c) {
            return $c->id;
        })->toArray();

        $countReviewPlace = ReviewService::countReviewsByPlace($place_ids)->toArray();
        $reviewPlaceArgs = [];
        foreach ($countReviewPlace as $item) {
            $reviewPlaceArgs[$item->place] = $item->count;
        }

        foreach ($places as $key => $p) {
            $province = ProvinceService::findByKey('id', $place->province);
            $district = DistrictService::findByKey('id', $place->district);
            $area = AreaService::findByKey('id', $place->area);
            $career = CareerService::findByKey('id', $place->career);
            // $reviewCount = ReviewService::totalRows(array('place' => $place->id));
            $p->province_name = $p->province != 0 ? $province->name : '';
            $p->province_code = $p->province != 0 ? $province->code : '';
            $p->district_name = $p->district != 0 ? $district->name : '';
            $p->district_code = $p->district != 0 ? $district->code : '';
            $p->area_name = $p->area != 0 ? $area->name : '';
            $p->area_code = $p->area != 0 ? $area->code : '';

            if ($p->career != 0) {
                $p->career_name = $career->name;
                $p->career_slug = $career->slug;
            } else {
                $p->career_name = '';
                $p->career_slug = '';
            }

            if (isset($reviewPlaceArgs[$place->id])) {
                $p->reviewCount = $reviewPlaceArgs[$place->id];
            } else {
                $p->reviewCount = 0;
            }
        }

        $totalPlaces = $this->placeService->totalRows([]);
        Session::flash('total_place', $totalPlaces);
        return view('frontsite.places.detail', compact('place', 'places', 'reviews', 'load_more'));
    }


    public function byProvince($slug)
    {
        //check permission
        $input = $this->request->only('r', 'b', 'p');
        $rating = isset($input['r']) ? intval($input['r']) : null;
        $branch = isset($input['b']) ? intval($input['b']) : null;
        $page = 1;
        $pagination = array(
            'perpage' => 20,
            'page'    => $page,
        );


        $districts = [];
        $province_curent = $province = $district = $area = null;
        $province = ProvinceService::findByKey('code', $slug);
        $field = 'province';
        if($province) {
            $districts = DistrictService::getMany(['id', 'name', 'code'], null, null, array('province' => $province->id));
            $province_id = array($province->id);
            $province_current = $province;
        }
        
        if (empty($province)) {
            $district = DistrictService::findByKey('code', $slug);
            $field = 'district';
            if($district) {
                $districts = DistrictService::getMany(['id', 'name', 'code'], null, null, array('province' => $district->province));
                $province = ProvinceService::findByKey('id', $district->province);
            }
            
            $province_id = array($district->id);
            $province_current = $district;
            if (empty($district)) {
                $area = AreaService::findByKey('code', $slug);
                $field = 'area';

                $province_id = array($area->id);
                $province_curent = $area;
                if (empty($area)) {
                    return abort('404');
                }
            }
        }
        

        $places = $this->placeService->getMany(['id', 'thumbnail', 'title', 'slug', 'address', 'price', 'phone', 'province', 'district', 'area', 'career', 'rating_value', 'created_at'], $pagination, null, [$field => $province_id, 'rating' => $rating]);

        foreach ($places as $key => $place) {
            $p = ProvinceService::findByKey('id', $place->province);
            $d = DistrictService::findByKey('id', $place->district);
            $a = AreaService::findByKey('id', $place->area);
            $c = CareerService::findByKey('id', $place->career);
            $reviewCount = ReviewService::totalRows(array('place' => $place->id));
            $place->province_name = $p->name;
            $place->province_code = $p->code;
            $place->district_name = $place->district != 0 ? $d->name : '';
            $place->district_code = $place->district != 0 ? $d->code : '';
            $place->area_name = $place->area != 0 ? $a->name : '';
            $place->area_code = $place->area != 0 ? $a->code : '';

            if ($place->career != 0) {
                $place->career_name = $c->name;
                $place->career_slug = $c->slug;
            } else {
                $place->career_name = '';
                $place->career_slug = '';
            }

            $place->reviewCount = $reviewCount;
        }
        $totalPlaces = $this->placeService->totalRows(['province' => $province_id, 'rating' => $rating]);
        Session::flash('total_place', $totalPlaces);
        $query = [];
        if (isset($rating)) {
            $query[] = 'r=' . $rating;
        }
        if (isset($branch)) {
            $query[] = 'b=' . $branch;
        }
        if (isset($phone)) {
            $query[] = 'p=' . $phone;
        }
       

        $slug_link = '';
        if (count($query) > 0) {
            $slug_link = '?' . implode('&', $query);
        }
        $pagination = new Pagination();
        $pagination->paginate($page, 20, $totalPlaces);
        $renderPagination = $pagination->createLinksCustom('-' . $province_current->code, 3, 'page-nav', $slug_link);

        
        return view('frontsite.provinces.detail', compact('places', 'provinces', 'districts', 'careers', 'renderPagination', 'province', 'district', 'area', 'province_current'));
    }

    public function byProvincePage($page = 1, $slug)
    {

        $input = $this->request->only('r', 'b', 'p');
        $rating = isset($input['r']) ? intval($input['r']) : null;
        $branch = isset($input['b']) ? intval($input['b']) : null;
        //check permission
        $pagination = array(
            'perpage' => 20,
            'page'    => $page,
        );

        $districts = [];
        $province_curent = $province = $district = $area = null;
        $province = ProvinceService::findByKey('code', $slug);
        $field = 'province';
        if($province) {
            $districts = DistrictService::getMany(['id', 'name', 'code'], null, null, array('province' => $province->id));
            $province_id = array($province->id);
            $province_current = $province;
        }
        
        if (empty($province)) {
            $district = DistrictService::findByKey('code', $slug);
            $field = 'district';
            if($district) {
                $districts = DistrictService::getMany(['id', 'name', 'code'], null, null, array('province' => $district->province));
                $province = ProvinceService::findByKey('id', $district->province);
            }
            
            $province_id = array($district->id);
            $province_current = $district;
            if (empty($district)) {
                $area = AreaService::findByKey('code', $slug);
                $field = 'area';

                $province_id = array($area->id);
                $province_curent = $area;
                if (empty($area)) {
                    return abort('404');
                }
            }
        }
        

        $places = $this->placeService->getMany(['id', 'thumbnail', 'title', 'slug', 'address', 'price', 'phone', 'province', 'district', 'area', 'career', 'rating_value', 'created_at'], $pagination, null, [$field => $province_id, 'rating' => $rating]);

        foreach ($places as $key => $place) {
            $p = ProvinceService::findByKey('id', $place->province);
            $d = DistrictService::findByKey('id', $place->district);
            $a = AreaService::findByKey('id', $place->area);
            $c = CareerService::findByKey('id', $place->career);
            $reviewCount = ReviewService::totalRows(array('place' => $place->id));
            $place->province_name = $p->name;
            $place->province_code = $p->code;
            $place->district_name = $place->district != 0 ? $d->name : '';
            $place->district_code = $place->district != 0 ? $d->code : '';
            $place->area_name = $place->area != 0 ? $a->name : '';
            $place->area_code = $place->area != 0 ? $a->code : '';

            if ($place->career != 0) {
                $place->career_name = $c->name;
                $place->career_slug = $c->slug;
            } else {
                $place->career_name = '';
                $place->career_slug = '';
            }

            $place->reviewCount = $reviewCount;
        }
        $totalPlaces = $this->placeService->totalRows(['province' => $province_id, 'rating' => $rating]);
        Session::flash('total_place', $totalPlaces);
        $query = [];
        if (isset($rating)) {
            $query[] = 'r=' . $rating;
        }
        if (isset($branch)) {
            $query[] = 'b=' . $branch;
        }
        if (isset($phone)) {
            $query[] = 'p=' . $phone;
        }
       

        $slug_link = '';
        if (count($query) > 0) {
            $slug_link = '?' . implode('&', $query);
        }
        $pagination = new Pagination();
        $pagination->paginate($page, 20, $totalPlaces);
        $renderPagination = $pagination->createLinksCustom('-' . $province->code, 3, 'page-nav', $slug_link);

        return view('frontsite.provinces.detail', compact('places', 'provinces', 'districts', 'careers', 'renderPagination', 'province', 'district', 'area', 'province_current'));
    }

    public function filterRank()
    {

        $input = $this->request->only('r', 'b', 'p', 'n');
        $rating = isset($input['r']) ? intval($input['r']) : null;
        $branch = isset($input['b']) ? intval($input['b']) : null;
        $phone = isset($input['p']) ? intval($input['p']) : null;
        $page = isset($input['n']) ? intval($input['n']) : 1;
        $pagination = array(
            'perpage' => 20,
            'page'    => $page,
        );




        $places = $this->placeService->getMany(['id', 'thumbnail', 'title', 'slug', 'address', 'price', 'phone', 'province', 'district', 'area', 'career', 'rating_value', 'created_at'], $pagination, null, ['rating' => $rating]);

        foreach ($places as $key => $place) {
            $p = ProvinceService::findByKey('id', $place->province);
            $d = DistrictService::findByKey('id', $place->district);
            $a = AreaService::findByKey('id', $place->area);
            $c = CareerService::findByKey('id', $place->career);
            $reviewCount = ReviewService::totalRows(array('place' => $place->id));
            $place->province_name = $p->name;
            $place->province_code = $p->code;
            $place->district_name = $place->district != 0 ? $d->name : '';
            $place->district_code = $place->district != 0 ? $d->code : '';
            $place->area_name = $place->area != 0 ? $a->name : '';
            $place->area_code = $place->area != 0 ? $a->code : '';

            if ($place->career != 0) {
                $place->career_name = $c->name;
                $place->career_slug = $c->slug;
            } else {
                $place->career_name = '';
                $place->career_slug = '';
            }

            $place->reviewCount = $reviewCount;
        }
        $totalPlaces = $this->placeService->totalRows(['rating' => $rating]);
        Session::flash('total_place', $totalPlaces);
        $pagination = new Pagination();
        $query = [];
        if (isset($rating)) {
            $query[] = 'r=' . $rating;
        }
        if (isset($branch)) {
            $query[] = 'b=' . $branch;
        }
        if (isset($phone)) {
            $query[] = 'p=' . $phone;
        }
        if (isset($page)) {
            $query[] = 'n=';
        }

        $uri = $this->request->path();
        $slug_link = '';
        if (count($query) > 1) {
            $slug_link = $uri . '?' . implode('&', $query);
        } else {
            $slug_link = $uri . '?';
        }



        $pagination->paginate($page, 20, $totalPlaces);
        $renderPagination = $pagination->createLinksFilter($slug_link, 3, 'page-nav');

        $title = "Các địa điểm có đánh giá";
        return view('frontsite.search.index', compact('places', 'renderPagination', 'title'));
    }

    public function filterPC($career_slug, $province_slug)
    {

        $input = $this->request->only('r', 'b', 'p', 'n');
        $rating = isset($input['r']) ? intval($input['r']) : null;
        $branch = isset($input['b']) ? intval($input['b']) : null;
        $phone = isset($input['p']) ? intval($input['p']) : null;
        $page = isset($input['n']) ? intval($input['n']) : 1;
        $pagination = array(
            'perpage' => 20,
            'page'    => $page,
        );
        $province = isset($province_slug) ? ProvinceService::findByKey('code', $province_slug) : null;
        $career = isset($career_slug) ? CareerService::findByKey('slug', $career_slug) : null;
        if (!$province || !$career)
            return abort('404');

        $districts = DistrictService::getMany(['id', 'name', 'code'], null, null, array('province' => $province->id));
        $careers_child = [];
        if ($career->parent != 0) {
            $career_id = array($career->id);
            $careers_child = CareerService::getMany(['id', 'name'], null, null, array('parent' => $career->parent));
        } else {

            $careers_child = CareerService::getMany(['id', 'name'], null, null, array('parent' => $career->id));
            $career_id =  $careers_child->map(function ($c) {
                return $c->id;
            })->toArray();
        }

        $places = $this->placeService->getMany(['id', 'thumbnail', 'title', 'slug', 'address', 'price', 'phone', 'province', 'district', 'area', 'career', 'rating_value', 'created_at'], $pagination, null, ['province' => $province->id, 'career_ids' => $career_id, 'rating' => $rating]);

        foreach ($places as $key => $place) {
            $p = ProvinceService::findByKey('id', $place->province);
            $d = DistrictService::findByKey('id', $place->district);
            $a = AreaService::findByKey('id', $place->area);
            $c = CareerService::findByKey('id', $place->career);
            $reviewCount = ReviewService::totalRows(array('place' => $place->id));
            $place->province_name = $p->name;
            $place->province_code = $p->code;
            $place->district_name = $place->district != 0 ? $d->name : '';
            $place->district_code = $place->district != 0 ? $d->code : '';
            $place->area_name = $place->area != 0 ? $a->name : '';
            $place->area_code = $place->area != 0 ? $a->code : '';

            if ($place->career != 0) {
                $place->career_name = $c->name;
                $place->career_slug = $c->slug;
            } else {
                $place->career_name = '';
                $place->career_slug = '';
            }

            $place->reviewCount = $reviewCount;
        }
        $totalPlaces = $this->placeService->totalRows(['province' => $province->id, 'career_ids' => $career_id, 'rating' => $rating]);
        Session::flash('total_place', $totalPlaces);
        $pagination = new Pagination();
        $query = [];
        if (isset($rating)) {
            $query[] = 'r=' . $rating;
        }
        if (isset($branch)) {
            $query[] = 'b=' . $branch;
        }
        if (isset($phone)) {
            $query[] = 'p=' . $phone;
        }
        if (isset($page)) {
            $query[] = 'n=';
        }

        $uri = $this->request->path();
        $slug_link = '';
        if (count($query) > 1) {
            $slug_link = $uri . '?' . implode('&', $query);
        } else {
            $slug_link = $uri . '?';
        }



        $pagination->paginate($page, 20, $totalPlaces);
        $renderPagination = $pagination->createLinksFilter($slug_link, 3, 'page-nav');

        $title = "Các địa điểm {$career->name} {$province->name}";
        return view('frontsite.search.index', compact('places', 'districts', 'careers_child', 'province', 'career',  'renderPagination', 'title'));
    }

    public function filterDC($career, $dis)
    {
        $page = $this->request->input('page');
        $page = isset($page) ? intval($page) : 1;
        $input = $this->request->only('r', 'b', 'p');
        $rating = isset($input['r']) ? intval($input['r']) : null;
        $branch = isset($input['b']) ? intval($input['b']) : null;
        $pagination = array(
            'perpage' => 20,
            'page'    => $page,
        );

        $district = DistrictService::findByKey('code', $dis);

        $career = CareerService::findByKey('slug', $career);
        if (!$district || !$career)
            return abort('404');
        $province = (object) array('id' => $district->province);

        $districts = DistrictService::getMany(['id', 'name', 'code'], null, null, array('province' => $district->province));

        $careers_child = [];
        if ($career->parent != 0) {
            $career_id = array($career->id);
            $careers_child = CareerService::getMany(['id', 'name', 'slug', 'parent'], null, null, array('parent' => $career->parent));
        } else {
            $careers_child = CareerService::getMany(['id', 'name', 'slug', 'parent'], null, null, array('parent' => $career->id));
            $career_args = CareerService::getMany(['id'], null, null, array('parent' => $career->id));
            $career_id =  $career_args->map(function ($c) {
                return $c->id;
            })->toArray();
        }

        $places = $this->placeService->getMany(['id', 'thumbnail', 'title', 'slug', 'address', 'price', 'phone', 'province', 'district', 'area', 'career', 'rating_value', 'created_at'], $pagination, null, ['district' => $district->id, 'career_ids' => $career_id, 'rating' => $rating]);

        foreach ($places as $key => $place) {
            $p = ProvinceService::findByKey('id', $place->province);
            $d = DistrictService::findByKey('id', $place->district);
            $a = AreaService::findByKey('id', $place->area);
            $c = CareerService::findByKey('id', $place->career);
            $reviewCount = ReviewService::totalRows(array('place' => $place->id));
            $place->province_name = $p->name;
            $place->province_code = $p->code;
            $place->district_name = $place->district != 0 ? $d->name : '';
            $place->district_code = $place->district != 0 ? $d->code : '';
            $place->area_name = $place->area != 0 ? $a->name : '';
            $place->area_code = $place->area != 0 ? $a->code : '';

            if ($place->career != 0) {
                $place->career_name = $c->name;
                $place->career_slug = $c->slug;
            } else {
                $place->career_name = '';
                $place->career_slug = '';
            }

            $place->reviewCount = $reviewCount;
        }
        $totalPlaces = $this->placeService->totalRows(['district' => $district->id, 'career_ids' => $career_id, 'rating' => $rating]);
        Session::flash('total_place', $totalPlaces);
        $pagination = new Pagination();
        $pagination->paginate($page, 20, $totalPlaces);
        $renderPagination = $pagination->createLinksCustom('-', 3, 'page-nav');

        $title = "Các địa điểm {$career->name} {$district->name}";
        return view('frontsite.search.index', compact('places', 'provinces', 'districts', 'careers', 'careers_child', 'province', 'district', 'career', 'renderPagination', 'title'));
    }
}
