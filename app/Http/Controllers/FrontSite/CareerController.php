<?php

namespace App\Http\Controllers\FrontSite;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\PlaceService;
use App\Services\ProvinceService;
use App\Services\DistrictService;
use App\Services\AreaService;
use App\Services\CareerService;
use App\Helpers\Pagination;
use App\Services\ReviewService;
use Session;

class CareerController extends Controller
{
    //
    protected $request;
    protected $placeService;
    protected $provinceService;

    function __construct(Request $request, PlaceService $placeService)
    {
        $this->request = $request;
        $this->placeService = $placeService;

    }


    /**
     * ======================
     * Method:: View Home
     * ======================
     */

    public function index($page = 1)
    {
        $provinces = ProvinceService::getAll(['id', 'name', 'code']);
        $careers = CareerService::getMany(['id','name', 'slug', 'parent'], ['page' => 1, 'perpage' => 100], null, null);
        $career_parent = array();
        foreach($careers as $career) {
            $countPlace_review = $this->placeService->totalRows(array('career' => $career->id));
            if($career->parent == 0) {
                $career_parent[$career->id] = 0;
                $career->count = $countPlace_review;
                
            }
            else {
                $career_parent[$career->parent] += $countPlace_review;
                $career->count = $countPlace_review;
                
            }
            
            
        }
        foreach($careers as $career) {
            if($career->parent == 0) {
                $career->count = $career_parent[$career->id];
            }
        }
        foreach($provinces as $key => $item_province) {
            $countPlace = $this->placeService->totalRows(array('province' => $item_province->id));
            $item_province->count = $countPlace;
        }
       
        return view('frontsite.careers.index', compact('places', 'provinces', 'careers'));
    }



    /**
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function detail($slug)
    {   
        $input = $this->request->only('r', 'b', 'p');
        $rating = isset($input['r']) ? intval($input['r']) : null;
        $branch = isset($input['b']) ? intval($input['b']) : null;
        //check permission
        $page = 1;
        $pagination = array(
            'perpage' => 20,
            'page'    => $page,
        );
        
        $provinces = ProvinceService::getAll(['id', 'name', 'code']);
        $careers = CareerService::getMany(['*'], null, null, array('parent' => 0));
        
        $career_current = CareerService::findByKey('slug', $slug);
        if(!$career_current) {
            return redirect('404');
        }
        if($career_current->parent != 0) {
            $career_id = array($career_current->id);
        } else {

            $career_args = CareerService::getMany(['id'], null, null, array('parent' => $career_current->id));
            $career_id =  $career_args->map(function($c) { 
                return $c->id;
            })->toArray();
        }

        $places = $this->placeService->getMany(['id', 'thumbnail', 'title', 'slug', 'address','price', 'phone', 'province', 'district', 'area', 'career', 'rating_value', 'created_at'], $pagination, null, ['career_ids' => $career_id, 'rating' => $rating]);
        
        foreach($places as $key => $place) {
            $province = ProvinceService::findByKey('id', $place->province);
            $district = DistrictService::findByKey('id', $place->district);
            $area = AreaService::findByKey('id', $place->area);
            $career = CareerService::findByKey('id', $place->career);
            $reviewCount = ReviewService::totalRows(array('place' => $place->id));
            $place->province_name = $province->name;
            $place->province_code = $province->code;
            if($place->district != 0) {
                $place->district_name = $district->name;
                $place->district_code = $district->code;
            } else {
                $place->district_name = '';
                $place->district_code = '';
            }
            
            if($place->area != 0) {
                $place->area_name = $area->name;
                $place->area_code = $area->code;
            } else {
                $place->area_name = '';
                $place->area_code = '';
            }

            if($place->career != 0) {
                $place->career_name = $career->name;
                $place->career_slug = $career->slug;
            } else {
                $place->career_name = '';
                $place->career_slug = '';
            }

            $place->reviewCount = $reviewCount;
            
            
        }
        $totalPlaces = $this->placeService->totalRows( ['career_ids' => $career_id, 'rating' => $rating]);
        Session::flash('total_place', $totalPlaces);
        $query = [];
        if (isset($rating)) {
            $query[] = 'r=' . $rating;
        }
        if (isset($branch)) {
            $query[] = 'b=' . $branch;
        }
        if (isset($phone)) {
            $query[] = 'p=' . $phone;
        }
       

        $slug_link = '';
        if (count($query) > 0) {
            $slug_link = '?' . implode('&', $query);
        }
        $pagination = new Pagination();
        $pagination->paginate($page, 20, $totalPlaces);
        $renderPagination = $pagination->createLinksCustom('-nganh-nghe-'.$career_current->slug, 3, 'page-nav', $slug_link);
        $countPlaceProvinces = $this->placeService->countPlaceByProvince(array('groupby' => 'province'))->toArray();
        $provinceArgs = [];
        foreach($countPlaceProvinces as $item) {
            $provinceArgs[$item->province] = $item->count;
        }
        foreach($provinces as $key => $item_province) {
            
            if(isset($provinceArgs[$item_province->id])) {
                $item_province->count = $provinceArgs[$item_province->id];
            } else {
                $item_province->count = 0;
            }
            
        }
        
        return view('frontsite.careers.detail', compact('places', 'provinces', 'careers', 'renderPagination', 'career_current'));

    }

    // Detail has Page
    public function detailPage($page = 1, $slug)
    {   

        $input = $this->request->only('r', 'b', 'p');
        $rating = isset($input['r']) ? intval($input['r']) : null;
        $branch = isset($input['b']) ? intval($input['b']) : null;
        //check permission
        $pagination = array(
            'perpage' => 20,
            'page'    => $page,
        );
        
        $provinces = ProvinceService::getAll(['id', 'name', 'code']);
        $careers = CareerService::getMany(['*'], null, null, array('parent' => 0));
        
        $career_current = CareerService::findByKey('slug', $slug);
        if(!$career_current) {
            return redirect('404');
        }
        if($career_current->parent != 0) {
            $career_id = array($career_current->id);
        } else {

            $career_args = CareerService::getMany(['id'], null, null, array('parent' => $career_current->id));
            $career_id =  $career_args->map(function($c) { 
                return $c->id;
            })->toArray();
        }
        
        $places = $this->placeService->getMany(['id', 'thumbnail', 'title', 'slug', 'address','price', 'phone', 'province', 'district', 'area', 'career', 'rating_value', 'created_at'], $pagination, null, ['career_ids' => $career_id, 'rating' => $rating]);
        
        foreach($places as $key => $place) {
            $province = ProvinceService::findByKey('id', $place->province);
            $district = DistrictService::findByKey('id', $place->district);
            $area = AreaService::findByKey('id', $place->area);
            $career = CareerService::findByKey('id', $place->career);
            $reviewCount = ReviewService::totalRows(array('place' => $place->id));
            $place->province_name = $province->name;
            $place->province_code = $province->code;
            if($place->district != 0) {
                $place->district_name = $district->name;
                $place->district_code = $district->code;
            } else {
                $place->district_name = '';
                $place->district_code = '';
            }
           
            if($place->area != 0) {
                $place->area_name = $area->name;
                $place->area_code = $area->code;
            } else {
                $place->area_name = '';
                $place->area_code = '';
            }

            if($place->career != 0) {
                $place->career_name = $career->name;
                $place->career_slug = $career->slug;
            } else {
                $place->career_name = '';
                $place->career_slug = '';
            }

            $place->reviewCount = $reviewCount;
            
            
        }
        $totalPlaces = $this->placeService->totalRows( ['career_ids' => $career_id, 'rating' => $rating]);
        Session::flash('total_place', $totalPlaces);
        $query = [];
        if (isset($rating)) {
            $query[] = 'r=' . $rating;
        }
        if (isset($branch)) {
            $query[] = 'b=' . $branch;
        }
        if (isset($phone)) {
            $query[] = 'p=' . $phone;
        }
       

        $slug_link = '';
        if (count($query) > 0) {
            $slug_link = '?' . implode('&', $query);
        }
        $pagination = new Pagination();
        $pagination->paginate($page, 20, $totalPlaces);
        $renderPagination = $pagination->createLinksCustom('-nganh-nghe-'.$career_current->slug, 3, 'page-nav', $slug_link);
        $countPlaceProvinces = $this->placeService->countPlaceByProvince(array('groupby' => 'province'))->toArray();
        $provinceArgs = [];
        foreach($countPlaceProvinces as $item) {
            $provinceArgs[$item->province] = $item->count;
        }
        foreach($provinces as $key => $item_province) {
            
            if(isset($provinceArgs[$item_province->id])) {
                $item_province->count = $provinceArgs[$item_province->id];
            } else {
                $item_province->count = 0;
            }
            
        }
        
        return view('frontsite.careers.detail', compact('places', 'provinces', 'careers', 'renderPagination', 'career_current'));

    }

    

    

   
}
