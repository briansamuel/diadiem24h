<?php

namespace App\Http\Controllers\FrontSite;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\PageService;
use App\Services\PlaceService;
use App\Services\ProvinceService;
use App\Services\DistrictService;
use App\Services\AreaService;
use App\Services\CareerService;
use App;
class PageController extends Controller
{
    //
    protected $request;
    protected $pageService;
    protected $locale;
    function __construct(Request $request, PageService $pageService)
    {
        $this->request = $request;
        $this->pageService = $pageService;
        
        
    }


    /**
     * ======================
     * Method:: View Home
     * ======================
     */

    public function index($slug)
    {   
        
        $page = $this->pageService->findByKey('page_slug', $slug);
        if (!$page || $page->page_status !== 'publish') {
            abort(404);
        }

        $data['page'] = $page;
  
        if($page) {
           
            switch ($page->page_type) {
                case 'page':
                    if($page->page_template == 'about-page') {
                        
                        return view('frontsite.pages.'.$page->page_template, $data);
                    }
                    return view('frontsite.pages.'.$page->page_template, $data);
                case 'service':
                    return view('frontsite.services.'.$page->page_template, $data);
                default:
                    return view('frontsite.pages.default', $data);
                    
    
            }
        }
        
        
    }
}
