<?php

namespace App\Http\Controllers\FrontSite;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\PlaceService;
use App\Services\ProvinceService;
use App\Services\DistrictService;
use App\Services\AreaService;
use App\Services\CareerService;
use App\Helpers\Pagination;
use Session;

class ProvinceController extends Controller
{
    //
    protected $request;
    protected $placeService;
    protected $provinceService;

    function __construct(Request $request, ProvinceService $provinceService)
    {
        $this->request = $request;
        $this->provinceService = $provinceService;

    }


    /**
     * ======================
     * Method:: View Home
     * ======================
     */

    public function index($page = 1)
    {
      
        
       
        $totalPlaces = PlaceService::totalRows([]);
        Session::flash('total_place', $totalPlaces);

        return view('frontsite.provinces.index');
    }

    

    /**
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function detail($page)
    {
      //check permission
      $pagination = array(
        'perpage' => 20,
        'page'    => $page,
    );


    $places = $this->placeService->getMany(['id', 'thumbnail', 'title', 'slug', 'address','price', 'phone', 'province', 'district', 'area', 'career', 'rating_value'], $pagination, null, null);
    foreach($places as $key => $place) {
        $province = ProvinceService::findByKey('id', $place->province);
        $district = DistrictService::findByKey('id', $place->district);
        $area = AreaService::findByKey('id', $place->area);
        $career = CareerService::findByKey('id', $place->career);
        $place->province_name = $province->name;
        $place->province_code = $province->code;
        $place->district_name = $district->name;
        $place->district_code = $district->code;
        $place->area_name = $area->name;
        $place->area_code = $area->code;

        if($place->career != 0) {
            $place->career_name = $career->name;
            $place->career_slug = $career->slug;
        } else {
            $place->career_name = '';
            $place->career_slug = '';
        }
        
    }
    $totalPlaces = $this->placeService->totalRows([]);
    $pagination = new Pagination();
    $pagination->paginate($page, 20, $totalPlaces);
    $renderPagination = $pagination->createLinks(3, 'page-nav');

    return view('frontsite.province.detail', compact('places', 'renderPagination'));
    }
}
