<?php

namespace App\Http\Controllers\FrontSite;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\PostService;
use App\Services\PlaceService;
use App\Services\ProvinceService;
use App\Services\DistrictService;
use App\Services\AreaService;
use App\Services\CareerService;
use App\Services\SettingService;
use App\Helpers\Pagination;
use View;
use App;
use App\Services\ReviewService;
use Auth;
use Session;

class HomeController extends Controller
{
    //
    protected $request;
    protected $placeService;
    protected $provinceService;

    function __construct(Request $request, PlaceService $placeService)
    {
        $this->request = $request;
        $this->placeService = $placeService;

    }


    /**
     * ======================
     * Method:: View Home
     * ======================
     */

    public function index($page = 1)
    {
        //check permission
        $pagination = array(
            'perpage' => 20,
            'page'    => $page,
        );
        $lang = $this->request->input('lang', 'vi');
        $option = SettingService::getSetting('theme_option', $lang);
        $sort_type = isset($option['theme_option::home::filter_place']) ? intval($option['theme_option::home::filter_place'])  : 1;
        $sort = [];
        switch ($sort_type) {
            case 1:
                $sort = ['field' => 'province', 'sort' => 'ASC'];
                break;
            case 2:
                $sort = ['field' => 'province', 'sort' => 'DESC'];
                break;
            case 3:
                $sort = ['field' => 'title', 'sort' => 'ASC'];
                break;
            case 4:
                $sort = ['field' => 'title', 'sort' => 'DESC'];
                break;
            case 5:
                $sort = ['field' => 'created_at', 'sort' => 'DESC'];
                break;
            case 6:
                $sort = ['field' => 'created_at', 'sort' => 'ASC'];
                break;
            default:
                $sort = ['field' => 'created_at', 'sort' => 'ASC'];
                break;
        }
        $places = $this->placeService->getMany(['id', 'thumbnail', 'title', 'slug', 'address','price', 'phone', 'province', 'district', 'area', 'career', 'rating_value', 'created_at'], $pagination, $sort, null);
        $place_ids =  $places->map(function($c) { 
            return $c->id;
        })->toArray();

        $countReviewPlace = ReviewService::countReviewsByPlace($place_ids)->toArray();
        $reviewPlaceArgs = [];
        foreach($countReviewPlace as $item) {
            $reviewPlaceArgs[$item->place] = $item->count;
        }
        
        foreach($places as $key => $place) {
            $province = ProvinceService::findByKey('id', $place->province);
            $district = DistrictService::findByKey('id', $place->district);
            $area = AreaService::findByKey('id', $place->area);
            $career = CareerService::findByKey('id', $place->career);
            // $reviewCount = ReviewService::totalRows(array('place' => $place->id));
            $place->province_name = $place->province != 0 ? $province->name : '';
            $place->province_code = $place->province != 0 ? $province->code : '';
            $place->district_name = $place->district != 0 ? $district->name : '';
            $place->district_code = $place->district != 0 ? $district->code : '';
            $place->area_name = $place->area != 0 ? $area->name : '';
            $place->area_code = $place->area != 0 ? $area->code : '';

            if($place->career != 0) {
                $place->career_name = $career->name;
                $place->career_slug = $career->slug;
            } else {
                $place->career_name = '';
                $place->career_slug = '';
            }

            if(isset($reviewPlaceArgs[$place->id])) {
                $place->reviewCount = $reviewPlaceArgs[$place->id];
            } else {
                $place->reviewCount = 0;
            }
            
            
            
        }
        $totalPlaces = $this->placeService->totalRows([]);
        Session::flash('total_place', $totalPlaces);
        $pagination = new Pagination();
        $pagination->paginate($page, 20, $totalPlaces);
        $renderPagination = $pagination->createLinks(3, 'page-nav');
 
        return view('frontsite.home.index', compact('places', 'provinces', 'careers', 'renderPagination'));
    }




    // 

    public function ajaxFilter(Request $request) {

        $input = $request->all();
        $action = isset($input['action']) ? $input['action'] : '';
        $province_id = isset($input['province_id']) ? $input['province_id'] : 0;
        $parent = isset($input['id']) ? $input['id'] : 1;

        switch($action) {
            case 'get_quanhuyen' : 
                return $this->ajaxLoadDistrict($province_id);
                break;
            case 'get_nganhnghe' : 
                return $this->ajaxLoadCareer($parent);
                break;
            case 'get_dienthoai' : 
                return false;
                break;
            default: 
                return false;
                break;
        }
    }

    public function ajaxLoadDistrict($province_id)
    {
        //check permission
       

        $pagination = array(
            'perpage' => 50,
            'page' => 1,
        );
        $filter =  array(
            'province' => $province_id,
        );
        $districts = DistrictService::getMany(['*'], $pagination, null, $filter );
  
        $html = '<select class="clsip slinput" name="quanhuyen" id="khuvuc3" ><option value="0">- Tất cả Quận/Huyện -</option>';
        foreach($districts as $district) {
            
            $html .= '<option value="'.$district->id.'" >'.$district->name.'</option>';
        }
        $html .= '</select><i></i>';
        return $html;
        
    }

    public function ajaxLoadCareer($parent)
    {
        //check permission
       

        $pagination = array(
            'perpage' => 50,
            'page' => 1,
        );
        $filter =  array(
            'parent' => $parent,
        );
        $careers = CareerService::getMany(['*'], $pagination, null, $filter );
  
        $html = '<select class="clsip slinput" name="chitiet" id="chitiet" ><option value="0">- Tất cả Chi tiết -</option>';
        foreach($careers as $career) {
            
            $html .= '<option value="'.$career->id.'" >'.$career->name.'</option>';
        }
        $html .= '</select><i></i>';
        return $html;
        
    }

    public function processFilter() {
        $input = $this->request->only('tinhthanh', 'quanhuyen', 'thutuan', 'chitiet', 'danhgia', 'chinhanh', '');
        $tinhthanh = isset($input['tinhthanh']) ? $input['tinhthanh'] : false;
        $quanhuyen = isset($input['quanhuyen']) ? $input['quanhuyen'] : false;
        $thutuan = isset($input['thutuan']) ? $input['thutuan'] : false;
        $chitiet = isset($input['chitiet']) ? $input['chitiet'] : false;
        $danhgia = isset($input['danhgia']) && $input['danhgia'] != "" ? $input['danhgia'] : null;
        $chinhanh = isset($input['chinhanh']) && $input['chinhanh'] != "" ? $input['chinhanh'] : null;
        $sapxepdidong = isset($input['sapxepdidong']) && $input['sapxepdidong'] != "" ? $input['sapxepdidong'] : null;
        $query = '';
        $query_kv = 'tai';
        $tim = '';
        if($tinhthanh && $tinhthanh != '0') {
            $term = ProvinceService::findByKey( 'id', $tinhthanh );
            $query = $term->code;
        }
        if($quanhuyen && $quanhuyen != '0') {
            $term = DistrictService::findByKey( 'id', $quanhuyen );
            $query = $term->code;
            $query_kv = 'o';
        }
        $query_2 = $query;
        if($thutuan && $thutuan != '0') {
            $term = CareerService::findByKey( 'id', $thutuan );
            if($query != '') {
                $tim = 'tim';
                $query_2 = 'tim-ve-'.$term->slug.'-'.$query_kv.'-'.$query.'.html';
            } else {
                $query_2 = 'nganh-nghe-'.$term->slug.'.html';
            }
            
        }
        if($chitiet && $chitiet != '0') {

            $term = CareerService::findByKey( 'id', $chitiet );
            if($query != '') {
                
                $query_2 = 'tim-cho-'.$term->slug.'-'.$query_kv.'-'.$query.'.html';
            } else {
                $query_2 = 'nganh-nghe-'.$term->slug.'.html';
            }
        } 

        if($query_2 == '') {
            $query_2 = 'tim.html';
        }
        $query3 = [];
        if(isset($danhgia) && $danhgia != '0') {
            $query3[] = 'r='.$danhgia;
        }
        if(isset($chinhanh) && $chinhanh != '0') {
            $query3[] = 'b='.$chinhanh;
        }
        if(isset($sapxepdidong) && $sapxepdidong != '0') {
            $query3[] = 'p='.$sapxepdidong;
        }
        $url = '';
        if(count($query3) > 0) {
            $url = $query_2.'?'.implode('&', $query3);
        } else {
            $url = $query_2;
        }
        
        
        return redirect(url($url)); 
    }

    public function sitemap() {

        $input = $this->request->only('page');
        $page = isset($input['page']) ? $input['page'] : 1;
        $pagination = ['page' => $page, 'perpage' => 5000];
        $places = $this->placeService->getMany(['id', 'slug', 'created_at'], $pagination, null, null);

        return response()->view('frontsite.sitemap', compact('places'))->withHeaders([
            'Content-Type' => 'text/xml'
        ]);

    }

    public function sitemapPost() {
        $input = $this->request->only('page');
        $page = isset($input['page']) ? $input['page'] : 1;
        $pagination = ['page' => $page, 'perpage' => 5000];
        $posts = PostService::getMany(['id', 'post_slug', 'created_at'], $pagination, null, null);

        return response()->view('frontsite.sitemapPost', compact('posts'))->withHeaders([
            'Content-Type' => 'text/xml'
        ]);
    }
}
