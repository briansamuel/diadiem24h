<?php

namespace App\Http\Controllers\FrontSite;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\PlaceService;
use App\Services\ProvinceService;
use App\Services\DistrictService;
use App\Services\AreaService;
use App\Services\CareerService;
use App\Helpers\Pagination;
use View;
use App;
use Auth;

class DistrictController extends Controller
{
    //
    protected $request;
    protected $placeService;
    protected $provinceService;

    function __construct(Request $request, DistrictService $districtService)
    {
        $this->request = $request;
        $this->districtService = $districtService;

    }


    /**
     * ======================
     * Method:: View Home
     * ======================
     */

    public function index($page = 1)
    {
        //check permission
        $pagination = array(
            'perpage' => 20,
            'page'    => $page,
        );
        $provinces = ProvinceService::getAll();

        $places = $this->placeService->getMany(['id', 'thumbnail', 'title', 'slug', 'address','price', 'phone', 'province', 'district', 'area', 'career', 'rating_value'], $pagination, null, null);
        foreach($places as $key => $place) {
            $province = ProvinceService::findByKey('id', $place->province);
            $district = DistrictService::findByKey('id', $place->district);
            $area = AreaService::findByKey('id', $place->area);
            $career = CareerService::findByKey('id', $place->career);
            $place->province_name = $province->name;
            $place->province_code = $province->code;
            $place->district_name = $district->name;
            $place->district_code = $district->code;
            $place->area_name = $area->name;
            $place->area_code = $area->code;

            if($place->career != 0) {
                $place->career_name = $career->name;
                $place->career_slug = $career->slug;
            } else {
                $place->career_name = '';
                $place->career_slug = '';
            }
            
        }
        $totalPlaces = $this->placeService->totalRows([]);
        $pagination = new Pagination();
        $pagination->paginate($page, 20, $totalPlaces);
        $renderPagination = $pagination->createLinks(3, 'page-nav');
 
        foreach($provinces as $key => $item_province) {
            $countPlace = $this->placeService->totalRows(array('province' => $item_province->id));
            $item_province->count = $countPlace;
        }
        // dd($provinces);
        return view('frontsite.home.index', compact('places', 'provinces', 'renderPagination'));
    }

    public function ajaxLoadDistrict(Request $request)
    {
        //check permission
       
        $input = $request->input('page');
        $province_id = isset($input['province_id']) ? $input['province_id'] : 0;
        $pagination = array(
            'perpage' => 50,
            'page' => 1,
        );
        $filter =  array(
            'province' => $province_id,
        );
        $districts = $this->districtService->getMany(['*'], $pagination, null, $filter );
        $html = '<select class="clsip slinput" name="quanhuyen" id="khuvuc3" ><option value="0">- Tất cả Quận/Huyện -</option>';
        foreach($districts as $district) {
            
            $html .= '<option value="'.$district->id.'" >'.$district->name.'</option>';
        }
        $html .= '</select><i></i>';
        return $html;
        
    }


    /**
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function detail($slug, $id)
    {
        $new = $this->postService->findByKey('id', $id);

        return view('frontsite.home.detail', compact('new'));
    }
}
