<?php

namespace App\Http\Controllers\FrontSite;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\PlaceService;
use App\Services\ProvinceService;
use App\Services\DistrictService;
use App\Services\AreaService;
use App\Services\CareerService;
use App\Helpers\Pagination;
use App\Services\ReviewService;
use App\Services\PostService;
use Session;

class PostController extends Controller
{
    //
    protected $request;
    protected $placeService;
    protected $provinceService;

    function __construct(Request $request, PlaceService $placeService)
    {
        $this->request = $request;
        $this->placeService = $placeService;

    }


    /**
     * ======================
     * Method:: View Home
     * ======================
     */

    public function index($page = 1)
    {
        
        $pagination = array(
            'perpage' => 20,
            'page'    => $page,
        );
        $posts = PostService::getMany(['id','post_title', 'post_description', 'post_thumbnail', 'post_slug'], $pagination, null, null);
        
        $totalPlaces = PostService::totalRows( []);
        Session::flash('total_place', $totalPlaces);
        $pagination = new Pagination();
        $pagination->paginate($page, 20, $totalPlaces);
        $renderPagination = $pagination->createLinksCustom('-tin-tuc', 3, 'page-nav');
        
        return view('frontsite.news.index', compact( 'posts', 'renderPagination'));
    }



    /**
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function detail($slug)
    {   
      
        $post = PostService::findByKey('post_slug', $slug);
        if($post->post_slug != $slug) {
            return abort(404);
        }


        return view('frontsite.news.detail', compact('post'));

    }

  

    

    

   
}
