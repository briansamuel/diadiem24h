<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Harimayco\Menu\Facades\Menu;
use App\Services\ValidationService;
use App\Services\LogsUserService;
use Exception;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use Sunra\PhpSimple\HtmlDomParser;
use App\Services\PostService;
use App\Services\PlaceService;
use App\Services\ProvinceService;
use App\Services\DistrictService;
use App\Services\AreaService;
use App\Services\CareerService;
use App\Services\ReviewService;

class CrawlTruyenCVController extends Controller
{



    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * ======================
     * Method:: INDEX
     * ======================
     */
    public function crawl()
    {
        return view('admin.crawlers.index');
    }

    public function crawlChapter()
    {
        return view('admin.crawlers.chapter');
    }

    public function index()
    {
        // $url = "https://truyencv.com/tien-hiep/";
        // $html = $this->curl($url);
        // $stories_link = HTMLDomParser::str_get_html($html)->find('h2.title a');
        // foreach ($stories_link as $story) {
        //     //echo $story->href.'<br>';

        //     $row = StoryService::findByKey('story_slug', Str::slug($story->plaintext));
        //     if ($row) {
        //         $this->fillGenres($row->id, $story->href);
        //         echo 'Truyện ' . $story->plaintext . ' đã có sẵn <br>';
        //         //$this->listChapter($row->id, $story->href);
        //     } else {
        //         $data = $this->fillStory($story->href);

        //         if ($data) {
        //             $insert_id = StoryService::insert($data);
        //             if ($story) {
        //                 $this->fillGenres($insert_id, $story->href);
        //                 echo 'Truyện ' . $story->plaintext . ' thêm thành công <br>';
        //             } else {
        //                 echo 'Truyện ' . $story->plaintext . ' thêm thất bại <br>';
        //             }
        //         }
        //     }
        // }
        // $url ="https://truyenfull.net/";
        // $html = $this->curl($url);
        // $categories = HTMLDomParser::str_get_html($html)->find('.col-truyen-side .list-cat a');

        // foreach($categories as $category) {

        //     //echo $story->href.'<br>';

        //     $row = CategoryService::findByKey('category_name', $category->plaintext);
        //     if($row) {
        //         echo 'Thể loại '.$category->plaintext.' đã có sẵn <br>';
        //     } else {
        //         $data = $this->fillCategories($category->href);
        //         if($data) {
        //             $insert = CategoryService::insert($data);
        //             if($insert) {
        //                 echo 'Thể loại '.$category->plaintext.' thêm thành công <br>';
        //             } else {
        //                 echo 'Thể loại '.$category->plaintext.' thêm thất bại <br>';
        //             }
        //         }


        //     }
        // }

        // for($i = 1; $i < 37; $i++) {
        //     echo 'https://truyencv.com/kiem-hiep/trang-'.$i.'/';
        //     echo '<br>';
        // }

        // for($i = 1; $i < 206; $i++) {
        //     echo 'https://truyencv.com/do-thi/trang-'.$i.'/';
        //     echo '<br>';
        // }
        $url = "https://truyencv.com/pham-nhan-tu-tien-chi-tien-gioi-thien/";
        $html = $this->curl($url);
        $link_id = HTMLDomParser::str_get_html($html)->find('.list-popover li a')[0]->href;
        $list_id = explode('/', $link_id);
        $id = isset($list_id[3]) ? (int) $list_id[3] : 1;
        $slug = explode('/', $url)[3];
        $row = StoryService::findByKey('story_slug', $slug);
        if ($row) {
            $para = 'showChapter=1&media_id=' . $id . '&number=1&page=1&type=' . $slug;


            $html2 = $this->curlPost('https://truyencv.com/index.php', $para);

            $list_chapter = HTMLDomParser::str_get_html($html2)->find('.item a');
            $list_chapter = array_reverse($list_chapter, true);

            // foreach ($list_chapter as $chapter) {
            for ($i = count($list_chapter) - 1195; $i > 0; $i--) {
                //dd($chapter->href);
                //echo $list_chapter[$i]->href.'<br>';

                $data = $this->fillChapter(1, $list_chapter[$i]->href);
                if ($data) {

                    $filter = array('story_id' => $row->id, 'chapter_slug' => $data['chapter_slug']);
                    //dd($filter);
                    $chapter = ChapterService::totalRows($filter);
                    if ($chapter < 1) {
                        $data['story_id'] = $row->id;
                        ChapterService::insert($data);
                    }
                }
            }
        } else {
            echo 'Truyện này chưa có trong dữ liệu <br>';
        }


        die();
    }
    public function homestay() {
        $input = $this->request->only('from', 'to');
        $from = isset($input['from']) ? intval($input['from']) : 2;
        $to = isset($input['to']) ? intval($input['to']) : 51;

        for ($i = $from; $i < $to; $i++) {
            // $url = 'https://homestay.review/category/top-5/page/' . $i . '/';
            $url =  'https://kenhhomestay.com/cam-nang-phuot/page/'.$i.'/';
           
            $data = $this->curl($url);
 
            $html = HtmlDomParser::str_get_html( $data );
            if($html) {
                $links = $html->find('article .post-thumbnail a');
                foreach($links as $key => $link) {
                   
                    $params = $this->fillPost2($link->href);

                    
                    // $title = $link->plaintext;
                    // $image_el = $html->find('.news-v3 .col-md-3 img');
                    // $thumbnail = isset($image_el[$key]) ? $image_el[$key]->src : '';

                    $summary_el = $html->find('article .entry-content p');
                    $description = isset($summary_el[$key]) ? $summary_el[$key]->plaintext : '';

                    
                  
                    if($params) {
                        $params['post_description'] = $description;
                        
                        
                        // $params['post_thumbnail'] = $thumbnail;
                       
                        if ($params['post_thumbnail']) {
                            $image = $this->grab_image($params['post_thumbnail'], 'uploads/files/' . $params['post_slug'] . '.jpg');
                            $params['post_thumbnail'] = '/uploads/files/' . $params['post_slug'] . '.jpg';
                        }
                        
                        try {
                            $pSv = new PostService();
                            $pSv->insert($params);
                            $params = [];
                        } catch(Exception $e) {
                            dd($e);
                        }
                       
                    }
                
                }
            }
            
        }
        die();
    }
    public function listPage()
    {
        for ($i = 1; $i < 205; $i++) {
            echo 'https://truyencv.com/xuyen-khong/trang-' . $i . '/';
            echo '<br>';
        }

        
    }

    public function ajaxListUrl() {
        $page = $this->request->input('page');
        $url = 'https://thegioidiadiem.com/page-'.$page.'-dia-chi.html';
        $data = array();
        $data['list_url'] = array();
        $html = $this->curl($url);
        if (isset($page)) {
            if ($html) {
                $data_link = HtmlDomParser::str_get_html($html)->find('.news-v3 h2 a');
                foreach ($data_link as $link) {
                    //echo $story->href.'<br>';

                    array_push($data['list_url'], $link->href);
                }

                $data['status'] = 'success';
                $data['message'] = '<strong class="kt-font-success kt-font-bold">Lấy danh sách url thành công</strong>';
            } else {
                $data['status'] = 'error';
                $data['message'] = '<strong class="kt-font-danger kt-font-bold">Không tìm thấy HTML</strong>';
            } 
        }   
        else {
            $data['status'] = 'error';
            $data['message'] = '<strong class="kt-font-danger kt-font-bold">Không tìm thấy url</strong>';
        }

        echo json_encode($data);
        die();
    }

    public function ajaxInsertPlace() {
        $url = $this->request->input('page');
        $params = $this->fillPlace($url);
        if(!empty($params)) {
            if(!is_array($params) && $params == -1) {
                $data['status'] = 'error';
                $data['message'] = '<strong class="kt-font-warning kt-font-bold">Địa điểm đã tồn tại</strong>';
            } else {
                $placeMdl = new PlaceService();
                $place_id = $placeMdl->insert($params);
                if($place_id) {
                    $this->fillComment($place_id, $url);
                    $data['status'] = 'success';
                    $data['message'] = '<strong class="kt-font-success kt-font-bold">Thêm địa điểm thành công</strong>';
                } else {
                    $data['status'] = 'error';
                    $data['message'] = '<strong class="kt-font-danger kt-font-bold">Lỗi khi thêm địa điểm</strong>';
                }
            }
            
        } else {
            

            $data['status'] = 'error';
            $data['message'] = '<strong class="kt-font-danger kt-font-bold">Không tìm thấy HTML</strong>';
           
        }
        
        echo json_encode($data);
        die();
    }


    public function ajaxListStory()
    {
        $params = $this->request->only('page');
        $data = array();
        $data['list_story'] = array();
        if (isset($params['page'])) {
            $url = $params['page'];

            $html = $this->curl($url);

            if ($html) {



                $stories_link = HTMLDomParser::str_get_html($html)->find('.list-group-item-table h2.title a');
                foreach ($stories_link as $story) {
                    //echo $story->href.'<br>';

                    array_push($data['list_story'], $story->href);
                }

                $data['status'] = 'success';
                $data['message'] = '<strong class="kt-font-success kt-font-bold">Lấy danh sách truyện thành công</strong>';
            } else {
                $data['status'] = 'error';
                $data['message'] = '<strong class="kt-font-danger kt-font-bold">Không tìm HTML</strong>';
            }
        } else {
            $data['status'] = 'error';
            $data['message'] = '<strong class="kt-font-danger kt-font-bold">Không tìm thấy url</strong>';
        }
        //echo 'TEST';
        echo json_encode($data);
        die();
    }



    public function ajaxListChapter()
    {
        $params = $this->request->only('url');
        $data = array();
        $data['list_chapter'] = array();
        $story_status = '';
        if (isset($params['url'])) {

            $story = $this->fillStory($params['url']);
            $row = StoryService::findByKey('story_title', $story['story_title']);
            if ($row) {
                $genres = $this->fillGenres($row->id, $params['url']);
                $story_status = 'Truyện ' . $story['story_title'] . ' đã có sẵn ' . $genres;
                $data['message'] = '<strong class="kt-font-warning kt-font-bold">' . $story_status . '</strong>';
            } else {


                if ($story) {
                    $insert_id = StoryService::insert($story);
                    if ($story) {
                        $genres = $this->fillGenres($insert_id, $params['url']);
                        $story_status = 'Truyện ' . $story['story_title'] . ' thêm thành công ' . $genres;
                        $data['message'] = '<strong class="kt-font-success kt-font-bold">' . $story_status . '</strong>';
                    } else {
                        $story_status = 'Truyện ' . $story['story_title'] . ' thêm thất bại ';
                        $data['message'] = '<strong class="kt-font-danger kt-font-bold">' . $story_status . '</strong>';
                    }
                } else {
                    $story_status = 'Truyện ' . $story['story_title'] . ' dữ liệu bị lỗi, vui lòng thử lại ';
                    $data['message'] = '<strong class="kt-font-danger kt-font-bold">' . $story_status . '</strong>';
                }
            }

            // try {

            $data['status']    = 'success';
            //     $this->mergeListChapter($data['list_chapter'], $params['url']);
            //     $count = count($data['list_chapter']);
            //     $data['status'] = 'success';
            //     $data['message'] = '<strong class="kt-font-success kt-font-bold">' . $story_status . ' - Đã lấy ' . $count . ' chapter thành công</strong>';
            // } catch (Exception $e) {
            //     $data['status'] = 'error';
            //     $data['message'] = '<strong class="kt-font-danger kt-font-bold">Lỗi khi lấy dữ liệu, vui lòng thử lại</strong>';
            // }

        } else {
            $data['status'] = 'error';
            $data['message'] = '<strong class="kt-font-danger kt-font-bold">Không tìm thấy url</strong>';
        }

        echo json_encode($data);
        die();
    }

    public function ajaxListChapter2()
    {
        $params = $this->request->only('ids');

        $ids = $params['ids'];
        
        
        $ids = array_unique($ids);
        $data['data'] = array();
        foreach ($ids as $id) {
            $array_chapter = array();
            $row = StoryService::findByKey('id', $id);
            $url = 'https://truyencv.com/' . $row->story_slug . '/';

            $html = $this->curl($url);
            if(isset($row)) {
                
                if($row->crawl_status == 'done') {
                    $data['status'] = 'error';
                    $data['message'] = '<strong class="kt-font-danger kt-font-bold">Truyện này đã lấy chương</strong>';
                } else {
                    if ($html) {
                        try {
                            
                            $link_id = isset(HTMLDomParser::str_get_html($html)->find('.list-popover li a')[0]) ? HTMLDomParser::str_get_html($html)->find('.list-popover li a')[0]->href : null;
                            if($link_id) {
                                $list_id = explode('/', $link_id);
                                $id_truyencv = isset($list_id[3]) ? (int) $list_id[3] : 1;
        
                                $para = 'showChapter=1&media_id=' . $id_truyencv . '&number=1&page=1&type=' . $row->story_slug;
        
                                
                                $html2 = $this->curlPost('https://truyencv.com/index.php', $para);
                                if($html2) {
                                    $list_chapter = HTMLDomParser::str_get_html($html2)->find('.item a');
                            
                                    $list_chapter = array_reverse($list_chapter, true);
                                    
                                    foreach ($list_chapter as $chapter) {
            
                                        array_push($array_chapter, $chapter->href);
                                    }
                                } else {
                                    $data['status'] = 'error';
                                    $data['message'] = '<strong class="kt-font-danger kt-font-bold">Lỗi khi lấy dữ liệu, không lấy được HTML</strong>';
                                }
                                
                            }
                            
        
                            $data['status'] = 'success';
                            $data['message'] = '<strong class="kt-font-success kt-font-bold">Lấy danh sách chương thành công</strong>';
                        } catch(Exception $e) {
                            $data['status'] = 'error';
                            $data['message'] = '<strong class="kt-font-success kt-font-bold">Lấy danh sách chương thành công</strong>';
                        }
                        
        
                       
                    } else {
                        $data['status'] = 'error';
                        $data['message'] = '<strong class="kt-font-danger kt-font-bold">Lỗi khi lấy dữ liệu, không lấy được HTML</strong>';
                    }
                }
            }
            
            array_push($data['data'],array('story_id' => $id, 'story_title' => $row->story_title,'list_chapter' => $array_chapter));
            
        }

        
        echo json_encode($data);
        die();
    }

    public function ajaxUpdateCrawlLogs() {

        try {
            $params = $this->request->only('id');

            $id = $params['id'];
            $params['crawl_status'] = 'done';
            $update_id = StoryService::updateStatusCrawl($id, $params);
            if($update_id) {

                $data['status'] = 'success';
                $data['message'] = '<strong class="kt-font-success kt-font-bold">Cập nhật crawl Logs thành công</strong>';
            }
        } catch(Exception $e) {
            
            $data['status'] = 'error';
            $data['message'] = '<strong class="kt-font-success kt-font-bold">Cập nhật crawl Logs thất bại</strong>';
        }
        
        echo json_encode($data);
        die();
    }
    public function ajaxAddChapter()
    {
        $params = $this->request->only('id','url');
        //dd($params);
        $data = $this->fillChapter(1, $params['url']);
        if ($data) {

            
            $filter = array('story_id' => $params['id'], 'chapter_slug' => $data['chapter_slug']);
            //dd($filter);
            $chapter = ChapterService::totalRows($filter);
            if ($chapter < 1) {
                $data['story_id'] = $params['id'];
                $insert_id = ChapterService::insert($data);
                if($insert_id) {
                    $data['status'] = 'sucess';
                    $data['message'] = '<strong class="kt-font-success kt-font-bold">'.$data['chapter_title'].' : Đã lấy thành công</strong>';
                } else {
                    $data['status'] = 'sucess';
                    $data['message'] = '<strong class="kt-font-danger kt-font-bold">'.$data['chapter_title'].' : Lấy thất bại</strong>';
                }
            } else {
                $data['status'] = 'sucess';
                $data['message'] = '<strong class="kt-font-warning kt-font-bold">'.$data['chapter_title'].' : Đã có sẵn</strong>';
            }
        } else {
            $data['status'] = 'error';
            $data['message'] = '<strong class="kt-font-danger kt-font-bold">'.$data['chapter_title'].' : Lỗi khi lấy dữ liệu </strong>';
        }
        echo json_encode($data);
        die();
    }

    public function mergeListChapter(&$list_chapter, $url)
    {

        $html = $this->curl($url);

        if ($html) {
            $chapters = HTMLDomParser::str_get_html($html)->find('.list-chapter li a');

            foreach ($chapters as $chapter) {
                array_push($list_chapter, $chapter->href);
            }

            $pagination = isset(HTMLDomParser::str_get_html($html)->find('.pagination-sm .active+li a')[0]->href) ? HTMLDomParser::str_get_html($html)->find('.pagination-sm .active+li a')[0]->href : false;
            if ($pagination) {
                self::mergeListChapter($list_chapter, $pagination);
            }
            return true;
        }
        return false;
    }
    public function fillPlace($url) {
        $data = $this->curl($url);
        $html = HtmlDomParser::str_get_html( $data );
        $params = array();
        $titles = $html->find('.title_info_tc h1');
        if ($data) {
            // Tiêu đề
            $title = $titles[0]->plaintext;
            
            // Thumbnail 
            $thumbnail_el = $html->find('img[itemprop="image"]');
            $thumbnail_text = isset($thumbnail_el[0]) ? $thumbnail_el[0]->src : false;
            // Địa chỉ
            $address_el = $html->find('strong[itemprop="streetAddress"]');
            $address_text = isset($address_el[0]) ? $address_el[0]->plaintext : '';
            $address_text = html_entity_decode($address_text, ENT_COMPAT, 'UTF-8');
            // Tỉnh thành
            $province_el = $html->find('strong[itemprop="addressRegion"]');
            $provine_text = isset($province_el[0]) ? html_entity_decode(trim($province_el[0]->plaintext)) : '';
            // Quận huyện
            $district_el = $html->find('strong[itemprop="addressLocality"]');
            $district_text = isset($district_el[0]) ? $district_el[0]->innertext : '';
            $district_text = html_entity_decode(trim($district_text));

            // Khu vực
            $area_el = $html->find('.col-md-6 p a strong');
            $area_text = isset($area_el[2]) ? html_entity_decode(trim($area_el[2]->plaintext)) : '';
            // Ngành nghề
            $career_el = $html->find('.col-md-6 p a strong');
            $career_text = isset($career_el[3]) ? $career_el[3]->plaintext : '';
            // Giá từ
            $price_el = $html->find('strong[itemprop="priceRange"]');
            $price_text = isset($price_el[0]) ? $price_el[0]->plaintext : '';
            // Điện thoại
            $phone_el = $html->find('span[itemprop="telephone"]');
            $phone_text = isset($phone_el[0]) ? trim($phone_el[0]->plaintext) : '';
            // Từ khoá
            $keyword_el = $html->find('.col-md-6 p i');
            $keyword_text = isset($keyword_el[10]) ? $keyword_el[10]->plaintext : '';
            // Toppoint
            $toppoint_el = $html->find('.microsite-point-group span');
            $toppoint_text = '';
            $toppoint_args = [];
            foreach ($toppoint_el as $el) {
                $toppoint_args[] =  trim($el->plaintext);
            }
            $toppoint_text = json_encode($toppoint_args);
            // Rating
            $rating_el = $html->find('div[itemprop="ratingValue"]');
            $rating_text = isset($rating_el[0]) ? trim($rating_el[0]->plaintext) : 0;
            // aggregateRating
            $aggregateRating = $html->find('div[itemprop="aggregateRating"] meta');
            $aggregateRating_text = '';
            $aggregateRating_args = [];
            foreach ($aggregateRating as $ag_el) {
                $aggregateRating_args[] = trim($ag_el->content);
            }
            $aggregateRating_text = json_encode($aggregateRating_args);
            // Hình ảnh
            $images_el = $html->find('.headline-md img');
            $images_text = '';
            $images_args = [];
            foreach ($images_el as $image) {
                array_push($images_args, $image->src);
            }
            $images_text = json_encode($images_args);
            //  Map image 
            $map_el = $html->find('.sky-form a img');
            $map_text = isset($map_el[1]) ? trim($map_el[1]->src) : 0;
            // LatLong
            $lat_el = $html->find('meta[itemprop="latitude"]');
            $lat_text = isset($lat_el[0]) ? $lat_el[0]->content : '';
            $long_el = $html->find('meta[itemprop="longitude"]');
            $long_text = isset($long_el[0]) ? $long_el[0]->content : '';

            $exist = PlaceService::findByKey('title', $title);
            if(!$exist) {
                $areaMdl = new AreaService();
                $province = ProvinceService::findByKey('name', $provine_text);
                $district = DistrictService::findByKey('name', $district_text);
                if($district) {
                    $area = AreaService::findByKey('name', $area_text);
                }
                
                $career = CareerService::findByKey('name', $career_text);
                
                if(!$province) {
                   
                    $province_id = 0;
                } else {
                    $province_id = $province->id;
                }

                if(!$district) {
                   
                    $district_id = 0;
                } else {
                    $district_id = $district->id;
                }

                if(!$area) {
                    
                    $resultAre = $areaMdl->insert( array(
                        'name' => $area_text,
                        'code' => Str::slug($area_text),
                        'province' => $province_id,
                        'district' => $district_id,
                    ));

                    $area_id =  $resultAre;
                } else {
                    $area_id = $area->id;
                }

                if(!$career) {
                    // $resultCrr = $wpdb->insert($wpdb->careers, array(
                    // 	'name' => $provine_text,
                    // 	'code' => sanitize_title($career_text),
                    // 	'province' => $province_id,
                    // 	'district' => $district_id,
                    // ));
                    $career_id = 0;
                } else {
                    $career_id = $career->id;
                }

                $params = array(
                    'title'    => $title,
                    'slug'     => Str::slug($title),
                    'content'  => '',
                    'status'   => 'publish',
                    'address'   => $address_text,
                    'province' => $province_id,
                    'district' => $district_id,
                    'area' => $area_id,
                    'career' => $career_id,
                    'price' => $price_text,
                    'phone' => $phone_text,
                    'keyword' => $keyword_text,
                    'toppoint' => $toppoint_text,
                    'rating_value' => $rating_text,
                    'aggregate_rating' => $aggregateRating_text,
                    'images' => $images_text,
                    'map_image' => $map_text,
                    'thumbnail' => $thumbnail_text,
                    'latlng' => $lat_text.','.$long_text,
                );

               

                return $params;
            } else {
                return -1;
            }
            
            
            
        }

        return false;
    }

    // public function fillPost($url) {
    //     $data = $this->curl($url);
       
    //     $data = str_replace('data-src', 'src', $data);
    //     $html = HtmlDomParser::str_get_html( $data );
       
    //     $params = array();
       
    //     if ($data || $html) {

    //         $titles = $html->find('.single-post-title');
    //         $title = $titles[0]->plaintext;
            
    //         // Thumbnail 
    //         $thumbnail_el = $html->find('.post-header');
    //         $thumbnail_text = isset($thumbnail_el[0]) ? $thumbnail_el[0]->src : '';

    //         // Content
    //         $content_el = $html->find('.single-post-content');
    //         $content_text = isset($content_el[0]) ? $content_el[0]->innertext : '';

    //         $content_text =  preg_replace('/<a(.*?)href=\"(.*?)\">(.*?)<\/a>/', "\\3", $content_text);

    //         // Description
    //         $description_el = $html->find('meta[name=description]');
    //         $description_text = isset($description_el[0]) ? $description_el[0]->content : '';

    //         // 
    //         $params = array(
    //             'post_title' => html_entity_decode(trim($title)),
    //             'post_slug' => Str::slug($title),
    //             'post_thumbnail' => $thumbnail_text,
    //             'post_content' => $content_text,
    //             'post_description' => $description_text,
    //             'post_seo_title' => $title,
    //             'post_seo_description' => $description_text,
    //             'post_seo_keyword' => $description_text,
    //             'post_type' => 'news',
    //             'post_status' => 'publish',
                
    //         );
    //         $params['created_by_user'] =  1;
    //         $params['updated_by_user'] =  1;
    //         if(!isset($title))
    //             return false;
            
    //         return $params;
    //     }

    //     return false;
    // }
    public function fillPost($url) {
        $data = $this->curl($url);
       
        $data = str_replace('data-src', 'src', $data);
        $html = HtmlDomParser::str_get_html( $data );
       
        $params = array();
       
        if ($data || $html) {

            $titles = $html->find('.title_info_tc h1');
            $title = $titles[0]->plaintext;
            
            // Thumbnail 
            // $thumbnail_el = $html->find('.post-header');
            // $thumbnail_text = isset($thumbnail_el[0]) ? $thumbnail_el[0]->src : '';

            // Content
            $content_el = $html->find('.article-content');
            $content_text = isset($content_el[0]) ? $content_el[0]->innertext : '';

            $content_text =  preg_replace('/<a(.*?)href=\"(.*?)\">(.*?)<\/a>/', "\\3", $content_text);

            //Description
            $description_el = $html->find('meta[name=description]');
            $description_text = isset($description_el[0]) ? $description_el[0]->content : '';

            // Keyword
            $keyword_el = $html->find('meta[name=keywords]');
            $keyword_text = isset($keyword_el[0]) ? $keyword_el[0]->content : '';
            // 
            $params = array(
                'post_title' => html_entity_decode(trim($title)),
                'post_slug' => Str::slug($title),
                'post_thumbnail' => '',
                'post_content' => $content_text,
                'post_description' => '',
                'post_seo_title' => $title,
                'post_seo_description' => $description_text,
                'post_seo_keyword' => $keyword_text,
                'post_type' => 'news',
                'post_status' => 'publish',
                
            );
            $params['created_by_user'] =  1;
            $params['updated_by_user'] =  1;
            if(!isset($title))
                return false;
            
            return $params;
        }

        return false;
    }

    public function fillPost2($url) {
        $data = $this->curl($url);
       
        $data = str_replace('data-src', 'src', $data);
        $html = HtmlDomParser::str_get_html( $data );
       
        $params = array();
       
        if ($data || $html) {

            $excude_array = ['.afcd30bb457b5378f3540191a04713bf', '.kk-star-ratings', '.heateor_sss_sharing_container'];


            foreach($excude_array as $ex) {

                foreach($html->find($ex) as $item) {

                    $item->outertext = '';

                }

            }

            $titles = $html->find('h1.entry-title');
            $title = isset($titles[0]) ? $titles[0]->plaintext : '';
            
            //Thumbnail 
            $thumbnail_el = $html->find('article.single-post .post-thumbnail img');
            $thumbnail_text = isset($thumbnail_el[0]) ? $thumbnail_el[0]->src : '';

            // Content
            $content_el = $html->find('.entry-content');
            $content_text = isset($content_el[0]) ? $content_el[0]->innertext : '';

            $content_text =  preg_replace('/<a(.*?)href=\"(.*?)\">(.*?)<\/a>/', "\\3", $content_text);

            $content_text = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $content_text);

            $content_text = str_replace('Kenhhomestay.com', 'Sotaydiadiem.com', $content_text);

            //Description
            $description_el = $html->find('meta[name=description]');
            $description_text = isset($description_el[0]) ? $description_el[0]->content : '';

            // Keyword
            $keyword_el = $html->find('meta[name=keywords]');
            $keyword_text = isset($keyword_el[0]) ? $keyword_el[0]->content : '';
            // 
            $params = array(
                'post_title' => html_entity_decode(trim($title)),
                'post_slug' => Str::slug($title),
                'post_thumbnail' => $thumbnail_text,
                'post_content' => $content_text,
                'post_description' => '',
                'post_seo_title' => $title,
                'post_seo_description' => $description_text,
                'post_seo_keyword' => $keyword_text,
                'post_type' => 'news',
                'post_status' => 'publish',
                
            );
            $params['created_by_user'] =  1;
            $params['updated_by_user'] =  1;
            if(!isset($title))
                return false;
            
            return $params;
        }

        return false;
    }
    public function fillComment($place_id, $url) {
        try {
            $data = $this->curl($url);
            $html = HtmlDomParser::str_get_html( $data );
            $comments_el = $html->find('.review-item');
            foreach ($comments_el as $comment) {
                $author = $comment->find('.ru-username span[itemprop="name"]');
                $author_text = isset($author[0]) ? $author[0]->plaintext : 'N/A';
                $avtar_el = $comment->find('.ru-avatar img');
                $avatar_text = isset($avtar_el[0]) ? $avtar_el[0]->src : 'N/A';
                $name = $comment->find('.eview-des span[itemprop="name"]');
                $name_text = isset($name[0]) ? $name[0]->plaintext : 'N/A';
                $devive_el = $comment->find('.ru-device');
                $devive_text = isset($devive_el[0]) ? $devive_el[0]->plaintext : 'Android';
                $time_el = $comment->find('.ru-time');
                $time_text = isset($time_el[0]) ? str_replace('/', '-', $time_el[0]->plaintext) : time();
                $body_el = $comment->find('span[itemprop="reviewBody"]');
                $body_text = isset($body_el[0]) ? $body_el[0]->plaintext : '';
                $rating_el = $comment->find('span[itemprop="ratingValue"]');
                $rating_text = isset($rating_el[0]) ? $rating_el[0]->plaintext : 0.0;
                $imgs_comment_el = $comment->find('.review-photos img');
                $imgs_cmt_args = [];
                foreach ($imgs_comment_el as $img) {
                    $imgs_cmt_args[] = $img->src;
                }
                $dtime_text = trim(str_replace(',','',$time_text));
                $dtime = date_create_from_format("d-m-Y H:i:s", $dtime_text);
                $timestamp = $dtime->getTimestamp();
                $args_comment = array(
                    'place' => intval($place_id),
                    'author' => trim($author_text),
                    'avatar' => $avatar_text,
                    'device' => trim($devive_text),
                    'review' => $body_text,
                    'rating' => $rating_text,
                    'review_photo' => json_encode($imgs_cmt_args),
                    'created_at' => date('Y-m-d H:i:s', $timestamp),
                    'updated_at' => date('Y-m-d H:i:s', $timestamp),
                );
                $commentMdl = new ReviewService();
                $result = $commentMdl->insert( $args_comment);
                return $result;
            }
        } catch(Exception $e) {
            dd($e);
        }
        
    }

    public function fillStory($url)
    {
        $html = $this->curl($url);
        $params = array();
        if ($html) {
            $params['story_title'] = isset(HTMLDomParser::str_get_html($html)->find('h1.title')[0]) ? HTMLDomParser::str_get_html($html)->find('h1.title')[0]->plaintext : null;
            if ($params['story_title'] == null) {
                return false;
            } else {
                $params['story_title'] = trim($params['story_title']);
            }
            $params['story_slug'] = Str::slug($params['story_title']);
            $params['story_description'] = HTMLDomParser::str_get_html($html)->find('.brief')[0]->innertext;
            $params['story_seo_title'] =  $params['story_title'];
            $params['story_seo_keyword'] = isset(HTMLDomParser::str_get_html($html)->find('meta[name="keywords"]')[0]) ? HTMLDomParser::str_get_html($html)->find('meta[name="keywords"]')[0]->content : $params['story_title'];
            $params['story_seo_description'] = isset(HTMLDomParser::str_get_html($html)->find('meta[name="description"]')[0]) ? HTMLDomParser::str_get_html($html)->find('meta[name="description"]')[0]->content : $params['story_title'];
            $params['story_author'] = HTMLDomParser::str_get_html($html)->find('a.author')[0]->plaintext;
            $params['story_source'] = 'TruyenCV';
            $params['story_status'] = isset(HTMLDomParser::str_get_html($html)->find('.item .item-value')[2]) ? HTMLDomParser::str_get_html($html)->find('.item .item-value')[2]->plaintext : 'coming';

            $params['story_status'] =  trim(str_replace('Hoàn thành', 'full', $params['story_status']));
            $params['story_status'] =  trim(str_replace('Đang ra', 'coming', $params['story_status']));
            $params['story_status'] =  trim(str_replace('Tạm dừng', 'coming', $params['story_status']));
            $params['story_status'] =  trim(str_replace('Dừng viết', 'coming', $params['story_status']));
            $image_url = HTMLDomParser::str_get_html($html)->find('.col-thumb .img-responsive')[0]->getAttribute('data-cfsrc');
            if ($image_url) {
                $image = $this->grab_image($image_url, 'uploads/files/' . $params['story_slug'] . '.jpg');
                $params['story_thumbnail'] = 'http://' . config('domain.web.domain') . '/uploads/files/' . $params['story_slug'] . '.jpg';
            }


            $params['created_by_user'] =  1;
            $params['updated_by_user'] =  1;
            return $params;
        }

        return false;
    }
    public function fillGenres($id, $url)
    {
        $html = $this->curl($url);
        $params = array();
        $genres_text = ' | Thể loại : ';
        if ($html) {
            $genres = HTMLDomParser::str_get_html($html)->find('.categories a');
            foreach ($genres as $genre) {
                if ($genre->plaintext) {
                    $title = trim($genre->plaintext);
                    $row = CategoryService::findByKey('category_name', trim($title));
                    if ($row) {
                        $params = array('category_id' => $row->id, 'story_id' => $id);
                        $count = CategoryStoryService::totalRows($params);
                        if ($count < 1) {
                            CategoryStoryService::insert($params);
                        }
                        $genres_text .= $title . ', ';
                    }
                }
            }
        }
        return $genres_text;
    }
    public function fillCategories($url)
    {
        $html = $this->curl($url);
        $params = array();
        if ($html) {
            $params['category_name'] = HTMLDomParser::str_get_html($html)->find('span[itemprop="title"]')[1]->plaintext;
            $params['category_description'] = HTMLDomParser::str_get_html($html)->find('.cat-desc .panel-body')[0]->innertext;
            $params['category_seo_title'] = HTMLDomParser::str_get_html($html)->find('title')[0]->plaintext;
            $params['category_seo_keyword'] = HTMLDomParser::str_get_html($html)->find('meta[name="keywords"]')[0]->content;
            $params['category_seo_description'] = HTMLDomParser::str_get_html($html)->find('meta[name="description"]')[0]->content;

            return $params;
        }
        return false;
    }

    public function fillChapter($order, $url)
    {
        $html = $this->curl($url);

        $params = array();
        if ($html) {

            //dd($html);
            $params['chapter_title'] = HTMLDomParser::str_get_html($html)->find('h2.title')[0]->innertext;
            $params['chapter_slug'] = explode('/', $url)[4];
            $params['chapter_content'] = HTMLDomParser::str_get_html($html)->find('#js-truyencv-content')[0]->innertext;
            $params['chapter_seo_title'] =  $params['chapter_title'];
            $params['chapter_seo_keyword'] = HTMLDomParser::str_get_html($html)->find('meta[name="keywords"]')[0]->content;
            $params['chapter_seo_description'] = HTMLDomParser::str_get_html($html)->find('meta[name="description"]')[0]->content;
            $order = str_replace('chuong-', '', $params['chapter_slug']);
            $order = !empty($order) ? (int) $order : 1;
            $params['chapter_order'] = $order;

            $params['created_by_user'] =  1;
            $params['updated_by_user'] =  1;
            //dd($params);
            return $params;
        }

        return false;
    }
    public function listChapter($story_id, $url)
    {
        $html = $this->curl($url);
        $params = array();
        if ($html) {
            $chapters = HTMLDomParser::str_get_html($html)->find('.list-chapter li a');
            $order = 1;
            foreach ($chapters as $chapter) {
                if ($chapter->href) {
                    $data = $this->fillChapter($order, $chapter->href);
                    if ($data) {
                        $data['story_id'] = $story_id;
                        $params = array('story_id' => $story_id, 'chapter_slug' => $data['chapter_slug']);
                        $count = ChapterService::totalRows($params);
                        if ($count < 1) {
                            ChapterService::insert($data);
                        }
                    }
                }
                $order++;
            }

            $pagination = isset(HTMLDomParser::str_get_html($html)->find('.pagination-sm .active+li a')[0]->href) ? HTMLDomParser::str_get_html($html)->find('.pagination-sm .active+li a')[0]->href : false;
            if ($pagination) {
                self::listChapter($story_id, $pagination);
            }
            return true;
        }
        return false;
    }

    public function curl($url)
    {

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 360);
        curl_setopt($ch, CURLOPT_TIMEOUT, 360); //timeout in seconds
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        //curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_VERBOSE, true); // some output will go to stderr / error_log
        // curl_setopt($ch, CURLOPT_REFERER, 'https://homestay.review/');
        //curl_setopt($ch,CURLOPT_FOLLOWLOCATION,true);
        $data = curl_exec($ch);
        curl_close($ch);

        return $data;
    }

    public function curlPost($url, $para)
    {

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 360);
        curl_setopt($ch, CURLOPT_TIMEOUT, 360); //timeout in seconds
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $para);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }

    function grab_image($url, $saveto)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
        $raw = curl_exec($ch);
        curl_close($ch);
        if (file_exists($saveto)) {
            unlink($saveto);
        }
        $fp = fopen($saveto, 'x');
        fwrite($fp, $raw);
        fclose($fp);
    }
}
