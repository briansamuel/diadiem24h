<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Message;
use App\Http\Controllers\Controller;
use App\Models\PasswordReset;
use App\Models\UserModel;
use App\Services\Auth\AuthService;
use App\Services\UserService;
use Illuminate\Http\Request;
use Config;

class LoginController extends Controller
{
    protected $request;
    protected $userService;

    function __construct(Request $request, UserService $userService)
    {
        $this->request = $request;
        $this->userService = $userService;
    }

    /**
     * ======================
     * Method:: login
     * ======================
     */

    public function login()
    {
        
        //check login
        $checkLogin = AuthService::checkLogin();
        if ($checkLogin) {
            return redirect('/');
        }
        
        return view('admin.pages.login');
    }

    public function loginAction(Request $request)
    {
        $loginAdmin = AuthService::loginAdmin($request);

        return $loginAdmin;
    }

    /*
    * function logout
    */
    public function logout()
    {
        AuthService::logout();

        return redirect()->route('login');
    }

    public function resetPassword(Request $request)
    {
        $token = $request->input('token');
        $passwordReset = PasswordReset::where('token', $token)->first();
        if (!$passwordReset) {
            return view('admin.pages.error404');
        }
        return view('admin.pages.reset_password', ['token' => $token]);
    }

}
