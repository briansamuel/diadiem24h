<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::domain(Config::get('domain.web.domain'))->group(function () {
    Route::namespace('FrontSite')->group(function () {

        Route::get('/', ['uses' => 'HomeController@index'])->name('frontsite.home');
        Route::get('/page-{page}.html', ['uses' => 'HomeController@index'])->name('frontsite.home.pagination')->where('page', '[0-9]+');

        // Page
        Route::get('/{slug}.page', ['uses' => 'PageController@index'])->name('frontsite.page');
        // News

        Route::get('/tin-tuc.html', ['uses' => 'PostController@index'])->name('frontsite.posts');
        Route::get('/page-{page}-tin-tuc.html', ['uses' => 'PostController@index'])->name('posts.pagination')->where('page', '[0-9]+');
        Route::get('/tin-tuc/{slug}.html', ['uses' => 'PostController@detail'])->name('posts.detail')->where('slug', '[-A-Za-z0-9]+');


        Route::get('/nganh-nghe.html', ['uses' => 'CareerController@index'])->name('career.index');
        Route::get('/nganh-nghe-{slug}.html', ['uses' => 'CareerController@detail'])->name('career.detail')->where('slug', '[-A-Za-z0-9]+');
        Route::get('/page-{page}-nganh-nghe-{slug}.html', ['uses' => 'CareerController@detailPage'])->name('career.detail.pagination')->where('page', '[0-9]+')->where('slug', '[-A-Za-z0-9]+');

        // Get Places by Province
        Route::get('/tinh-thanh-pho.gov', ['uses' => 'ProvinceController@index'])->name('frontsite.provinces');
        Route::get('/{slug}', ['uses' => 'PlaceController@byProvince'])->name('place.byProvince')->where('slug', '[-A-Za-z0-9]+');
        Route::get('/page-{page}-{slug}.html', ['uses' => 'PlaceController@byProvincePage'])->name('place.byProvince')->where('page', '[0-9]+')->where('slug', '[-A-Za-z0-9]+');

        // Get Place by Filter
        Route::get('/tim-cho-{career}-tai-{province}.html', ['uses' => 'PlaceController@filterPC'])->name('place.filterPC')->where('career', '[-A-Za-z0-9]+')->where('province', '[-A-Za-z0-9]+');
        Route::get('/tim-cho-{career}-o-{disitrict}.html', ['uses' => 'PlaceController@filterDC'])->name('place.filterDC')->where('career', '[-A-Za-z0-9]+')->where('district', '[-A-Za-z0-9]+');
        Route::get('/tim-ve-{career}-tai-{province}.html', ['uses' => 'PlaceController@filterPC'])->name('place.filterPC2')->where('career', '[-A-Za-z0-9]+')->where('province', '[-A-Za-z0-9]+');
        Route::get('/tim-ve-{career}-o-{disitrict}.html', ['uses' => 'PlaceController@filterDC'])->name('place.filterDC2')->where('career', '[-A-Za-z0-9]+')->where('district', '[-A-Za-z0-9]+');
        Route::get('/tim.html', ['uses' => 'PlaceController@filterRank'])->name('place.filterRank');
        // Detail Place
        Route::get('/{slug}-{id}.html', ['uses' => 'PlaceController@detail'])->name('place.detail')
            ->where('slug', '[-A-Za-z0-9]+')
            ->where('id', '[0-9]+');

        // get District by Province id Ajax
        Route::post('/source/ajax', ['uses' => 'HomeController@ajaxFilter'])->name('ajax.ajaxFilter');
        Route::get('/sources/xuly.php', ['uses' => 'HomeController@processFilter'])->name('ajax.processFilter');
        // Route::post('/ajax/load-more-news', ['uses' => 'HomeController@ajaxLoadingNews'])->name('frontsite.ajaxLoadingNews');
        // Route::get('/{slug}.{id}.html', ['uses' => 'HomeController@detail'])->name('frontsite.detail')
        //     ->where('slug', '[-A-Za-z0-9]+')
        //     ->where('id', '[0-9]+');

        // Sitemap
        Route::get('/site_map.xml', ['uses' => 'HomeController@sitemap'])->name('frontsite.home.sitemap');
        Route::get('/site_map_post.xml', ['uses' => 'HomeController@sitemapPost'])->name('frontsite.home.sitemapPost');
    });
});
Route::domain(Config::get('domain.web.admin'))->group(function () {

    if (!defined('FM_USE_ACCESS_KEYS')) {
        define('FM_USE_ACCESS_KEYS', true); // TRUE or FALSE
    }

    if (!defined('FM_DEBUG_ERROR_MESSAGE')) {
        define('FM_DEBUG_ERROR_MESSAGE', false); // TRUE or FALSE
    }

    Route::namespace('Admin')->group(function () {

        Route::get('denied-permission', function () {
            return view('admin.pages.permission_denied');
        });
        // login

        Route::get('/login', ['uses' => 'LoginController@login'])->name('login');
        Route::post('/login', ['uses' => 'LoginController@loginAction'])->name('loginAction');
        Route::get('/logout', ['uses' => 'LoginController@logout'])->name('logout');
        Route::get('/reset_password', ['uses' => 'LoginController@resetPassword'])->name('resetPassword');
        Route::get('/active-user', ['uses' => 'LoginController@activeUser'])->name('activeUser');
        Route::get('/active-agent', ['uses' => 'LoginController@activeAgent'])->name('activeAgent');
        Route::get('/kich-hoat-tai-khoan', ['uses' => 'LoginController@activeGuest'])->name('activeGuest');
    });
    Route::middleware(['auth'])->group(function () {

        Route::get('/dashboard', ['uses' => 'Admin\DashboardController@index'])->name('dash-board');
        Route::get('/welcome', ['uses' => 'Admin\WelcomeController@index'])->name('welcome');
        Route::get('/', ['uses' => 'Admin\WelcomeController@index'])->name('home');

        Route::namespace('Admin')->group(function () {
            // my profile
            Route::get('/my-profile', ['uses' => 'MyProfileController@profile'])->name('profile');
            Route::post('/my-profile', ['uses' => 'MyProfileController@updateProfile'])->name('profile.update');

            // change password
            Route::get('/change-password', ['uses' => 'MyProfileController@changePassword'])->name('profile.changePassword');
            Route::put('/change-password', ['uses' => 'MyProfileController@changePasswordAction'])->name('profile.changePasswordAction');

            Route::namespace('CMS')->group(function () {

                // Page
                Route::get('/page', ['uses' => 'PageController@index'])->name('page.list');
                Route::get('/page/add', ['uses' => 'PageController@add'])->name('page.add');
                Route::post('/page/add', ['uses' => 'PageController@add'])->name('page.add.action');
                Route::get('/page/edit/{page_id}', ['uses' => 'PageController@add'])->name('page.edit');
                Route::post('/page/edit/{page_id}', ['uses' => 'PageController@add'])->name('page.edit.action');
                Route::post('/page/edit', ['uses' => 'PageController@editManyAction'])->name('page.edit.many.action');
                Route::get('/page/delete/{page_id}', ['uses' => 'PageController@delete'])->name('page.delete');
                Route::post('/page/delete', ['uses' => 'PageController@deletemany'])->name('page.delete.many');
                Route::post('/page/save', ['uses' => 'PageController@save'])->name('page.save');
                Route::get('/page/ajax/get-list', ['uses' => 'PageController@ajaxGetList'])->name('page.ajax.getList');

                // News
                Route::get('/news', ['uses' => 'NewsController@index'])->name('news.list');
                Route::get('/news/add', ['uses' => 'NewsController@add'])->name('news.add');
                Route::get('/news/edit/{news_id}', ['uses' => 'NewsController@add'])->name('news.edit');
                Route::post('/news/edit', ['uses' => 'NewsController@editManyAction'])->name('news.edit.many.action');
                Route::get('/news/delete/{news_id}', ['uses' => 'NewsController@delete'])->name('news.delete');
                Route::post('/news/delete', ['uses' => 'NewsController@deletemany'])->name('news.delete.many');
                Route::post('/news/save', ['uses' => 'NewsController@save'])->name('news.save');
                Route::get('/news/ajax/get-list', ['uses' => 'NewsController@ajaxGetList'])->name('news.ajax.getList');

                // User
                Route::get('/user', ['uses' => 'UsersController@index'])->name('user.list');
                Route::get('/user/add', ['uses' => 'UsersController@add'])->name('user.add');
                Route::post('/user/add', ['uses' => 'UsersController@addAction'])->name('user.add.action');
                Route::get('/user/detail/{user_id}', ['uses' => 'UsersController@detail'])->name('user.detail');
                Route::get('/user/edit/{user_id}', ['uses' => 'UsersController@edit'])->name('user.edit');
                Route::post('/user/edit', ['uses' => 'UsersController@editManyAction'])->name('user.edit.many.action');
                Route::post('/user/edit/{user_id}', ['uses' => 'UsersController@editAction'])->name('user.edit.action');
                Route::get('/user/delete', ['uses' => 'UsersController@deleteMany'])->name('user.delete.many');
                Route::get('/user/delete/{user_id}', ['uses' => 'UsersController@delete'])->name('user.delete');
                Route::post('/user/delete', ['uses' => 'UsersController@delete'])->name('user.delete');
                Route::get('/user/ajax/get-list', ['uses' => 'UsersController@ajaxGetList'])->name('user.ajax.getList');

                // Agent
                Route::get('/user-group', ['uses' => 'UserGroupController@index'])->name('user_group.list');
                Route::get('/user-group/add', ['uses' => 'UserGroupController@add'])->name('user_group.add');
                Route::post('/user-group/add', ['uses' => 'UserGroupController@addAction'])->name('user_group.add.action');
                Route::get('/user-group/detail/{guest_id}', ['uses' => 'UserGroupController@detail'])->name('user_group.detail');
                Route::get('/user-group/edit/{guest_id}', ['uses' => 'UserGroupController@edit'])->name('user_group.edit');
                Route::post('/user-group/edit', ['uses' => 'UserGroupController@editManyAction'])->name('user_group.edit.many.action');
                Route::post('/user-group/edit/{guest_id}', ['uses' => 'UserGroupController@editAction'])->name('user_group.edit.action');
                Route::get('/user-group/delete', ['uses' => 'UserGroupController@deleteMany'])->name('user_group.delete.many');
                Route::get('/user-group/delete/{guest_id}', ['uses' => 'UserGroupController@delete'])->name('user_group.delete');
                Route::post('/user-group/delete', ['uses' => 'UserGroupController@delete'])->name('user_group.delete');
                Route::get('/user-group/ajax/get-list', ['uses' => 'UserGroupController@ajaxGetList'])->name('user_group.ajax.getList');

                // Logs User
                Route::get('/logs-user', ['uses' => 'LogsUserController@index'])->name('logs_user.list');
                Route::get('/logs-user/detail/{id}', ['uses' => 'LogsUserController@detail'])->name('logs_user.detail');
                Route::get('/logs-user/ajax/get-list', ['uses' => 'LogsUserController@ajaxGetList'])->name('logs_user.ajax.getList');



                // Contacts
                Route::get('/contact', ['uses' => 'ContactController@index'])->name('contact.index');
                Route::get('/contact/ajax/get-list', ['uses' => 'ContactController@ajaxGetList'])->name('contact.ajax.getList');
                Route::get('/contact/edit/{id}', ['uses' => 'ContactController@edit'])->name('contact.edit');
                Route::post('/contact/edit', ['uses' => 'ContactController@editManyAction'])->name('contact.edit.many.action');
                Route::post('/contact/edit/{id}', ['uses' => 'ContactController@editAction'])->name('contact.edit.action');
                Route::get('/contact/delete', ['uses' => 'ContactController@deleteMany'])->name('contact.delete.many');
                Route::get('/contact/delete/{id}', ['uses' => 'ContactController@delete'])->name('contact.delete');
                Route::post('/contact/delete', ['uses' => 'ContactController@delete'])->name('contact.delete');
                Route::get('/contact/ajax/get-list', ['uses' => 'ContactController@ajaxGetList'])->name('contact.ajax.getList');
                Route::post('/contact/{id}/reply', ['uses' => 'ContactController@replyAction'])->name('contact.reply.action');

                // Gallery
                Route::get('/gallery', ['uses' => 'GalleryController@index'])->name('gallery.index');
            });

            Route::namespace('System')->group(function () {

                // Theme Option
                Route::get('/theme-option', ['uses' => 'ThemeOptionsController@option'])->name('theme_option.index');
                Route::post('/theme-option', ['uses' => 'ThemeOptionsController@optionAction'])->name('theme_option.action');
                // Custom Css
                Route::get('/custom-css', ['uses' => 'CustomCssController@index'])->name('custom_css.index');
                Route::post('/custom-css', ['uses' => 'CustomCssController@editAction'])->name('custom_css.editAction');

                // Custom Template
                Route::get('/template', ['uses' => 'TemplateController@index'])->name('template.index');
                Route::post('/template', ['uses' => 'TemplateController@editAction'])->name('template.editAction');

                // Setting
                Route::get('/settings-general', ['uses' => 'SettingController@general'])->name('setting.general');
                Route::post('/settings-general', ['uses' => 'SettingController@generalAction'])->name('setting.general.action');
                Route::post('/settings-login-social', ['uses' => 'SettingController@loginSocialAction'])->name('setting.login_social.action');
                Route::get('/settings-email', ['uses' => 'SettingController@email'])->name('setting.email');
                Route::get('/settings-social-login', ['uses' => 'SettingController@loginSocial'])->name('setting.login_social');
                Route::get('/settings-notification', ['uses' => 'SettingController@notification'])->name('setting.login_social');
                Route::post('/settings-notification', ['uses' => 'SettingController@notificationAction'])->name('setting.login_social');
            });

            // Crawler

            Route::get('/homestay', ['uses' => 'CrawlTruyenCVController@homestay'])->name('homestay');
            Route::get('/test-demo', ['uses' => 'CrawlTruyenCVController@index'])->name('test-demo');
            Route::get('/list-page', ['uses' => 'CrawlTruyenCVController@listPage'])->name('listPage');
            Route::get('/crawl-data', ['uses' => 'CrawlTruyenCVController@crawl'])->name('crawlData');
            Route::get('/crawl-chapter', ['uses' => 'CrawlTruyenCVController@crawlChapter'])->name('crawlChapter');
            Route::get('/crawl/ajax/listUrl', ['uses' => 'CrawlTruyenCVController@ajaxListUrl'])->name('ajax.listUrl');
            Route::get('/crawl/ajax/list-story', ['uses' => 'CrawlTruyenCVController@ajaxInsertPlace'])->name('listStory');
            Route::get('/crawl/ajax/list-chapter', ['uses' => 'CrawlTruyenCVController@ajaxListChapter'])->name('listChapter');
            Route::post('/crawl/ajax/list-chapter', ['uses' => 'CrawlTruyenCVController@ajaxListChapter2'])->name('listChapter');
            
            Route::post('/crawl/ajax/add-chapter', ['uses' => 'CrawlTruyenCVController@ajaxAddChapter'])->name('addChapter');
            Route::get('/crawl/ajax/update-crawl-logs', ['uses' => 'CrawlTruyenCVController@ajaxUpdateCrawlLogs'])->name('updateCrawlLogs');
        });
    });
});
