<?php

return [
  'web' => [
    'domain' => env('DOMAIN_GUEST'),
    'admin' => env('DOMAIN_ADMIN'),
  ],
  'api' => [
    'domain' => env('DOMAIN_GUEST'),
    'admin' => env('DOMAIN_ADMIN'),
  ]
];
