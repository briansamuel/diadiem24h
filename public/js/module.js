function stripUnicode(slug, target) {
    slug = slug.toLowerCase(); 
    slug = slug.replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, 'a');
    slug = slug.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, 'e');
    slug = slug.replace(/i|í|ì|ỉ|ĩ|ị/gi, 'i');
    slug = slug.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, 'o');
    slug = slug.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, 'u');
    slug = slug.replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, 'y');
    slug = slug.replace(/đ/gi, 'd');
    slug = slug.replace(/\`|\~|\!|\@|\#|\||\$|\%|\^|\&|\*|\(|\)|\+|\=|\,|\.|\/|\?|\>|\<|\'|\"|\:|\;|_/gi, '');
    slug = slug.replace(/ /gi, "-");
    slug = slug.replace(/\-\-\-\-\-/gi, '-');
    slug = slug.replace(/\-\-\-\-/gi, '-');
    slug = slug.replace(/\-\-\-/gi, '-');
    slug = slug.replace(/\-\-/gi, '-');
    slug = '@' + slug + '@';
    slug = slug.replace(/\@\-|\-\@|\@/gi, '');

    $(target).val(slug);
}
$(document).ready(function() {

    // $(window).on('resize load',function() {
    // 	ResizeModule();
    // });

    $(window).scroll(function() {
        if ($(this).scrollTop() > 300) {
            $('.fback-top').fadeIn();
        } else {
            $('.fback-top').fadeOut();
        }
    });

    $('.fancybox').fancybox();

    if ($(".form-gs-timkiem").length) {

        $('.form-gs-timkiem #tinhthanh').on('change', function() {
            var id = $(this).val();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: "POST",
                url: ajax_object.ajax_url,
                data: { action: 'get_quanhuyen', province_id: id, },
                success: function(data) {
                    $('.form-gs-timkiem #khuvuc3').html(data);
                    // $(".select2").select2();
                }
            });
        })

        // $('.form-gs-timkiem #tinhthanh').on('change', function() {
        //     var id = $(this).val();
        //     $.ajax({
        //         headers: {
        //             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        //         },
        //         method: "POST",
        //         url: ajax_object.ajax_url,
        //         data: { action: 'get_dienthoai' },
        //         success: function(data) {
        //             $('.form-gs-timkiem #dienthoai').html(data);
        //             // $(".select2").select2();
        //         }
        //     });
        // })

        $('.form-gs-timkiem #thutuan').on('change', function() {
            var id = $(this).val();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: "POST",
                url: ajax_object.ajax_url,
                data: { action: 'get_nganhnghe', 'id': id, },
                success: function(data) {
                    $('.form-gs-timkiem #chitiet').html(data);
                    $(".select2").select2();
                }
            });
        })

        $('.form-gs-timkiem #thutuan').on('change', function() {
            var id = $(this).val();
            $.ajax({
                method: "POST",
                url: ajax_object.ajax_url,
                data: { action: 'get_dienthoai' },
                success: function(data) {
                    $('.form-gs-timkiem #dienthoai').html(data);
                    $(".select2").select2();
                }
            });
        })
    }

})