<div class="tab-pane" id="chat" role="tabpanel">
    <div class="form-group row">
        <label class="col-lg-3 col-form-label">Bật Chat?:</label>
        <div class="col-lg-6">
            <span class="kt-switch">
                <label>
                    <input type="checkbox" class="form-control" name="theme_option::chat::is_chat" onclick="showChat()" id="is_chat" <?= isset($option['theme_option::chat::is_chat']) && $option['theme_option::chat::is_chat'] === 'on' ? 'checked' : '' ?>>
                    <input type="hidden" name="theme_option::chat::is_chat" id="is_chat_hidden" >
                    <span></span>
                </label>
            </span>
        </div>
    </div>
    <div class="div_wrap_chat">
        <div class="form-group row">
            <label class="col-lg-3 col-form-label">Tùy chọn:</label>
            <div class="col-lg-6">
                <select class="form-control" name="theme_option::chat::option_chat" id="option_chat" onchange="showOptionChat()">
                    <option value="facebook" {{ isset($option['theme_option::chat::option_chat']) && $option['theme_option::chat::option_chat'] === 'facebook' ? 'selected' : ''}}>Facebook</option>
                    <option value="tawk" {{ isset($option['theme_option::chat::option_chat']) && $option['theme_option::chat::option_chat'] === 'tawk' ? 'selected' : ''}}>Tawk</option>
                </select>
            </div>
        </div>
    </div>
    <div id="div_chat_facebook">
        <div class="form-group row">
            <label class="col-lg-3 col-form-label">ID Trang:</label>
            <div class="col-lg-6">
                <input class="form-control" name="theme_option::chat::facebook_page_id" id="facebook_page_id"  value="{{ isset($option['theme_option::chat::facebook_page_id']) ? $option['theme_option::chat::facebook_page_id'] : ''}}"/>
            </div>
        </div>
    </div>
    <div id="div_chat_tawk">
        <div class="form-group row">
            <label class="col-lg-3 col-form-label">ID Trang:</label>
            <div class="col-lg-6">
                <input class="form-control" name="theme_option::chat::tawk_page_id" id="tawk_page_id"  value="{{ isset($option['theme_option::chat::tawk_page_id']) ? $option['theme_option::chat::tawk_page_id'] : ''}}"/>
            </div>
        </div>
    </div>
</div>
