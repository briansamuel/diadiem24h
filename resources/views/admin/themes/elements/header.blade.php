<div class="tab-pane" id="header" role="tabpanel">
    <div class="form-group row">
        <label class="col-lg-3 col-form-label">Hotline:</label>
        <div class="col-lg-6">
            <input type="text" class="form-control" name="theme_option::header::hotline" value="{{isset($option['theme_option::header::hotline']) ? $option['theme_option::header::hotline'] : ''}}">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-lg-3 col-form-label">Email:</label>
        <div class="col-lg-6">
            <input type="text" class="form-control" name="theme_option::header::email" value="{{isset($option['theme_option::header::email']) ? $option['theme_option::header::email'] : ''}}">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-lg-3 col-form-label">Logo</label>
        <div class="col-lg-6">
            <a data-src="@filemanager_get_resource(dialog.php)?type=1&field_id=theme-option-header-logo&lang=vi&akey=@filemanager_get_key()" class="iframe-btn" data-fancybox data-fancybox data-type="iframe" href="javascript:;">
                @if(isset($option['theme_option::header::logo']) && $option['theme_option::header::logo'] !== '')
                <img style="width:50%" id="preview_thumbnail_header_logo" class="img-fluid" src="{{ $option['theme_option::header::logo'] }}">
                @else
                <img style="width:50%" id="preview_thumbnail_header_logo" class="img-fluid" src="admin/images/upload-thumbnail.png">
                @endif
            </a>
            <input type="hidden" name="theme_option::header::logo" id="theme-option-header-logo" value="">
        </div>
        {{-- <div class="kt-avatar kt-avatar--outline kt-avatar--circle" id="theme_option::header::logo">
            <label></label>
            <div class="kt-avatar__holder" style="background-image: url('{{isset($option['theme_option::header::logo']) ? $option['theme_option::header::logo'] : ''}}')"></div>
            <label class="kt-avatar__upload" data-toggle="kt-tooltip" title="" data-original-title="Change logo">
                <i class="fa fa-pen"></i>
                <input type="file" name="theme_option::header::logo" accept="image/*">
            </label>
            <span class="kt-avatar__cancel" data-toggle="kt-tooltip" title="" data-original-title="Cancel logo">
                <i class="fa fa-times"></i>
            </span>
        </div> --}}
    </div>
</div>