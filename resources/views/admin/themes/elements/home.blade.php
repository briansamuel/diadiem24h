<div class="tab-pane" id="home" role="tabpanel">
    <div class="form-group row">
        <label class="col-lg-3 col-form-label">Sắp xếp địa điểm Trang chủ:</label>
        <div class="col-lg-6">
            <select class="form-control" name="theme_option::home::filter_place" id="filter_place">
                <option value="0" {{ isset($option['theme_option::home::filter_place']) && $option['theme_option::home::filter_place'] === '0' ? 'selected' : ''}}>Mặc định</option>
                <option value="1" {{ isset($option['theme_option::home::filter_place']) && $option['theme_option::home::filter_place'] === '1' ? 'selected' : ''}}>Kiểu 1</option>
                <option value="2" {{ isset($option['theme_option::home::filter_place']) && $option['theme_option::home::filter_place'] === '2' ? 'selected' : ''}}>Kiểu 2</option>
                <option value="3" {{ isset($option['theme_option::home::filter_place']) && $option['theme_option::home::filter_place'] === '3' ? 'selected' : ''}}>Kiểu 3</option>
                <option value="4" {{ isset($option['theme_option::home::filter_place']) && $option['theme_option::home::filter_place'] === '4' ? 'selected' : ''}}>Kiểu 4</option>
                <option value="5" {{ isset($option['theme_option::home::filter_place']) && $option['theme_option::home::filter_place'] === '5' ? 'selected' : ''}}>Kiểu 5</option>
                <option value="6" {{ isset($option['theme_option::home::filter_place']) && $option['theme_option::home::filter_place'] === '6' ? 'selected' : ''}}>Kiểu 6</option>
            </select>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-lg-3 col-form-label">Hiển thị About Home:</label>
        <div class="col-lg-6">
            <select class="form-control" name="theme_option::home::about_media_option" id="option_about_media" onchange="showOptionAboutMedia()">
                <option value="image" {{ isset($option['theme_option::home::about_media_option']) && $option['theme_option::home::about_media_option'] === 'image' ? 'selected' : ''}}>Ảnh</option>
                <option value="video" {{ isset($option['theme_option::home::about_media_option']) && $option['theme_option::home::about_media_option'] === 'video' ? 'selected' : ''}}>Video</option>
            </select>
        </div>
    </div>
    <div class="form-group row" id="div_about_image">
            <label class="col-lg-3 col-form-label">Ảnh</label>
            <div class="col-lg-6">
                <a data-src="@filemanager_get_resource(dialog.php)?type=1&field_id=theme_option_about_image&lang=vi&akey=@filemanager_get_key()" class="iframe-btn" data-fancybox data-fancybox data-type="iframe" href="javascript:;">
                    @if(isset($option['theme_option::home::about_image']) && $option['theme_option::home::about_image'] !== '')
                    <img style="width:50%" id="preview_thumbnail" class="img-fluid" src="{{ $option['theme_option::home::image'] }}">
                    @else
                    <img style="width:50%" id="preview_thumbnail" class="img-fluid" src="admin/images/upload-thumbnail.png">
                    @endif
                </a>
                <input type="hidden" name="theme_option::home::image" id="theme_option_about_image" value="">
            </div>
        </div>
    <div class="form-group row" id="div_about_video">
        <label class="col-lg-3 col-form-label">Video</label>
        <div class="col-lg-6">
            @if(isset($option['theme_option::home::about_video']))
            <a data-src="@filemanager_get_resource(dialog.php)?type=3&field_id=theme_option_about_video_upload&lang=vi&akey=@filemanager_get_key()" class="iframe-btn" data-fancybox data-fancybox data-type="iframe" href="javascript:;">
                <video width="320" height="240" controls>
                    <source id="preview_thumbnail_about_video" src="{{ $option['theme_option::home::about_video'] }}" type="video/mp4">
                </video>
            </a>
            @else
            <a data-src="@filemanager_get_resource(dialog.php)?type=3&field_id=theme_option_about_video_upload&lang=vi&akey=@filemanager_get_key()" class="iframe-btn" data-fancybox data-fancybox data-type="iframe" href="javascript:;">
                <video width="320" height="240" controls>
                    <source id="preview_thumbnail_about_video" src="" type="video/mp4">
                </video>
            </a>
            @endif
            <input type="hidden" name="theme_option::home::about_video_upload" id="theme_option_about_video_upload" value="">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-lg-3 col-form-label">Nội dung About Home:</label>
        <div class="col-lg-9">
            <textarea id="about_content" class="tox-target">
                {{isset($option['theme_option::home::about_content']) ? $option['theme_option::home::about_content'] : ''}}
                     </textarea>
            <textarea style="display: none" id="about_content_temp" name="theme_option::home::about_content">
                {{isset($option['theme_option::home::about_content']) ? $option['theme_option::home::about_content'] : ''}}
            </textarea>
        </div>
    </div>
</div>