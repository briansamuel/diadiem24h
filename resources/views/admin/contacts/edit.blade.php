@extends('admin.index')
@section('page-header', 'Liên Hệ')
@section('page-sub_header', 'Cập nhập liên hệ')
@section('style')

@endsection
@section('content')
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

        <!-- begin:: Subheader -->
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container  kt-container--fluid ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        Liên hệ </h3>
                </div>
            </div>
        </div>

        <!-- end:: Subheader -->

        <!-- begin:: Content -->
        <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
            <div class="row">
                <div class="col-md-9">
                    <!--begin::Portlet-->
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Liên hệ
                                </h3>
                            </div>
                        </div>

                        <!--begin::Form-->
                        <form class="kt-form">
                            <div class="kt-portlet__body">
                                <div class="form-group form-group-xs row">
                                    <label class="col-4 col-form-label">Thời gian:</label>
                                    <div class="col-8">
                                        <span class="form-control-plaintext kt-font-bolder">{{$contactInfo->created_at}}</span>
                                    </div>
                                </div>
                                <div class="form-group form-group-xs row">
                                    <label class="col-4 col-form-label">Tên:</label>
                                    <div class="col-8">
                                        <span class="form-control-plaintext kt-font-bolder">{{$contactInfo->name}}</span>
                                    </div>
                                </div>
                                <div class="form-group form-group-xs row">
                                    <label class="col-4 col-form-label">Email:</label>
                                    <div class="col-8">
                                        <span class="form-control-plaintext kt-font-bolder">{{$contactInfo->email}}</span>
                                    </div>
                                </div>
                                <div class="form-group form-group-xs row">
                                    <label class="col-4 col-form-label">Số điện thoại:</label>
                                    <div class="col-8">
                                        <span class="form-control-plaintext kt-font-bolder">{{$contactInfo->phone_number}}</span>
                                    </div>
                                </div>
                                <div class="form-group form-group-xs row">
                                    <label class="col-4 col-form-label">Địa chỉ:</label>
                                    <div class="col-8">
                                        <span class="form-control-plaintext kt-font-bolder">{{$contactInfo->address}}</span>
                                    </div>
                                </div>
                                <div class="form-group form-group-xs row">
                                    <label class="col-4 col-form-label">Tiêu đề:</label>
                                    <div class="col-8">
                                        <span class="form-control-plaintext kt-font-bolder">{{$contactInfo->subject}}</span>
                                    </div>
                                </div>
                                <div class="form-group form-group-xs row">
                                    <label class="col-4 col-form-label">Nội dung:</label>
                                    <div class="col-8">
                                        <textarea rows="5" class="form-control" disabled="">{{$contactInfo->content}}</textarea>
                                    </div>
                                </div>
                            </div>
                        </form>

                        <!--end::Form-->
                    </div>
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Trả lời
                                </h3>
                            </div>
                        </div>

                        <!--begin::Form-->
                        <form class="kt-form" id="kt_add_reply_form">
                            <div class="kt-portlet__body">
                                <div class="kt-notes kt-scroll kt-scroll--pull" id="div_reply">
                                    @forelse($contactReply as $reply)
                                    <div class="kt-notes__items">
                                        <div class="kt-notes__item">
                                            <div class="kt-notes__media">
                                                <span class="kt-notes__icon">
                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--brand">
                                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                            <rect x="0" y="0" width="24" height="24"></rect>
                                                            <path d="M12.9835977,18 C12.7263047,14.0909841 9.47412135,11 5.5,11 C4.98630124,11 4.48466491,11.0516454 4,11.1500272 L4,7 C4,5.8954305 4.8954305,5 6,5 L20,5 C21.1045695,5 22,5.8954305 22,7 L22,16 C22,17.1045695 21.1045695,18 20,18 L12.9835977,18 Z M19.1444251,6.83964668 L13,10.1481833 L6.85557487,6.83964668 C6.4908718,6.6432681 6.03602525,6.77972206 5.83964668,7.14442513 C5.6432681,7.5091282 5.77972206,7.96397475 6.14442513,8.16035332 L12.6444251,11.6603533 C12.8664074,11.7798822 13.1335926,11.7798822 13.3555749,11.6603533 L19.8555749,8.16035332 C20.2202779,7.96397475 20.3567319,7.5091282 20.1603533,7.14442513 C19.9639747,6.77972206 19.5091282,6.6432681 19.1444251,6.83964668 Z" fill="#000000"></path>
                                                            <path d="M8.4472136,18.1055728 C8.94119209,18.3525621 9.14141644,18.9532351 8.89442719,19.4472136 C8.64743794,19.9411921 8.0467649,20.1414164 7.5527864,19.8944272 L5,18.618034 L5,14.5 C5,13.9477153 5.44771525,13.5 6,13.5 C6.55228475,13.5 7,13.9477153 7,14.5 L7,17.381966 L8.4472136,18.1055728 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
                                                        </g>
                                                    </svg>
                                                </span>
                                            </div>
                                            <div class="kt-notes__content">
                                                <div class="kt-notes__section">
                                                    <div class="kt-notes__info">
                                                        <span class="kt-notes__desc">
                                                            {{$reply->created_at}}
                                                        </span>
                                                    </div>
                                                </div>
                                                <span class="kt-notes__body">
                                                    {!! $reply->message !!}
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    @empty
                                    <div class="kt-notes__items"></div>
                                    @endforelse
                                </div>
                                <div class="form-group form-group-last">
                                    <label for="exampleTextarea"></label>
                                    {{csrf_field()}}
                                    <textarea class="tox-target" id="message" name="message" rows="5"></textarea>
                                </div>
                                <div class="kt-form__actions">
                                    <button type="button" id="btn_add_reply" class="btn btn-success kt-margin-t-10">Trả lời</button>
                                </div>
                            </div>
                        </form>

                        <!--end::Form-->
                    </div>
                    <!--end::Portlet-->
                </div>
                <div class="col-md-3">
                    <!--begin::Portlet-->
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Cập Nhập
                                </h3>
                            </div>
                        </div>

                        <!--begin::Form-->
                        <form class="kt-form" id="kt_edit_form">
                            <div class="kt-portlet__body">
                                <div class="form-group">
                                    <label>Status:</label>
                                    {{csrf_field()}}
                                    <input type="hidden" name="_id" id="_id" value="{{$contactInfo->id}}" />
                                    <select class="form-control kt-select2" id="status" name="status">
                                        <option value="unread" <?= $contactInfo->status === 'unread' ? 'selected' : ''; ?>>Chưa đọc</option>
                                        <option value="read" <?= $contactInfo->status === 'read' ? 'selected' : ''; ?>>Đã đọc</option>
                                    </select>
                                </div>
                                <div class="kt-form__actions">
                                    <button type="button" id="btn_edit" class="btn btn-primary">Cập nhập</button>
                                    <button type="reset" class="btn btn-secondary">Hủy bỏ</button>
                                </div>
                            </div>
                        </form>

                        <!--end::Form-->
                    </div>
                </div>
            </div>
        </div>

        <!-- end:: Content -->
</div>
@endsection
@section('vendor-script')

    <script src="assets/plugins/custom/tinymce/tinymce.bundle.js" type="text/javascript"></script>
@endsection
@section('script')
    <!--end::Page Vendors -->
    <script src="admin/js/pages/contacts/edit-contact.js?v3" type="text/javascript"></script>
    <script src="assets/js/pages/components/portlets/tools.js" type="text/javascript"></script>
    <script src="assets/js/pages/crud/forms/widgets/select2.js" type="text/javascript"></script>
    <script src="assets/js/pages/crud/forms/widgets/bootstrap-datepicker.js" type="text/javascript"></script>

    <script>
        ! function(t) {
            var e = {};

            function n(i) {
                if (e[i]) return e[i].exports;
                var r = e[i] = {
                    i: i,
                    l: !1,
                    exports: {}
                };
                return t[i].call(r.exports, r, r.exports, n), r.l = !0, r.exports
            }
            n.m = t, n.c = e, n.d = function(t, e, i) {
                n.o(t, e) || Object.defineProperty(t, e, {
                    enumerable: !0,
                    get: i
                })
            }, n.r = function(t) {
                "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(t, Symbol.toStringTag, {
                    value: "Module"
                }), Object.defineProperty(t, "__esModule", {
                    value: !0
                })
            }, n.t = function(t, e) {
                if (1 & e && (t = n(t)), 8 & e) return t;
                if (4 & e && "object" == typeof t && t && t.__esModule) return t;
                var i = Object.create(null);
                if (n.r(i), Object.defineProperty(i, "default", {
                    enumerable: !0,
                    value: t
                }), 2 & e && "string" != typeof t)
                    for (var r in t) n.d(i, r, function(e) {
                        return t[e]
                    }.bind(null, r));
                return i
            }, n.n = function(t) {
                var e = t && t.__esModule ? function() {
                    return t.default
                } : function() {
                    return t
                };
                return n.d(e, "a", e), e
            }, n.o = function(t, e) {
                return Object.prototype.hasOwnProperty.call(t, e)
            }, n.p = "", n(n.s = 677)
        }({
            677: function(t, e, n) {
                "use strict";
                var i = {
                    init: function() {
                        tinymce.init({
                            selector: "[name='message']",
                            menubar: !1,
                            paste_data_images: true,
                            relative_urls: false,
                            remove_script_host: false,
                            toolbar: ["styleselect fontselect fontsizeselect", "undo redo | cut copy paste | bold italic | link image | alignleft aligncenter alignright alignjustify", "bullist numlist | outdent indent | blockquote subscript superscript | advlist | autolink | lists charmap | print preview |  code"],
                            image_advtab: true,
                            filemanager_access_key: '@filemanager_get_key()',
                            filemanager_sort_by: '',
                            filemanager_descending: '',
                            filemanager_subfolder: '',
                            filemanager_crossdomain: '',
                            external_filemanager_path: '@filemanager_get_resource(dialog.php)',
                            filemanager_title: "Responsive Filemanager",
                            external_plugins: {
                                "filemanager": "/vendor/responsivefilemanager/plugin.min.js"
                            }
                        })
                    }
                };
                jQuery(document).ready((function() {
                    i.init()
                }))
            }
        });
    </script>
@endsection