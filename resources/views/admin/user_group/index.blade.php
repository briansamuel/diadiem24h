@extends('admin.index')
@section('page-header', 'User group')
@section('page-sub_header', 'Danh sách user group')
@section('style')

@endsection
@section('content')
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
                                    <span class="kt-portlet__head-icon">
                                        <i class="kt-font-brand flaticon2-line-chart"></i>
                                    </span>
            <h3 class="kt-portlet__head-title">
                Danh sách user group
            </h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <a href="" class="btn btn-clean btn-icon-sm">
                    <i class="la la-long-arrow-left"></i>
                    Back
                </a>
                &nbsp;
                <div class="dropdown dropdown-inline">
                    <a href="user-group/add" class="btn btn-brand btn-icon-sm"><i class="flaticon2-plus"></i> Add User Group</a>
                </div>
            </div>
        </div>
    </div>
    <div class="kt-portlet__body">

        <!--begin: Selected Rows Group Action Form -->
        <div class="kt-form kt-form--label-align-right kt-margin-t-20 collapse" id="kt_datatable_group_action_form1">
            <div class="row align-items-center">
                <div class="col-xl-12">
                    <div class="kt-form__group kt-form__group--inline">
                        <div class="kt-form__label kt-form__label-no-wrap">
                            <label class="kt-font-bold kt-font-danger-">Chọn
                                <span id="kt_datatable_selected_number1">0</span> dòng:</label>
                                {{csrf_field()}}
                        </div>

                        <div class="kt-form__control">
                            <div class="btn-toolbar">
                                <div class="dropdown kt-margin-r-5">
                                    <button type="button" class="btn btn-brand btn-sm dropdown-toggle" data-toggle="dropdown">
                                        Kích Hoạt
                                    </button>
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item kt_datatable_update_status_all" href="javascript:void(0)" attr="1">Yes</a>
                                        <a class="dropdown-item kt_datatable_update_status_all" href="javascript:void(0)" attr="0">No</a>
                                    </div>
                                </div>
                                <button class="btn btn-sm btn-danger" id="kt_datatable_delete_all">Xóa Tất Cả</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--end: Selected Rows Group Action Form -->
    </div>
    <div class="kt-portlet__body kt-portlet__body--fit">
        @include('admin.elements.alert_flash')
        <!--begin: Datatable -->
        <div class="kt-datatable" id="ajax_data"></div>

        <!--end: Datatable -->
    </div>
</div>
@endsection
@section('script')
    <!--end::Page Vendors -->
    <script src="/assets/js/pages/components/portlets/tools.js" type="text/javascript"></script>
    <script src="/assets/js/pages/crud/forms/widgets/select2.js" type="text/javascript"></script>
    <script src="/admin/js/pages/user_group/user_group.js" type="text/javascript"></script>
@endsection
