@extends('admin.index')
@section('page-header', 'Logs User')
@section('page-sub_header', 'Chi tiết logs user')
@section('style')

@endsection
@section('content')
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

        <!-- begin:: Subheader -->
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container  kt-container--fluid ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        Quản Lý Logs User </h3>
                </div>
            </div>
        </div>

        <!-- end:: Subheader -->

        <!-- begin:: Content -->
        <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
            <div class="row">
                <div class="col-md-12">

                    <!--begin::Portlet-->
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Xem Chi Tiết Logs User
                                </h3>
                            </div>
                        </div>

                        <!--begin::Form-->
                        <form class="kt-form" id="kt_user_edit_form">
                            <div class="kt-portlet__body">
                                <div class="form-group">
                                    <label>ID:</label>
                                    <input type="text" class="form-control" value="{{$logsInfo->id}}" disabled>
                                </div>
                                <div class="form-group">
                                    <label>Email:</label>
                                    <input type="text" class="form-control" value="{{$logsInfo->email}}" disabled>
                                </div>
                                <div class="form-group">
                                    <label>User Id:</label>
                                    <input type="text" class="form-control" placeholder="Nhập username" name="username" value="{{$logsInfo->user_id}}" disabled>
                                </div>
                                <div class="form-group">
                                    <label>Họ và tên:</label>
                                    <input type="text" class="form-control" placeholder="Nhập họ và tên" name="full_name" value="{{$logsInfo->full_name}}" disabled>
                                </div>
                                <div class="form-group">
                                    <label>Hành động:</label>
                                    <textarea class="form-control">{{$logsInfo->action}}</textarea>
                                </div>
                                <div class="form-group">
                                    <label>Nội dung:</label>
                                    <textarea class="form-control">{{$logsInfo->content}}</textarea>
                                </div>
                                <div class="form-group">
                                    <label>Thời gian:</label>
                                    <input type="text" class="form-control" value="{{isset($logsInfo->timestamp) ? date("d-m-Y H:i:s", $logsInfo->timestamp) : ''}}" disabled>
                                </div>
                            </div>
                            <div class="kt-portlet__foot">
                                <div class="kt-form__actions">
                                    <a href="guest" class="btn btn-secondary">Quay lại</a>
                                </div>
                            </div>
                        </form>

                        <!--end::Form-->
                    </div>

                    <!--end::Portlet-->
                </div>
            </div>
        </div>

        <!-- end:: Content -->
    </div>
@endsection