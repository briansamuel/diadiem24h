@extends('frontsite.index')
@section('content')
    <div class="container content">
        @include('frontsite.elements.filter')
        <div class="row">
            <div class="col-md-9">
                @include('frontsite.elements.alert', ['title_query' => 'Địa điểm 24h'])
                @foreach ($places as $key => $place)
                    @include('frontsite.home.elements.item-place')
                @endforeach
                <div class="row">
                    <div class="wp_page">
                        <div class="page">
                            <center>
                                <div class="pagination pnavigation clearfix">
                                    {!! $renderPagination !!}
                                </div>
                            </center>
                        </div>
                    </div>
                </div>
            </div>

            @include('frontsite.elements.sidebar')
        </div>

       
    </div>
@endsection
