@php 
@endphp
<div class="news-v3 bg-color-white">
    <div class="col-md-12 no-padding-left no-padding-right">
        <div class="col-md-3 no-padding-left no-padding-right max-height bottom-5">
            <a rel="bookmark" href="{{ url("{$place->slug}-{$place->id}.html") }}"
                title="{{ html_entity_decode($place->title) }}"><img
                    alt="{{ html_entity_decode($place->title) }}"
                    src="{{ $place->thumbnail }}"></a>
        </div>
        <div class="col-md-9">
            <h2><a href="<?=url("{$place->slug}-{$place->id}.html");?>"
                    title="{{ $place->title }}">{{ $place->title }}</a></h2>
            <div class="row">
                <div class="col-md-5">
                    <p><i class="fa fa-home" aria-hidden="true"></i> <strong>{{ html_entity_decode($place->address) }}</strong></p>
                </div>
                <div class="col-md-4">
                    <p><i class="fa fa-tags" aria-hidden="true"></i> <a
                            href="{{ url("{$place->area_code}") }}">{{ $place->area_name }}</a></p>
                </div>
                <div class="col-md-3">
                    <p><i class="fa fa-comments-o" aria-hidden="true"></i> <i>Bình luận ({{ $place->reviewCount }})</i></p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <p><i class="fa fa-map-marker" aria-hidden="true"></i> Tỉnh/Thành: <a
                            href="{{ url("{$place->province_code}") }}">{{ $place->province_name }}</a></p>
                </div>
                <div class="col-md-6">
                    <p><i class="fa fa-map-o" aria-hidden="true"></i> Quận/Huyện: <a
                            href="{{ url("{$place->district_code}") }}">{{ $place->district_name }}</a></p>
                </div>
                <div class="col-md-2">
                    <p><i class="fa fa-check-square-o" aria-hidden="true"></i> <strong>{{ $place->rating_value }}</strong> <i
                            style="color:#fabd02" aria-hidden="true" class="fa fa-star"></i></p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <p><i aria-hidden="true" class="fa fa-bullseye"></i> Mục: <a
                            href="{{ url("nganh-nghe-{$place->career_slug}.html") }}">{{ $place->career_name }}</a></p>
                </div>
                <div class="col-md-5">
                    <p><i aria-hidden="true" class="fa fa-dollar"></i> Giá từ: <strong>{{ $place->price }}</strong></p>
                </div>
                <div class="col-md-3">
                    <p><i aria-hidden="true" class="fa fa-phone"></i> Tel: {{ $place->phone }} </p>
                </div>
            </div>

        </div>
    </div>
</div>
