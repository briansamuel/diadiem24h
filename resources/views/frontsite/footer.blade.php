<div class="footer-v1">
    <div class="footer">
       <div class="container">
          <div class="row">
             <!-- About -->
             <div class="col-md-4 md-margin-bottom-40">
                <a title="Thế Giới Địa Điểm" href="{{ __('seo.site_name') }}"><img id="logo-footer" class="footer-logo" src="{!! $setting->get('theme_option::footer::banner', 'images/logo.png') !!}" alt=""></a>
                {!! $setting->get('theme_option::footer::footer_introduce', __('frontsite.footer.footer_introduce')) !!}
                <div class="headline">
                   <h3>Liên hệ nhanh</h3>
                </div>
                <address class="md-margin-bottom-40">
                  {!! $setting->get('theme_option::footer::quick_contact', __('frontsite.footer.quick_contact')) !!}
                </address>
             </div>
             <!--/col-md-3-->
             <!-- End About -->
             <!-- Latest -->
             <div class="col-md-4 md-margin-bottom-40">
                <div class="posts">
                   <div class="headline">
                      <h2>Truy cập nhanh</h2>
                   </div>
                   <ul class="list-unstyled latest-list">
                      <li>
                         <a title="{{ __('seo.site_name') }}" href="{{ url('') }}">Trang chủ</a>
                      </li>
                      <li><a href="{{ url('nganh-nghe.html') }}">Ngành nghề</a></li>
                      <li><a href="{{ url('tinh-thanh-pho.gov') }}">Tỉnh/Thành phố</a></li>
                      <li>
                         <a href="{{ url('lien-he.page') }}">Liên hệ</a>
                      </li>
                   </ul>
                </div>
             </div>
             <!--/col-md-3-->
             <!-- End Latest -->
             <!-- Link List -->
             <div class="col-md-4 md-margin-bottom-40">
                <div class="headline">
                   <h2>Website tiện ích</h2>
                </div>
                <ul class="list-unstyled link-list">
                  
                </ul>
             </div>
             <!--/col-md-3-->
             <!-- End Link List -->
          </div>
       </div>
    </div>
    <!--/footer-->
 </div>


<!-- Main Script -->
{{-- <script type="text/javascript" src="js/script.js"></script>
<script type="text/javascript" src="js/popup.js"></script> --}}
<!-- End Main Script -->
<!-- Script For page -->
<!-- End Script For page -->