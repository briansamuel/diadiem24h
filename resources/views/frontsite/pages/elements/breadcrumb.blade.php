<div class="breadcrumbs no-padding-top">
    <div class="containers">
        <ul class="pull-left breadcrumb">
            <li><a href="{{ url("") }}"><span>Trang chủ</span></a></li>
            <li><a href="{{ url("$page->page_slug.page") }}"><span>{{ $page->page_title }}</span></a></li>
            
        </ul>
    </div>
</div>
