<div class="article-content">
    <div class="col-md-12 no-padding-left no-padding-right">
        <div class="title_info_tc">
            <h1 itemprop="name">{{ $page->page_title }}</h1>
        </div>
        
        
        <div class="post-content">
            {!! $page->page_content !!}
        </div>

       
        

    </div>
    <div>
    </div>
    <div class="clear"></div>
</div>
