@extends('frontsite.index', ['provinces' => $provinces])
@section('title', $page->page_seo_title)
@section('description', $page->page_seo_keyword)
@section('description', $page->page_seo_description)
@section('content')
    <div class="container content">
        @include('frontsite.elements.filter')
        <div class="row">
            <div class="col-md-9">
                @include('frontsite.pages.elements.breadcrumb')
               <div class="content-box_contact">
                   <div class="content_page">
                        <div class="tc_have">
                            <div class="items-container">
                                <div class="fade in info_tc sky-for">
                                    <div class="item-page">
                                        @include('frontsite.pages.elements.content')

                                    </div>
                                    <div class="clear"></div>
                                    <hr>
     
                                </div>
                            </div>
                        </div>
                   </div>
               </div>
            </div>

            @include('frontsite.elements.sidebar')
        </div>

       
    </div>
@endsection
