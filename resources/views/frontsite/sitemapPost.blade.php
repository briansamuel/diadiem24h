<?php header('Content-type: text/xml'); ?>
<urlset xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">


    @foreach($posts as $key => $post)
    <url>

    <loc>{{ url($post->post_slug.'.html') }}</loc>
        
        <lastmod>{{ date('Y-m-d\TH:i:s+\0\0\:\0\0', strtotime($post->created_at)) }}</lastmod>
        
        <priority>0.8</priority>
        
    </url>
    @endforeach
</urlset>
