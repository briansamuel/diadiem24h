@extends('frontsite.index', ['provinces' => $provinces])
@section('title', 'Tin tức')
@section('description', 'Tin tức' )
@section('content')
    <div class="container content">
        @include('frontsite.elements.filter')
        <div class="row">
            <div class="col-md-9">
               

                @foreach ($posts as $key => $post)
                    @include('frontsite.news.elements.item')
                @endforeach
                <div class="row">
                    <div class="wp_page">
                        <div class="page">
                            <center>
                                <div class="pagination pnavigation clearfix">
                                    {!! $renderPagination !!}
                                </div>
                            </center>
                        </div>
                    </div>
                </div>
            </div>

            @include('frontsite.elements.sidebar')
        </div>

       
    </div>
@endsection
