@extends('frontsite.index', ['provinces' => $provinces])
@section('title', $post->post_title)
@section('description', $post->post_description)
@section('content')
    <div class="container content">
        @include('frontsite.elements.filter')
        <div class="row">
            <div class="col-md-9">
                @include('frontsite.news.elements.breadcrumb')
               <div class="content-box_contact">
                   <div class="content_page">
                        <div class="tc_have">
                            <div class="items-container">
                                <div class="fade in info_tc sky-for">
                                    <div class="item-page">
                                        @include('frontsite.news.elements.article')

                                    </div>
                                    <div class="clear"></div>
                                    <hr>
     
                                </div>
                            </div>
                        </div>
                   </div>
               </div>
            </div>

            @include('frontsite.elements.sidebar')
        </div>

       
    </div>
@endsection
