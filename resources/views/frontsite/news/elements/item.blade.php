@php 
@endphp
<div class="news-v3 bg-color-white">
    <div class="col-md-12 no-padding-left no-padding-right">
        <div class="col-md-3 no-padding-left no-padding-right max-height bottom-5">
            <a rel="bookmark" href="{{ url("tin-tuc/{$post->post_slug}.html") }}"
                title="{{ html_entity_decode($post->post_title) }}"><img
                    alt="{{ html_entity_decode($post->post_title) }}"
                    src="<?=$post->post_thumbnail;?>"></a>
        </div>
        <div class="col-md-9">
            <h2><a href="<?=url("tin-tuc/{$post->post_slug}.html");?>"
                    title="{{ $post->post_title }}">{{ $post->post_title }}</a></h2>
            
            <div class="row">
                <div class="col-md-12">
                    {!! $post->post_description !!}
                </div>
                
            </div>

        </div>
    </div>
</div>
