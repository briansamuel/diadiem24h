<div class="header">
    <div class="container">
        <a class="logo" href="{{ url('') }}">
            <img src="{{$setting->get('theme_option::header::logo', 'images/logo.png')}}" alt="{{ __('seo.site_name')}}">
        </a>
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="fa fa-bars"></span>
        </button>
    </div>
    <div class="collapse navbar-collapse mega-menu navbar-responsive-collapse">
        <div class="container">
            <ul class="nav navbar-nav">
                <li><a href="{{ url('') }}">Trang chủ</a></li>
                <li class="dropdown mega-menu-fullwidth">
                    <a rel="nofollow" title="Tin tức tài chính" href="{{ url('tin-tuc.html') }}">Tin tức</a>
                    
                </li>
                <li><a href="{{ url("nganh-nghe.html") }}">Ngành nghề</a></li>
                <li class="dropdown mega-menu-fullwidth ">
                    <a href="{{ url("tinh-thanh-pho.gov") }}" data-toggle="dropdown"
                        class="dropdown-toggle">
                        Tỉnh/Thành phố
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <div class="mega-menu-content disable-icons">
                                <div class="container">
                                    <div class="row equal-height">
                                       @php 
                                       $provinces = $provinces->toArray();
                                       
                                       @endphp
                                        <div class="col-md-2 equal-height-in">
                                            <ul class="list-unstyled equal-height-list">
                                                @for($i = 0; $i < 8; $i++ )
                                                
                                                @if(isset($provinces[$i]))
                                                
                                                <li class="live"><a href="{{ url($provinces[$i]->code) }}"
                                                        title="Các địa chỉ ăn uống, mua sắm, làm đẹp tại {{ $provinces[$i]->name }}">{{ $provinces[$i]->name }}</a></li>
                                                @endif
                                                @endfor
                                                
                                            </ul>
                                        </div>
                                        <div class="col-md-2 equal-height-in">
                                            <ul class="list-unstyled equal-height-list">
                                                @for($i = 8; $i < 16; $i++ )
                                                @if(isset($provinces[$i]))
                                                <li class="live"><a href="{{ url($provinces[$i]->code) }}"
                                                        title="Các địa chỉ ăn uống, mua sắm, làm đẹp tại {{ $provinces[$i]->name }}">{{ $provinces[$i]->name }}</a></li>
                                                @endif
                                                @endfor
                                            </ul>
                                        </div>
                                        <div class="col-md-2 equal-height-in">
                                            <ul class="list-unstyled equal-height-list">
                                                @for($i = 16; $i < 24; $i++ )
                                                @if(isset($provinces[$i]))
                                                <li class="live"><a href="{{ url($provinces[$i]->code) }}"
                                                        title="Các địa chỉ ăn uống, mua sắm, làm đẹp tại {{ $provinces[$i]->name }}">{{ $provinces[$i]->name }}</a></li>
                                                @endif
                                                @endfor
                                            </ul>
                                        </div>
                                        <div class="col-md-2 equal-height-in">
                                            <ul class="list-unstyled equal-height-list">
                                                @for($i = 24; $i < 32; $i++ )
                                                @if(isset($provinces[$i]))
                                                <li class="live"><a href="{{ url($provinces[$i]->code) }}"
                                                        title="Các địa chỉ ăn uống, mua sắm, làm đẹp tại {{ $provinces[$i]->name }}">{{ $provinces[$i]->name }}</a></li>
                                                @endif
                                                @endfor
                                            </ul>
                                        </div>
                                        <div class="col-md-2 equal-height-in">
                                            <ul class="list-unstyled equal-height-list">
                                                @for($i = 32; $i <40; $i++ )
                                                @if(isset($provinces[$i]))
                                                <li class="live"><a href="{{ url($provinces[$i]->code) }}"
                                                        title="Các địa chỉ ăn uống, mua sắm, làm đẹp tại {{ $provinces[$i]->name }}">{{ $provinces[$i]->name }}</a></li>
                                                @endif
                                                @endfor
                                            </ul>
                                        </div>
                                        <div class="col-md-2 equal-height-in">
                                            <ul class="list-unstyled equal-height-list">
                                                @for($i = 40; $i < 48; $i++ )
                                                @if(isset($provinces[$i]))
                                                <li class="live"><a href="{{ url($provinces[$i]->code) }}"
                                                        title="Các địa chỉ ăn uống, mua sắm, làm đẹp tại {{ $provinces[$i]->name }}">{{ $provinces[$i]->name }}</a></li>
                                                @endif
                                                @endfor
                                            </ul>
                                        </div>
                                        <div class="col-md-2 equal-height-in">
                                            <ul class="list-unstyled equal-height-list">
                                                @for($i = 48;  $i < 56; $i++ )
                                                @if(isset($provinces[$i]))
                                                <li class="live"><a href="{{ url($provinces[$i]->code) }}"
                                                        title="Các địa chỉ ăn uống, mua sắm, làm đẹp tại {{ $provinces[$i]->name }}">{{ $provinces[$i]->name }}</a></li>
                                                @endif
                                                @endfor
                                            </ul>
                                        </div>
                                        <div class="col-md-2 equal-height-in">
                                            <ul class="list-unstyled equal-height-list">
                                                @for($i = 56;  $i < 64; $i++ )
                                                @if(isset($provinces[$i]))
                                                <li class="live"><a href="{{ url($provinces[$i]->code) }}"
                                                        title="Các địa chỉ ăn uống, mua sắm, làm đẹp tại {{ $provinces[$i]->name }}">{{ $provinces[$i]->name }}</a></li>
                                                @endif
                                                @endfor
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li>
                <li><a title="Liên hệ Thế Giới Địa Điểm" href="{{ url("lien-he.page") }}">Liên hệ</a></li>
            </ul>
        </div>
    </div>
</div>
