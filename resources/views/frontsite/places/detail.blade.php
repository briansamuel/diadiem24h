@extends('frontsite.index', ['provinces' => $provinces])
@section('title', $place->title)
@section('content')
    <div class="container content">
        @include('frontsite.elements.filter')
        <div class="row">
            <div class="col-md-9">
                @include('frontsite.places.elements.breadcrumb')
               <div class="content-box_contact">
                   <div class="content_page">
                        <div class="tc_have">
                            <div class="items-container">
                                <div class="fade in info_tc sky-form" itemscope="" itemtype="http://schema.org/LocalBusiness" id="{{ url("{$place->slug}-{$place->id}.html") }}">
                                    <div class="item-page">
                                        @include('frontsite.places.elements.article')
                                        @include('frontsite.places.elements.galleries')
                                    </div>
                                    <div class="clear"></div>
                                    <hr>
                                    @include('frontsite.places.elements.contact')
                                    @include('frontsite.places.elements.reviews')
                                    @include('frontsite.places.elements.related_places')
                                </div>
                            </div>
                        </div>
                   </div>
               </div>
            </div>

            @include('frontsite.elements.sidebar')
        </div>

       
    </div>
@endsection
