@php 
$galleries = json_decode($place->images);

@endphp
@if(isset($galleries))
<div>
    @foreach($galleries as $image)
    <div class="headline-md col-md-2">
        <img alt="{{ $place->title }}" title="{{ $place->title }}" width="100%"
            src="{{ $image }}">
    </div>
    @endforeach
</div>
@endif