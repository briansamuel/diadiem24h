<hr>
<h3><i class="fa fa-list" aria-hidden="true"></i> Có thể bạn quan tâm</h3>
<div class="content-box_contact">
    <div class="content_page">
        <div class="tc_have">
            
            @foreach($places as $p)
            <div class="news-v3 bg-color-white">
                <div class="col-md-12 no-padding-left no-padding-right">
                    <div class="col-md-3 no-padding-left no-padding-right max-height bottom-5">
                        <a rel="bookmark" href="{{ url("{$p->slug}-{$p->id}.html") }}"
                            title="{{ $p->title }}"><img alt="{{ $p->title }}"
                                src="{{ $p->thumbnail }}"></a>
                    </div>
                    <div class="col-md-9">
                        <h2><a href="{{ url("{$p->slug}-{$p->id}.html") }}"
                                title="{{ $p->title }}">{{ $p->title }}</a></h2>
                        <div class="row">
                            <div class="col-md-5">
                                <p><i class="fa fa-home" aria-hidden="true"></i> <strong>{{ html_entity_decode($p->address) }}</strong></p>
                            </div>
                            <div class="col-md-4">
                                <p><i class="fa fa-tags" aria-hidden="true"></i> <a
                                    href="{{ url("{$p->area_code}") }}">{{ $p->area_name }}</a></p>
                            </div>
                            <div class="col-md-3">
                                <p><i class="fa fa-comments-o" aria-hidden="true"></i> <i>Bình luận ({{ $p->reviewCount }})</i></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <p><i class="fa fa-map-marker" aria-hidden="true"></i> Tỉnh/Thành: <a
                                        href="{{ url("{$p->province_code}") }}">{{ $p->province_name }}</a></p>
                            </div>
                            <div class="col-md-6">
                                <p><i class="fa fa-map-o" aria-hidden="true"></i> Quận/Huyện: <a
                                        href="{{ url("{$p->district_code}") }}">{{ $p->district_name }}</a></p>
                            </div>
                            <div class="col-md-2">
                                <p><i class="fa fa-check-square-o" aria-hidden="true"></i> <strong>{{ $p->rating_value }}</strong> <i
                                        style="color:#fabd02" aria-hidden="true" class="fa fa-star"></i></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <p><i aria-hidden="true" class="fa fa-bullseye"></i> Mục: <a
                                        href="{{ url("nganh-nghe-{$p->career_slug}.html") }}">{{ $p->career_name }}</a></p>
                            </div>
                            <div class="col-md-5">
                                <p><i aria-hidden="true" class="fa fa-dollar"></i> Giá từ: <strong>{{ $p->price }}</strong></p>
                            </div>
                            <div class="col-md-3">
                                <p><i aria-hidden="true" class="fa fa-phone"></i> Tel: {{ $p->phone }} </p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            @endforeach
            <div class="clear"></div>
            <div class="wp_page">
                <div class="page">
                    <center>
                        <div class="pagination pnavigation clearfix">
                            <p class="page-nav"><a rel="nofollow" class="page-nav-act active"
                                    href="{{ url($load_more) }}">Xem thêm</a>
                            </p>
                        </div>
                    </center>
                </div>
            </div>
        </div>
    </div>
</div>
