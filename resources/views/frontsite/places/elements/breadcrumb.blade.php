<div class="breadcrumbs no-padding-top">
    <div class="containers">
        <ul class="pull-left breadcrumb">
            <li><a href="{{ url("") }}"><span>Trang chủ</span></a></li>
            <li><a href="{{ url("{$place->province_code}") }}"><span>{{ $place->province_name }}</span></a></li>
            <li><a href="{{ url("{$place->district_code}") }}"><span>{{ $place->district_name }}</span></a></li>
            <li><a href="{{ url("{$place->area_code}") }}"><span> {{ $place->area_name }}</span></a></li>
        </ul>
    </div>
</div>
<script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "BreadcrumbList",
        "itemListElement": [{
                "@type": "ListItem",
                "position": 1,
                "item": {
                    "@id": "{{ url("") }}",
                    "name": "{{ __('seo.site_name') }}"
                }
            },
            {
                "@type": "ListItem",
                "position": 2,
                "item": {
                    "@id": "{{ url("{$place->province_code}") }}",
                    "name": "{{ $place->province_name }}"
                }
            }, {
                "@type": "ListItem",
                "position": 3,
                "item": {
                    "@id": "{{ url("{$place->district_code}/") }}",
                    "name": "{{ $place->district_name }}"
                }
            }
        ]
    }
</script>