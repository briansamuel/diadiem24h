<div class="article-content">
    <div class="col-md-12 no-padding-left no-padding-right">
        <div class="title_info_tc">
            <h1 itemprop="name">{{ $place->title }}</h1>
        </div>
        <div class="alert alert-warning fade in"><i><b>{{ $place->title }}</b> có địa chỉ tại {{ html_entity_decode($place->address) }}. Dựa vào các đánh giá nhận xét khách quan mà hệ thống thu thập dữ liệu đưa ra <b>{{ $place->title }}t</b> đạt được <b>{{ $place->rating_value }}</b> điểm. Mong mọi người ủng hộ <b>{{ $place->title }}</b> nhiều hơn nữa! {{ $place->phone }}</i></div>
        <div class="col-md-6 no-padding-left no-padding-right bottom-5">
            <img itemprop="image" alt="{{ $place->title }}"
                title="{{ $place->title }}" width="100%"
                src="{{ $place->thumbnail }}">
            <meta data-item-type="home" itemprop="url"
                content="{{ url("{$place->slug}-{$place->id}.html") }}">
        </div>
        <div class="col-md-6 no-padding-lefts no-padding-right">
            <div itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
                <div class="row">
                    <div class="col-md-9">
                        <p><i class="fa fa-home" aria-hidden="true"></i> <strong itemprop="streetAddress">{{ html_entity_decode($place->address) }}</strong></p>
                    </div>
                    <div class="col-md-3">
                        <p><i class="fa fa-check-square-o" aria-hidden="true"></i> <strong>7.0</strong> <i
                                style="color:#fabd02" aria-hidden="true" class="fa fa-star"></i></p>
                    </div>
                </div>
                <p><i class="fa fa-map-marker" aria-hidden="true"></i> Tỉnh/Thành: <a
                    href="{{ url("{$place->province_code}/") }}/">{{ $place->province_name }}</a></p>
                <p><i class="fa fa-map-o" aria-hidden="true"></i> Quận/Huyện: <a
                    href="{{ url("{$place->district_code}/") }}/">{{ $place->district_name }}</a></p>
                <p><i class="fa fa-tags" aria-hidden="true"></i> <a
                        href="{{ url("{$place->area_code}/") }}/">{{ $place->area_name }}</a></p>
            </div>

            <p><i aria-hidden="true" class="fa fa-bullseye"></i> Category: <a
                    href="{{ url("{$place->career_slug}/") }}"><strong>{{ $place->career_name }}</strong></a></p>
            <p><i aria-hidden="true" class="fa fa-dollar"></i> Giá từ: <strong itemprop="priceRange">{{ $place->price }}</strong></p>
            <p><i aria-hidden="true" class="fa fa-phone"></i> Điện thoại: <span itemprop="telephone">
                    <font color="red">{{ $place->phone }}</font>
                </span></p>
            <p><i aria-hidden="true" class="fa fa-key"></i> <i>{{ html_entity_decode($place->keyword) }}</i></p>
            <p><a rel="nofollow" class="sharefacebook"
                    href="https://www.facebook.com/sharer/sharer.php?u={{ url("{$place->slug}-{$place->id}.html") }}&amp;src=sdkpreparse"
                    target="popup"
                    onclick="window.open('https://www.facebook.com/sharer/sharer.php?u={{ url("{$place->slug}-{$place->id}.html") }}&amp;src=sdkpreparse','popup','width=600,height=600'); return false;">Chia
                    sẻ Facebook</a></p>
        </div>
        @php 
        $toppoint = json_decode($place->toppoint);
        $aggregateRating = json_decode($place->aggregate_rating);
        
        @endphp
       
        <div class="microsite-points-summary">
            @if($toppoint)
            <div class="microsite-point-group">
                
                <div class="microsite-top-points">
                    <div>
                        <span class="avg-txt-highlight">{{ $toppoint[0] }}</span>
                    </div>
                    <div class="label">Vị trí</div>
                </div>
                <div class="microsite-top-points">
                    <div>
                        <span class="avg-txt-highlight">{{ $toppoint[1] }}</span>
                    </div>
                    <div class="label">Giá cả</div>
                </div>
                <div class="microsite-top-points">
                    <div>
                        <span class="avg-txt-highlight">{{ $toppoint[2] }}</span>
                    </div>
                    <div class="label">Chất lượng</div>
                </div>
                <div class="microsite-top-points">
                    <div>
                        <span class="avg-txt-highlight">{{ $toppoint[3] }}</span>
                    </div>
                    <div class="label">Phục vụ</div>
                </div>
                <div class="microsite-top-points">
                    <div>
                        <span class="avg-txt-highlight">{{ $toppoint[4] }}</span>
                    </div>
                    <div class="label">Không gian</div>
                </div>
            </div>
            @endif
            @if($aggregateRating)
            <div itemprop="aggregateRating" itemscope="" itemtype="http://schema.org/AggregateRating"
                class="microsite-top-points-block" style="font-size:20px;position:absolute;width: 600px;">
                <meta itemprop="worstRating" content="1">
                <meta itemprop="bestRating" content="10">
                <meta itemprop="ratingCount" content="{{ $aggregateRating[2] }}">
                <div itemprop="ratingValue" class="microsite-point-avg ">{{ $place->rating_value }}</div>
            </div>
            @endif
        </div>

    </div>
    <div>
    </div>
    <div class="clear"></div>
</div>
