<form method="post" action="" name="modacajoomForm1" id="modacajoomForm1">
    <textarea maxlength="1000" required="required" aria-required="true" name="tieude"
        placeholder="Nội dung yêu cầu của bạn (vui lòng nhập tiếng Việt có dấu)..." id="txtEditor"></textarea>
    <div class="clear"></div>
    <input maxlength="15" class="large-4" required="required" aria-required="true" name="phone" type="text" value=""
        size="30" placeholder="Số điện thoại"
        oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
    <input maxlength="50" class="large-4" required="required" aria-required="true" id="author" name="hoten" type="text"
        value="" size="30" placeholder="Họ tên bạn">
    <input name="submit" type="submit" id="submit" class="submit" value="Gửi liên hệ">
    <input type="hidden" value="2" name="hdnSubmit" id="hdnSubmit">
    <input type="hidden" value="/nem-cha-nam-thu-hoang-quoc-viet-59657.html" name="congdungta" id="congdungta">
    <input type="hidden" value="59657" name="idx" id="idx">
    <input type="hidden" value="1623" name="phuongxax" id="phuongxax">
    <input type="hidden" value="1218" name="quanhuyenx" id="quanhuyenx">
    <input type="hidden" value="1169" name="tinhthanhx" id="tinhthanhx">
    <input type="hidden" value="1046" name="nguyenquanx" id="nguyenquanx">
</form>
<div class="infocomment">
    <div class="woocommerce-product-rating">

    </div>
</div>
<div class="clear"></div>
@if(isset($place->map_image) && $place->map_image != '0')
<h4><i class="fa fa-map-marker" aria-hidden="true"></i> Vị trí {{ $place->title }}</h4>
<a target="_blank" rel="nofollow" href="https://www.google.com/maps?q={{ html_entity_decode($place->latlng) }}"><img width="100%" src="{{ html_entity_decode($place->map_image) }}" alt="Vị trí bản đồ {{ $place->title }} ở Bình Định"></a>
<center><span><i><i class="fa fa-map-o" aria-hidden="true"></i> {{ html_entity_decode($place->address) }}</i></span></center>
<span itemprop="geo" itemscope="" itemtype="http://schema.org/GeoCoordinates">
	<meta itemprop="latitude" content="13.7813810000000">
	<meta itemprop="longitude" content="109.2255870000000">
</span>
@endif;
<h4><i class="fa fa-commenting-o" aria-hidden="true"></i> Bình luận {{ $place->title }} ở {{ $place->province_name }}</h4>
