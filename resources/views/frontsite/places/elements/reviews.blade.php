@foreach($reviews as $key => $review)
    <li class="review-item fd-clearbox foody-box-shadow" itemprop="review" itemscope=""
        itemtype="http://schema.org/Review">
        <div class="review-user fd-clearbox">
            <a class="ru-avatar">
                <img width="40" height="40" alt="{{ $review->author }}"
                    src="{{ $review->avatar }}">
            </a>
            <div class="ru-row">
                <a class="ru-username">
                    <span itemprop="author" itemscope="" itemtype="http://schema.org/Person">
                        <span itemprop="name" class="tieudebl">{{ $review->author }}</span>
                    </span>
                </a>
                <div class="ru-stats">
                    đã bình luận
                    <span>bằng</span>
                    <a class="ru-device">
                        {{ $review->device }}
                        <i class="fa fa-android"></i>

                    </a>
                    <span class="ru-time" title="{{ date_format(date_create($review->created_at), 'd/m/Y H:i:s') }}" itemprop="datePublished"
                        content="{{ date_format(date_create($review->created_at), 'Y-m-d H:i:s (T)') }}">{{ $review->created_at }}</span>
                </div>
            </div>




        </div>


        <div class="review-des fd-clearbox">
            <div class="review-points green" itemprop="reviewRating" itemscope="" itemtype="http://schema.org/Rating">
                <meta itemprop="worstRating" content="1">
                <meta itemprop="bestRating" content="10">
                <span itemprop="ratingValue">{{ $review->rating }}</span>

            </div>
            <span itemprop="name" class="tieudebl">{{ $place->title }}</span>

            <div class="rd-des toggle-height"><span itemprop="reviewBody">{{ $review->review }}</span>
                ...</div>
        </div>



        <ul class="review-photos fd-clearbox">
            @php 
            
            $photos  = json_decode($review->review_photo);
            @endphp
            @foreach($photos as $photo)
            <li class="rp-size-5">
                <a>
                    <img class="rp-size-fw"
                        src="{{ $photo }}"
                        alt="{{ $place->title }}">
                </a>

            </li>
            @endforeach
        </ul>
    </li>
@endforeach
