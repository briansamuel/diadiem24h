<div class="alert alert-warning fade in margin-bottom-40">
    <h1>{{ isset($title_query) ? $title_query : '' }}</h1>
    <p><i aria-hidden="true" class="fa fa-search"></i> Tìm thấy <font color="red">{{ number_format(Session::get('total_place'))}}</font> kết quả ({{ Session::get('query_excution_time')}} giây)</p>
</div>