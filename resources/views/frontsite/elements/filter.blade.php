<div class="margin-bottom-30">
    <div class="well sky-form">
        <form method="get" action="/sources/xuly.php" name="frmKhuVuc" class="form-gs-timkiem"
            enctype="multipart/form-data">
            <input type="hidden" value="timdiachi" name="page">
            <div class="row">

                <section class="col col-3">
                    <label class="select">
                        <select name="tinhthanh" id="tinhthanh" class="clsip slinput select2">
                            <option value="0">-- Tỉnh/Thành --</option>
                            @foreach($provinces as $p)
                            <option {{ isset($province->id) && $p->id == $province->id ? 'selected' : '' }} value="{{ $p->id }}">{{ $p->name.'('.$p->count.')' }}</option>
                            @endforeach
                            
                        </select>
                        <i></i>
                    </label>
                </section>
                <section class="col col-3">
                    <label class="select" id="khuvuc3">
                        <select {{ !isset($districts) ? 'disabled' : ''}} name="quanhuyen" id="quanhuyen" class="clsip slinput select2">
                            <option value="0">---Quận/Huyện---</option>
                            @if(!empty($districts))
                            @foreach($districts as $d)
                            <option {{ isset($district->id) && $d->id == $district->id ? 'selected' : '' }} value="{{ $d->id }}">{{ $d->name }}</option>
                            @endforeach
                            @endif
                        </select><i></i>
                    </label>
                </section>

                <section class="col col-3">
                    <label class="select">
                        <select name="thutuan" id="thutuan" class="clsip slinput">
                            <option value="0">-- Danh mục --</option>
                           
                            @foreach($careers as $c)
                            <option {{ isset($career->id) && ($c->id == $career->id || $c->id == $career->parent) ? 'selected' : '' }} value="{{ $c->id }}">{{ $c->name }}</option>
                            @endforeach
                            
                        </select>
                        <i></i>
                    </label>
                </section>
                <section class="col col-3">
                    <label class="select" id="chitiet">
                        <select {{ !isset($careers_child) ? 'disabled' : ''}} name="chitiet" class="clsip slinput select2">
                            <option value="0">---Danh mục chi tiết---</option>
                            @if(!empty($careers_child))
                            @foreach($careers_child as $cc)
                            <option {{ $cc->id == $career->id ? 'selected' : '' }} value="{{ $cc->id }}">{{ $cc->name }}</option>
                            @endforeach
                            @endif
                        </select><i></i>
                    </label>
                </section>

                <div class="clear-both"></div>
                <section class="col col-3">
                    <label class="select">
                        <select name="danhgia" id="danhgia" class="clsip slinput">
                            <option value="0" selected="">-- Đánh giá --</option>
                            <option value="1" {{ request()->get('r') == 1 ? 'selected' : ''  }}>1-5 Điểm (Kém)</option>
                            <option value="2" {{ request()->get('r') == 2 ? 'selected' : ''  }}>5-7 Điểm (Trung Bình)</option>
                            <option value="3" {{ request()->get('r') == 3 ? 'selected' : ''  }}>7-9 Điểm (Khá)</option>
                            <option value="4" {{ request()->get('r') == 4 ? 'selected' : ''  }}>9-10 Điểm (Tốt)</option>
                        </select>
                        <i></i>
                    </label>
                </section>
                <section class="col col-3">
                    <label class="select">
                        <select name="chinhanh" id="chinhanh" class="clsip slinput">
                            <option value="0" selected="">-- Chi nhánh --</option>
                            <option value="1" {{ request()->get('b') == 1 ? 'selected' : ''  }}>1 điểm duy nhất</option>
                            <option value="2" {{ request()->get('b') == 2 ? 'selected' : ''  }}>2 chi nhánh</option>
                            <option value="3" {{ request()->get('b') == 3 ? 'selected' : ''  }}>3-5 chi nhánh</option>
                            <option value="4" {{ request()->get('b') == 4 ? 'selected' : ''  }}>Trên 5 chi nhánh</option>
                        </select>
                        <i></i>
                    </label>
                </section>
                <section title="Cần chọn ngành nghề hoặc tỉnh thành sau đó Ấn Tìm Kiếm" class="col col-3">
                    <label class="select" id="dienthoai">
                        <select name="sapxepdidong" id="sapxepdidong" class="clsip slinput">
                            <option value="0" selected="">-- Tất cả điện thoại --</option>
                            <option value="1">Điện thoại di động</option>
                            <option value="2">Điện thoại bàn</option>
                            <option value="3">Chưa có điện thoại</option>

                        </select>
                        <i></i>
                    </label>
                </section>
                <section class="col col-3">
                    <button type="submit" class="btn-u btn-block"><i class="fa fa-search"></i> Tìm kiếm</button>
                </section>
            </div>
        </form>
    </div>
</div>
