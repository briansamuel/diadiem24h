<div class="col-md-3">
    <div class="fixbanner-ads" style="z-index: 1000;"></div>
    <div></div>
    <ul id="sidebar-nav" class="list-group sidebar-nav-v1">
        <li class="list-group-item active">
            <a>Tỉnh/Thành phố</a>
            <ul class="collapse in">
                @foreach($provinces as $province)
                <li>
                    <span class="badge badge-u">{{ $province->count }}</span>
                    <a title="Các địa chỉ ăn uống, mua sắm, làm đẹp tại {{ $province->name }}"
                        href="{{ url($province->code) }}">{{ $province->name }}</a>
                </li>
                @endforeach
                
            </ul>
        </li>
    </ul>


    <div class="ads_left">
        <div class="ads_item">


        </div>
    </div>
    
</div>
