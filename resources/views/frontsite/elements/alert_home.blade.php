<div class="alert alert-warning fade in margin-bottom-40">
    <h1>Danh sách Thế Giới Địa Điểm</h1>
    <p><i aria-hidden="true" class="fa fa-search"></i> Có tổng số <strong>{{ number_format(Session::get('total_place')) }}</strong> địa điểm được thống kê trong
        <strong>63</strong> tỉnh thành trên cả nước ({{ Session::get('query_excution_time')}} giây)</p>
</div>
