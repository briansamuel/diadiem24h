<!DOCTYPE html>
<html lang="vi-VN">
<head>

    <base href="{{ url('') }}">
    <meta charset="UTF-8">
    
    <link rel="canonical" href="{{ url()->current() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=EDGE">
    <title>@yield('title', 'Trang chủ') - Sotaydiadiem.com</title>
    <meta name="keywords" content="@yield('keyword', 'Sotaydiadiem.com')">
    <meta name="description" content="@yield('description', 'Sotaydiadiem.com')">
    <meta name="robots" content="index, follow">
    <meta property="og:type" content="website">
    <meta property="og:site_name" content="{{ __('seo.site_name')}}">
    <meta property="og:url" content="{{ url()->current() }}">
    <meta property="og:title" content="@yield('title', 'Sotaydiadiem.com')">
    <meta property="og:description" content="@yield('description', 'Sotaydiadiem.com')">
    <!--<link rel="alternate" href="https://thegioidiadiem.com/" hreflang="vi-vn"/>-->
    <meta property="og:image" itemprop="thumbnailUrl" content="@yield('thumbnailUrl', asset('images/logo.jpg'))">
    <meta property="og:image:url" itemprop="thumbnailUrl" content="@yield('thumbnailUrl', asset('images/logo.jpg'))" >

    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <link rel="shortcut icon" href="{{asset('')}}images/favicon.ico">
    <link rel="stylesheet" href="{{asset('')}}css/bootstrap.mins.css">
    <link rel="stylesheet" href="{{asset('')}}css/style.css">
    <link rel="stylesheet" href="{{asset('')}}css/header-default.css">
    <link rel="stylesheet" href="{{asset('')}}css/footer-v1.css">
    <link rel="stylesheet" href="{{asset('')}}css/line-icons.css">
    <link rel="stylesheet" href="{{asset('')}}css/font-awesome.min.css">
    <link rel="stylesheet" href="{{asset('')}}css/sky-forms.css">
    <link rel="stylesheet" href="{{asset('')}}css/custom-sky-forms.css">
    <link rel="stylesheet" href="{{asset('')}}css/default.css">
    <link rel="stylesheet" href="{{asset('')}}css/custom.css">
    <link rel="stylesheet" href="{{asset('')}}css/cssmois.css">
    @yield('style')
    <script type="application/ld+json">
        {
            "@context" : "https://schema.org",
            "@type" : "Website",
            "name" : "{{ __('seo.site_name')}}",
            "alternateName": "{{ __('seo.site_name')}}",
            "url" : "{{ url('') }}"
        }
    </script>
    <script type="text/javascript">
        /* <![CDATA[ */
        var ajax_object = {"ajax_url":"{{ url('source/ajax/')}}"};
        /* ]]> */
    </script>
    {!! $setting->get('general::custom_script::header_script', '') !!}
</head>
<body>
<div class="wrapper">
    
    @include('frontsite.header', ['provinces' => $provinces ?? ''])
    @yield('content')
    @include('frontsite.footer')
    <script type="text/javascript" src="{{asset('')}}js/jquery.min.js"></script>
    <script type="text/javascript" src="{{asset('')}}js/bootstrap.min.js"></script>
    <script type="text/javascript" src="{{asset('')}}js/back-to-top.js"></script>
    <script type="text/javascript" src="{{asset('')}}js/module.js"></script>
    <script type="text/javascript" src="{{asset('')}}js/jquery.fancybox.js"></script>
    @yield('script')
    {!! $setting->get('general::custom_script::footer_script', '') !!}

</div>

</body>
</html>