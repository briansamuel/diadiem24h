@extends('frontsite.index', ['provinces' => $provinces])
@section('title', 'Địa điểm 24h')
@section('content')
    <div class="container content">
        @include('frontsite.elements.filter')
        <div class="row">
            <div class="col-md-9">
                <style>
                    .table1,
                    .table1 td,
                    .table1 th {
                        border: 1px solid #c2c2c2;
                        border-collapse: collapse;
                    }

                    tr.alt-row {
                        background: #f5f5f5 none repeat scroll 0 0;
                    }

                    .table1 th.th1 {
                        width: 7%;
                    }

                    .table1 th {
                        background: #333 none repeat scroll 0 0;
                        color: #fff;
                    }

                    .table1 td,
                    .table1 th {
                        padding: 3px 5px;
                        vertical-align: middle;
                    }

                </style>
                <table class="table1" style="width:100%;">
                    <tbody>
                        <tr class="alt-row">
                            <th>Tên ngành</th>
                            <th>Số lượng</th>
                        </tr>
                        @foreach($careers as $career)
                        <tr>
                            <td class="left"><a title="Các địa điểm Ăn uống"
                                    href="{{ url("nganh-nghe-{$career->slug}.html") }}">{{ $career->name }}</a> <i>({{ number_format($career->count) }})</i>
                            </td>
                            <td>{{ $career->count }}</td>
                        </tr>
                        @endforeach
                        <tr class="alt-row"></tr>


                        <tr class="alt-row">
                        </tr>
                    </tbody>
                </table>
            </div>

            @include('frontsite.elements.sidebar')
        </div>


    </div>
@endsection
