@extends('frontsite.index', ['provinces' => $provinces])
@section('title', 'Tỉnh thành phố')
@section('description', 'Tỉnh thành phố')
@section('content')
    <div class="container content">
        @include('frontsite.elements.filter')
        <div class="row">
            <div class="col-md-12">
                @include('frontsite.elements.alert_home')
                <div class="row">
                    @foreach($provinces as $province)
                    <div class="col-md-3 col-xs-6">
                        <p>                
                            <span class="badge badge-u">{{ number_format($province->count) }}</span>
                                <a title="Các địa chỉ ăn uống, mua sắm, làm đẹp tại {{ $province->name }}" href="{{ url($province->code) }}">{{ $province->name }}</a>                 
                            <i class="fa fa-angle-double"></i> 
                        </p>
                    </div>
                    @endforeach
                </div>
                   
            </div>

           
        </div>


    </div>
@endsection
