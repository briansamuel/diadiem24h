@extends('frontsite.index', ['provinces' => $provinces])
@section('title', $title )
@section('description', $title )
@section('content')
    <div class="container content">
        @include('frontsite.elements.filter')
        <div class="row">
            <div class="col-md-9">
                @include('frontsite.elements.alert', ['title_query' => $title ])

                @foreach ($places as $key => $place)
                    @include('frontsite.home.elements.item-place')
                @endforeach
                <div class="row">
                    <div class="wp_page">
                        <div class="page">
                            <center>
                                <div class="pagination pnavigation clearfix">
                                    {!! $renderPagination !!}
                                </div>
                            </center>
                        </div>
                    </div>
                </div>
            </div>

            @include('frontsite.elements.sidebar')
        </div>

       
    </div>
@endsection
