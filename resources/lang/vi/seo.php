<?php

return array(
  'home' =>
  array(
    'title' => 'Tổng Hợp Địa Điểm 24h',
    'keyword' => 'the gioi dia diem, thế giới địa điểm, địa điểm ăn uống, địa điểm foody',
    'description' => 'Tìm địa điểm nhà hàng, ăn uống, địa điểm quán Cafe, Địa điểm nghỉ mát tốt nhất Việt Nam',
  ),
  'province' =>
  array(
    'title' => 'Các địa chỉ ăn uống, mua sắm, làm đẹp tại :name',
    'title_ab' => 'Các địa chỉ ăn uống, mua sắm, làm đẹp tại :name',
    'keyword' => 'Các địa chỉ ăn uống, mua sắm, làm đẹp tại :name',
    'description' => 'Các địa chỉ ăn uống, mua sắm, làm đẹp tại :name',
  ),
  'district' =>
  array(
    'title' => 'Các điểm mua sắm, ăn uống, cafe tại :district : province ',
    'title_ab' => 'Các điểm mua sắm, ăn uống, cafe tại :district : province',
    'keyword' => 'Các điểm mua sắm, ăn uống, cafe tại :district : province',
    'description' => 'Các điểm mua sắm, ăn uống, cafe tại :district : province',
  ),
  'career' => array(
    'title' => 'Các địa điểm :name ',
    'title_ab' => 'Các địa điểm :name',
    'keyword' => 'Các địa điểm :name',
    'description' => 'Các địa điểm :name',
  ),
  'site_name' => 'Sổ tay địa điểm',
);
