<?php

return array (
  'home' => 
  array (
    'description' => 'Home',
    'keyword' => 'SenHos',
    'title' => 'Home',
  ),
  'news' => 
  array (
    'title_ab' => 'News',
    'title' => 'News',
    'keyword' => 'Tin tức',
    'description' => 'Tin tức của SenHos',
  ),
  'partner' => 
  array (
    'description' => 'Partners',
    'keyword' => 'Partners',
    'title' => 'Partners',
    'title_ab' => 'Partners',
  ),
  'project' => 
  array (
    'description' => 'Project',
    'keyword' => 'Project',
    'title' => 'Project',
    'title_ab' => 'Project',
  ),
);
