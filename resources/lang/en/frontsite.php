<?php

return array (
  'breadcrumb' => 
  array (
    'home_text' => 'Trang chủ',
    'project_text' => 'Project',
    'partner_text' => 'Partner',
  ),
  'home' => 
  array (
    'about_text' => 'ABOUT SENHOS',
    'gallery_video' => 'VIDEO LIBRARY',
    'project_feature' => 'OUTSTANDING PROJECT',
  ),
  'project' => 
  array (
    'other_project' => 'Other projects',
    'video_project' => 'Project video',
  ),
  'header' => 
  array (
    'login' => 'Log in',
    'language_vi' => 'Vietnamese',
    'language_en' => 'English',
    'search' => 'Search',
    'logout' => 'Logout',
    'site_name' => 'SEN HOSPITALITY',
  ),
  'projects' => 
  array (
    'lastest_project' => 'DỰ ÁN ĐÃ THỰC HIỆN',
    'description' => 'SEN HOSPITALITY cung cấp dịch vụ tư vấn và quản lý khách sạn chuyên nghiệp, chất lượng cùng tinh thần “cùng nhau phát triển - cùng nhau thành công” cho các chủ đầu tư trong nước, từ khi thành lập đến nay, SEN Hospitality luôn được các đối tác tin tưởng, được giới chuyên môn đánh giá là một trong những thương hiệu quản lý khách sạn hàng đầu.',
  ),
  'partners' => 
  array (
    'description' => 'SEN HOSPITALITY cung cấp dịch vụ tư vấn và quản lý khách sạn chuyên nghiệp, chất lượng cùng tinh thần “cùng nhau phát triển - cùng nhau thành công” cho các chủ đầu tư trong nước, từ khi thành lập đến nay, SEN Hospitality luôn được các đối tác tin tưởng, được giới chuyên môn đánh giá là một trong những thương hiệu quản lý khách sạn hàng đầu.',
    'lastest_partner' => 'ĐỐI TÁC',
  ),
  'booking' => 
  array (
    'no_review' => 'No review for here.',
  ),
);
